﻿<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="/xmlrpc.php">
    <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-W8XSN4W');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125473191-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-125473191-1');
    </script>
    <title>Vinsofts - Cung cấp giải pháp và ứng dụng CNTT theo yêu cầu hàng đầu Việt Nam</title>
    <!-- This site is optimized with the Yoast SEO Premium plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
    <meta name="description" content="Vinsofts - Cung cấp giải pháp phát triển phần mềm ứng dụng Web, Mobile &amp; Blockchain chuyên nghiệp hàng đầu Việt Nam" />
    <meta name="keywords" content="Web, Mobile app, Blockchain vi" />
    <link rel="canonical" href="/" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Vinsofts - Cung cấp giải pháp và ứng dụng CNTT theo yêu cầu hàng đầu Việt Nam" />
    <meta property="og:description" content="Vinsofts - Cung cấp giải pháp phát triển phần mềm ứng dụng Web, Mobile &amp; Blockchain chuyên nghiệp hàng đầu Việt Nam" />
    <meta property="og:url" content="/" />
    <meta property="og:site_name" content="Công ty phần mềm Vinsofts" />
    <meta property="fb:app_id" content="1096476827181655" />
    <meta property="og:image" content="/wp-content/uploads/2019/03/call-centre-man.png" />
    <meta property="og:image:secure_url" content="/wp-content/uploads/2019/03/call-centre-man.png" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Vinsofts - Cung cấp giải pháp phát triển phần mềm ứng dụng Web, Mobile &amp; Blockchain chuyên nghiệp hàng đầu Việt Nam" />
    <meta name="twitter:title" content="Vinsofts - Cung cấp giải pháp và ứng dụng CNTT theo yêu cầu hàng đầu Việt Nam" />
    <meta name="twitter:image" content="/wp-content/uploads/2019/03/call-centre-man.png" />
    <script type='application/ld+json'>
    { "@context": "http:\/\/schema.org", "@type": "WebSite", "@id": "#website", "url": "\/", "name": "C\u00f4ng ty ph\u1ea7n m\u1ec1m Vinsofts", "potentialAction": { "@type": "SearchAction", "target": "\/?s={search_term_string}", "query-input": "required name=search_term_string" } }
    </script>
    <meta name="google-site-verification" content="J5njbcNY9OFQoTYEoGIFhKFIZr_1odNSm3ycU99slBo" />
    <!-- / Yoast SEO Premium plugin. -->
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin Công ty phần mềm Vinsofts &raquo;" href="/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Công ty phần mềm Vinsofts &raquo;" href="/comments/feed/" />
    <!-- <link rel='stylesheet' id='vc_extend_shortcode-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes.css?version=910' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='vc_extend_style-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes-3rd.css?version=763' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='bootstrap-css'  href='/wp-content/themes/fo/css/bootstrap.css?version=1858' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?version=693' type='text/css' media='all' /> -->
    <link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qjpyrk45/4b9o.css" media="all" />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
    #rs-demo-id {}
    </style>
    <!-- <link rel='stylesheet' id='qts_front_styles-css'  href='/wp-content/plugins/qtranslate-slug/assets/css/qts-default.css' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='themesflat_main-css'  href='/wp-content/themes/fo/css/main.css?version=9629' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='themesflat-style-css'  href='/wp-content/themes/fo-child/style.css' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='font-fontawesome-css'  href='/wp-content/themes/fo/css/font-awesome.css?version=9249' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='themesflat-ionicons-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=4011' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='vc_simpleline-css-css'  href='/wp-content/themes/fo/css/simple-line-icons.css?version=7638' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='vc_ion_icon-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=2147' type='text/css' media='all' /> -->
    <link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/9jminkn5/4b9o.css" media="all" />
    <!--[if lte IE 9]><link rel='stylesheet' id='ie9-css'  href='/wp-content/themes/fo/css/ie.css?version=3086' type='text/css' media='all' /><![endif]-->
    <!-- <link rel='stylesheet' id='themesflat_logo-css'  href='/wp-content/themes/fo/css/logo.css?version=4650' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='themesflat_animate-css'  href='/wp-content/themes/fo/css/animate.css?version=2792' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='themesflat_responsive-css'  href='/wp-content/themes/fo/css/responsive.css?version=9022' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='themesflat-inline-css-css'  href='/wp-content/themes/fo/css/inline-css.css?version=9429' type='text/css' media='all' /> -->
    <link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/mmoo7qcx/4b9o.css" media="all" />
    <style id='themesflat-inline-css-inline-css' type='text/css'>
    .logo {
        padding-top: 24px;
        padding-left: 15px;
    }

    .footer {
        padding-top: 64px;
        padding-bottom: 74px;
    }

    .page-title {
        padding-top: 21px;
    }

    .logo img,
    .logo svg {
        height: 70px;
    }

    .page-title .overlay {
        background: #ffffff
    }

    .page-title {
        background: url() center /cover no-repeat;
    }

    .page-title h1 {
        color: #21536c !important;
    }

    .breadcrumbs span,
    .breadcrumbs span a,
    .breadcrumbs a {
        color: #595959 !important;
    }

    body,
    button,
    input,
    select,
    textarea {
        font-family: Poppins;
    }

    body,
    button,
    input,
    select,
    textarea {
        font-weight: 400;
    }

    body,
    button,
    input,
    select,
    textarea {
        font-style: normal;
    }

    body,
    button,
    input,
    select,
    textarea {
        font-size: 14px;
    }

    body,
    button,
    input,
    select,
    textarea {
        line-height: 25px;
    }

    h1,
    h2,
    h3,
    h5,
    h6 {
        font-family: Poppins;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-weight: 600;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-style: normal;
    }

    #mainnav>ul>li>a {
        font-family: Poppins;
    }

    #mainnav>ul>li>a {
        font-weight: 600;
    }

    #mainnav>ul>li>a {
        font-style: normal;
    }

    #mainnav ul li a {
        font-size: 14px;
    }

    #mainnav>ul>li>a {
        line_height100px;
    }

    h1 {
        font-size: 32px;
    }

    h2 {
        font-size: 25px;
    }

    h3 {
        font-size: 22px;
    }

    h4 {
        font-size: 18px;
    }

    h5 {
        font-size: 15px;
    }

    h6 {
        font-size: 14px;
    }

    .iconbox .box-header .box-icon span,
    a:hover,
    a:focus,
    .portfolio-filter li a:hover,
    .portfolio-filter li.active a,
    .themesflat-portfolio .item .category-post a,
    .color_theme,
    .widget ul li a:hover,
    .footer-widgets ul li a:hover,
    .footer a:hover,
    .themesflat-top a:hover,
    .themesflat-portfolio .portfolio-container.grid2 .title-post a:hover,
    .themesflat-button.no-background,
    .themesflat-button.blog-list-small,
    .show-search a i:hover,
    .widget.widget_categories ul li a:hover,
    .breadcrumbs span a:hover,
    .breadcrumbs a:hover,
    .comment-list-wrap .comment-reply-link,
    .portfolio-single .content-portfolio-detail h3,
    .portfolio-single .content-portfolio-detail ul li:before,
    .themesflat-list-star li:before,
    .themesflat-list li:before,
    .navigation.posts-navigation .nav-links li a .meta-nav,
    .testimonial-sliders.style3 .author-name a,
    ul.iconlist .list-title a:hover,
    .themesflat_iconbox .iconbox-icon .icon span,
    .bottom .copyright a,
    .top_bar2 .wrap-header-content ul li i {
        color: #337493;
    }

    #Ellipse_7 circle,
    .testimonial-sliders .logo_svg path {
        fill: #337493;
    }

    .info-top-right a.appoinment,
    .wrap-header-content a.appoinment,
    button,
    input[type=button],
    input[type=reset],
    input[type=submit],
    .go-top:hover,
    .portfolio-filter.filter-2 li a:hover,
    .portfolio-filter.filter-2 li.active a,
    .themesflat-socials li a:hover,
    .entry-footer .social-share-article ul li a:hover,
    .themesflat-button,
    .featured-post.blog-slider .flex-prev,
    .featured-post.blog-slider .flex-next,
    mark,
    ins,
    #themesflat-portfolio-carousel ul.flex-direction-nav li a,
    .flex-direction-nav li a,
    .navigation.posts-navigation .nav-links li a:after,
    .title_related_portfolio:after,
    .navigation.loadmore a:hover,
    .owl-theme .owl-controls .owl-nav [class*=owl-],
    .widget.widget_tag_cloud .tagcloud a,
    .btn-menu:before,
    .btn-menu:after,
    .btn-menu span,
    .themesflat_counter.style2 .themesflat_counter-icon .icon,
    widget a.appoinment,
    .themesflat_imagebox .imagebox-image:after,
    .nav-widget a.appoinment {
        background-color: #337493;
    }

    .themesflat_btnslider:not(:hover) {
        background-color: #337493 !important;
    }

    .loading-effect-2>span,
    .loading-effect-2>span:before,
    .loading-effect-2>span:after,
    textarea:focus,
    input[type=text]:focus,
    input[type=password]:focus,
    input[type=datetime]:focus,
    input[type=datetime-local]:focus,
    input[type=date]:focus,
    input[type=month]:focus,
    input[type=time]:focus,
    input[type=week]:focus,
    input[type=number]:focus,
    input[type=email]:focus,
    input[type=url]:focus,
    input[type=search]:focus,
    input[type=tel]:focus,
    input[type=color]:focus,
    select:focus,
    .owl-theme .owl-dots .owl-dot.active span,
    .owl-theme .owl-dots .owl-dot:hover span,
    .navigation.loadmore a:hover {
        border-color: #337493
    }

        {
        border-color: #337493 !important;
    }

        {
        color: #fff !important;
    }

        {
        background-color: #2e363a !important;
    }

    #Financial_Occult text,
    #F__x26__O tspan {
        fill: #595959;
    }

    body {
        color: #595959
    }

    a,
    .portfolio-filter li a,
    .themesflat-portfolio .item .category-post a:hover,
    .title-section .title,
    ul.iconlist .list-title a,
    .breadcrumbs span a,
    .breadcrumbs a,
    .breadcrumbs span,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    strong,
    .testimonial-content blockquote,
    .testimonial-content .author-info,
    .sidebar .widget ul li a,
    .themesflat_counter.style2 .themesflat_counter-content-right,
    .themesflat_counter.style2 .themesflat_counter-content-left,
    .title_related_portfolio,
    .navigation.paging-navigation:not(.loadmore) a:hover,
    .navigation.paging-navigation .current,
    .page-links a:hover,
    .page-links a:focus,
    .widget_search .search-form input[type=search],
    .entry-meta ul,
    .entry-meta ul.meta-right,
    .entry-footer strong,
    .widget.widget_archive ul li:before,
    .widget.widget_categories ul li:before,
    .widget.widget_recent_entries ul li:before {
        color: #595959
    }

    .owl-theme .owl-dots .owl-dot span,
    .widget .widget-title:after,
    .widget .widget-title:before,
    ul.iconlist li.circle:before {
        background-color: #595959
    }

    .navigation.paging-navigation:not(.loadmore) a:hover,
    .navigation.paging-navigation:not(.loadmore) .current,
    .page-links a:hover,
    .page-links a:focus,
    .page-links>span {
        border-color: #595959
    }

    .themesflat-top {
        background-color: #21536c;
    }

    .themesflat-top .border-left:before,
    .themesflat-widget-languages:before,
    .themesflat-top .border-right:after {
        background-color: rgba(255, 255, 255, 0.2);
    }

    .themesflat-top,
    .info-top-right,
    .themesflat-top a,
    .themesflat-top .themesflat-socials li a {
        color: #ffffff;
    }

    .themesflat_header_wrap.header-style1,
    .nav.header-style2,
    .themesflat_header_wrap.header-style3,
    .nav.header-style4,
    .header.widget-header .nav {
        background-color: #fff;
    }

    #mainnav>ul>li>a {
        color: #595959;
    }

    #mainnav>ul>li>a:hover,
    #mainnav>ul>li.current-menu-item>a {
        color: #377493 !important;
    }

    #mainnav ul.sub-menu>li>a {
        color: #ffffff !important;
    }

    #mainnav ul.sub-menu {
        background-color: #377493;
    }

    #mainnav ul.sub-menu>li>a:hover {
        background-color: #aac9ce !important;
    }

    #mainnav ul.sub-menu>li {
        border-color: #dde9eb !important;
    }

    .footer_background:before {
        background-color: #21536c;
    }

    .footer a,
    .footer,
    .themesflat-before-footer .custom-info>div,
    .footer-widgets ul li a,
    .footer-widgets .company-description p {
        color: #f9f9f9;
    }

    .bottom {
        background-color: #337493;
    }

    .bottom .copyright p,
    .bottom #menu-bottom li a {
        color: #e5e5e5;
    }

    .white #Financial_Occult text,
    .white #F__x26__O tspan {
        fill: #fff;
    }

    .parsley-type, .parsley-required, .parsley-pattern {
    	color: red;
    }
    </style>
    <!-- <link rel='stylesheet' id='js_composer_front-css'  href='/wp-content/plugins/js_composer/assets/css/js_composer.min.css?version=6265' type='text/css' media='all' /> -->
    <link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qhynyhyt/4b9n.css" media="all" />
    <link rel='https://api.w.org/' href='/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.8" />
    <link rel='shortlink' href='/' />
    <link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2F" />
    <link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2F&#038;format=xml" />
    <meta name="generator" content="qTranslate-X 3.4.6.8" />
    <link hreflang="x-default" href="/" rel="alternate" />
    <link hreflang="vi" href="/" rel="alternate" />
    <link hreflang="en" href="/en" rel="alternate" />
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?version=4097" media="screen"><![endif]-->
    <meta name="generator" content="Powered by Slider Revolution 5.4.6.3.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/10/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="/wp-content/uploads/2018/10/cropped-favicon-270x270.png" />
    <style type="text/css" id="wp-custom-css">
    .page-wrap.sidebar-left #primary.content-area {
        /*     margin-top: -40px; */
    }

    .single-post .page-wrap.sidebar-left #primary.content-area {
        margin-top: 0;
    }

    .themesflat-portfolio .item .featured-post {
        text-align: center;
    }

    .themesflat-portfolio .item .featured-post img,
    .content-area.fullwidth .themesflat_iconbox.inline-left img {
        height: 220px;
        width: 100%;
        object-fit: cover;
    }

    /* .content-area.fullwidth .themesflat_iconbox.transparent .title {
height: 55px;
display: flex;
align-items: center;
margin-top: 0;
}
.content-area.fullwidth .themesflat_iconbox.transparent .iconbox-content p:nth-child(2) {
height: 125px;
overflow: hidden;
}
*/
    .header.header-sticky .wpmenucartli {
        display: none !important;
    }

    .header.header-sticky #menu-main li:first-child {
        display: inline-block !important;
    }

    #mainnav>ul>li {
        margin-left: 25px !important;
    }
    </style>
    <style type="text/css" data-type="vc_custom-css">
    ul.themesflat_iconlist .list-title a:hover {
        color: #3d9be9;
    }

    .themesflat-boxed .wpcf7 .custom_form input {
        color: #fff;
    }

    @media (min-width: 1200px) {
        .rev_slider .tp-bgimg.defaultimg {
            /*background-size: 100% 100% !important;*/
        }
    }

    @media only screen and (max-width: 480px) {
        #down-slider-home {
            /*margin-top: -15px;*/
            padding-top: 17px !important;
        }

        .rev_slider_wrapper .rev_slider .tp-revslider-mainul li:first-child .tp-bgimg.defaultimg {
            margin-top: 0 !important;
        }
    }
    </style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
    .vc_custom_1561690547007 {
        padding-top: 70px !important;
        padding-bottom: 24px !important;
    }

    .vc_custom_1533543314404 {
        padding-top: 5px !important;
        padding-bottom: 40px !important;
        background-color: #eeeeee !important;
    }

    .vc_custom_1492229778169 {
        padding-top: 55px !important;
        padding-bottom: 90px !important;
    }

    .vc_custom_1561690359692 {
        padding-top: 70px !important;
        padding-bottom: 24px !important;
    }

    .vc_custom_1533543314404 {
        padding-top: 5px !important;
        padding-bottom: 40px !important;
        background-color: #eeeeee !important;
    }

    .vc_custom_1492229778169 {
        padding-top: 55px !important;
        padding-bottom: 90px !important;
    }

    .vc_custom_1591266895665 {
        padding-top: 0px !important;
        padding-right: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 0px !important;
    }

    .vc_custom_1591267127301 {
        padding-top: 0px !important;
        padding-right: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 0px !important;
    }

    .vc_custom_1534922985607 {
        margin-top: -50px !important;
        padding-top: 70px !important;
        padding-bottom: 30px !important;
    }

    .vc_custom_1534923083880 {
        margin-top: -50px !important;
    }

    .vc_custom_1553596379844 {
        padding-top: 0px !important;
        padding-right: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 0px !important;
    }

    .vc_custom_1553676142269 {
        padding-top: 0px !important;
        padding-right: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 0px !important;
    }

    .vc_custom_1491300752570 {
        padding-top: 70px !important;
        padding-bottom: 30px !important;
    }

    .vc_custom_1534835085116 {
        margin-top: -70px !important;
    }
    </style><noscript>
        <style type="text/css">
        .wpb_animate_when_almost_visible {
            opacity: 1;
        }
        </style>
    </noscript>
    <script data-wpfc-render="false">var Wpfcll={s:[],osl:0,scroll:false,i:function(){Wpfcll.ss();window.addEventListener('load',function(){window.addEventListener("DOMSubtreeModified",function(e){Wpfcll.osl=Wpfcll.s.length;Wpfcll.ss();if(Wpfcll.s.length > Wpfcll.osl){Wpfcll.ls(false);}},false);Wpfcll.ls(true);});window.addEventListener('scroll',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('resize',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('click',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});},c:function(e,pageload){var w=document.documentElement.clientHeight || body.clientHeight;var n=0;if(pageload){n=0;}else{n=(w > 800) ? 800:200;n=Wpfcll.scroll ? 800:n;}var er=e.getBoundingClientRect();var t=0;var p=e.parentNode;if(typeof p.getBoundingClientRect=="undefined"){var pr=false;}else{var pr=p.getBoundingClientRect();}if(er.x==0 && er.y==0){for(var i=0;i < 10;i++){if(p){if(pr.x==0 && pr.y==0){p=p.parentNode;if(typeof p.getBoundingClientRect=="undefined"){pr=false;}else{pr=p.getBoundingClientRect();}}else{t=pr.top;break;}}};}else{t=er.top;}if(w - t+n > 0){return true;}return false;},r:function(e,pageload){var s=this;var oc,ot;try{oc=e.getAttribute("data-wpfc-original-src");ot=e.getAttribute("data-wpfc-original-srcset");if(s.c(e,pageload)){if(oc || ot){if(e.tagName=="DIV" || e.tagName=="A"){e.style.backgroundImage="url("+oc+")";e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");}else{if(oc){e.setAttribute('src',oc);}if(ot){e.setAttribute('srcset',ot);}e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");if(e.tagName=="IFRAME"){e.onload=function(){if(typeof window.jQuery !="undefined"){if(jQuery.fn.fitVids){jQuery(e).parent().fitVids({customSelector:"iframe[src]"});}}var s=e.getAttribute("src").match(/templates\/youtube\.html\#(.+)/);var y="https://www.youtube.com/embed/";if(s){try{var i=e.contentDocument || e.contentWindow;if(i.location.href=="about:blank"){e.setAttribute('src',y+s[1]);}}catch(err){e.setAttribute('src',y+s[1]);}}}}}}else{if(e.tagName=="NOSCRIPT"){if(jQuery(e).attr("data-type")=="wpfc"){e.removeAttribute("data-type");jQuery(e).after(jQuery(e).text());}}}}}catch(error){console.log(error);console.log("==>",e);}},ss:function(){var i=Array.prototype.slice.call(document.getElementsByTagName("img"));var f=Array.prototype.slice.call(document.getElementsByTagName("iframe"));var d=Array.prototype.slice.call(document.getElementsByTagName("div"));var a=Array.prototype.slice.call(document.getElementsByTagName("a"));var n=Array.prototype.slice.call(document.getElementsByTagName("noscript"));this.s=i.concat(f).concat(d).concat(a).concat(n);},ls:function(pageload){var s=this;[].forEach.call(s.s,function(e,index){s.r(e,pageload);});}};document.addEventListener('DOMContentLoaded',function(){wpfci();});function wpfci(){Wpfcll.i();}</script>
</head>

<body class="home page-template page-template-tpl page-template-front-page page-template-tplfront-page-php page page-id-262  has-topbar header_sticky wide sidebar-left bottom-center wpb-js-composer js-comp-ver-5.4.7 vc_responsive vi">
    <div class="themesflat-boxed">
        <!-- Preloader -->
        <div class="preloader">
            <div class="clear-loading loading-effect-2">
                <span></span>
            </div>
        </div>
        <!-- Top -->
        <div class="themesflat-top header-style1">
            <div class="container">
                <div class="container-inside">
                    <div class="content-left">
                        <ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-chooser">
                            <li class="lang-vi active"><a href="/vi" hreflang="vi" title="VI (vi)" class="qtranxs_image qtranxs_image_vi"><img src="/wp-content/plugins/qtranslate-x/flags/vn.png" alt="VI (vi)" /><span style="display:none">VI</span></a></li>
                            <li class="lang-en"><a href="/en" hreflang="en" title="EN (en)" class="qtranxs_image qtranxs_image_en"><img src="/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN (en)" /><span style="display:none">EN</span></a></li>
                        </ul>
                        <div class="qtranxs_widget_end"></div>
                        <ul>
                            <li class="border-right">
                                <i class="fa fa-phone"></i><a href="tel:0247 1080 285" target="_top"> (+84) 0247 1080 285</a>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i> <a href="mailto:info@vinsofts.com" target="_top"> info@vinsofts.com</a>
                            </li>
                        </ul>
                    </div><!-- /.col-md-7 -->
                    <div class="content-right">
                        <ul class="themesflat-socials">
                            <li class="facebook">
                                <a class="title" href="">
                                    <i class="fa fa-facebook"></i>
                                    <span>Facebook</span>
                                </a>
                            </li>
                            <li class="youtube">
                                <a class="title" href="">
                                    <i class="fa fa-youtube"></i>
                                    <span>Youtube</span>
                                </a>
                            </li>
                            <li class="linkedin">
                                <a class="title" href="/linkedin.com/company/vinsofts" target="_blank" rel="alternate" title="LinkedIn">
                                    <i class="fa fa-linkedin"></i>
                                    <span>LinkedIn</span>
                                </a>
                            </li>
                        </ul><!-- /.social -->
                        <div class="info-top-right border-left">
                            <span><i class="fa fa-question-circle"></i>Bạn cần tư vấn?</span>
                            <a class="appoinment" href="#">Yêu cầu báo giá</a>
                        </div>
                    </div><!-- /.col-md-5 -->
                </div><!-- /.container -->
            </div><!-- /.container -->
        </div><!-- /.top -->
        <div class="popup">
            <div id="myModal2" class="modal fade in" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content form contact-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <div role="form" class="wpcf7" id="wpcf7-f3777-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate" data-parsley-validate>
                                    <!-- data-parsley-validate -->
                                    <div class="row gutter-10 contactform4">
                                        <h4 class="title-form" style="display:none">Quý khách vui lòng điền form bên dưới và gửi cho chúng tôi. Chúng tôi sẽ liên hệ lại ngay.</h4>
                                        <div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name">
                                                <input type="text" name="your-name" required="required" data-parsley-trigger="change focusout" data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" placeholder="Họ tên *" /></span>
                                        </div>
                                        <div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email">
                                                <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" required="required" data-parsley-trigger="change focusout" data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" data-parsley-pattern="^[a-z A-Z].*@.+" data-parsley-pattern-message="Email không đúng định dạng" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
                                        <div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone">
                                                <input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" data-parsley-trigger="change focusout" required="required" data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" data-parsley-pattern="[0-9]{10,11}" data-parsley-pattern-message="SĐT không đúng định dạng (VD: 0912345678, +84912345678)" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" /></span> </div>
                                        <div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Câu hỏi" /></span> </div>
                                        <div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can">
                                                <select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false">
                                                    <option value="Bạn quan tâm đến dịch vụ nào?">Bạn quan tâm đến dịch vụ nào?</option>
                                                    <option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option>
                                                    <option value="Phát triển ứng dụng web, thiết kế website">Phát triển ứng dụng web, thiết kế website</option>
                                                    <option value="Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin">Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin</option>
                                                    <option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option>
                                                    <option value="Mua Phần mềm quản trị doanh nghiệp (ERP)">Mua Phần mềm quản trị doanh nghiệp (ERP)</option>
                                                    <option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option>
                                                    <option value="Thuê nhân sự cho dự án">Thuê nhân sự cho dự án</option>
                                                </select></span></div>
                                        <div class="col-sm-6">
                                            <span>Đính kèm file:</span><br />
                                            <span class="wpcf7-form-control-wrap file-978"><input onChange="uploadFileToServer()" type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" />
                                        </div>
                                        <div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung chi tiết"></textarea></span> </div>
                                        <div class="col-sm-2 col-xs-12"><input type="submit" value="GỬI" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Form contact-->
            <div id="myModal3" class="modal fade in" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content form contact-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <div role="form" class="wpcf7" id="wpcf7-f3777-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/#wpcf7-f3777-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="3777" />
                                        <input type="hidden" name="_wpcf7_version" value="5.0.5" />
                                        <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3777-o1" />
                                        <input type="hidden" name="_wpcf7_container_post" value="0" />
                                    </div>
                                    <div class="row gutter-10 contactform4">
                                        <h4 class="title-form" style="display:none">Quý khách vui lòng điền form bên dưới và gửi cho chúng tôi. Chúng tôi sẽ liên hệ lại ngay.</h4>
                                        <div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ tên *" /></span></div>
                                        <div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
                                        <div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" /></span> </div>
                                        <div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Câu hỏi" /></span> </div>
                                        <div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can"><select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false">
                                                    <option value="Bạn quan tâm đến dịch vụ nào?">Bạn quan tâm đến dịch vụ nào?</option>
                                                    <option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option>
                                                    <option value="Phát triển ứng dụng web, thiết kế website">Phát triển ứng dụng web, thiết kế website</option>
                                                    <option value="Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin">Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin</option>
                                                    <option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option>
                                                    <option value="Mua Phần mềm quản trị doanh nghiệp (ERP)">Mua Phần mềm quản trị doanh nghiệp (ERP)</option>
                                                    <option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option>
                                                    <option value="Thuê nhân sự cho dự án">Thuê nhân sự cho dự án</option>
                                                </select></span></div>
                                        <div class="col-sm-6">
                                            <span>Đính kèm file:</span><br />
                                            <span class="wpcf7-form-control-wrap file-978"><input type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" /></span>
                                        </div>
                                        <div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung chi tiết"></textarea></span> </div>
                                        <div class="col-sm-10 col-xs-12 captcha-form-vins">
                                            <div class="wpcf7-form-control-wrap">
                                                <div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
                                                <noscript>
                                                    <div style="width: 302px; height: 422px;">
                                                        <div style="width: 302px; height: 422px; position: relative;">
                                                            <div style="width: 302px; height: 422px; position: absolute;">
                                                                <iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
                                                                </iframe>
                                                            </div>
                                                            <div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                                                                <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </noscript>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12"><input type="submit" value="GỬI" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="popup-team-member" class="popup-2">
            <div class="modal fade" id="myModa1" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <div class="row">
                                <div class="image-members-full col-md-4 col-sm-6 col-xs-12">
                                    <div class="image">
                                    </div>
                                </div>
                                <div class="title-modal col-md-8 col-sm-6 col-xs-12">
                                    <div class="title">
                                        <h3 class="modal-title"></h3>
                                        <span class="modal-title-mini"></span>
                                        <div class="social-content">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="line-color"></div>
                        <div class="modal-body">
                            <div class="content-member">
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
        <style type="text/css">
        .themesflat-top .content-left {
            float: none;
        }

        .themesflat-top .content-left ul {
            float: left;
        }

        .themesflat-top .content-left ul.language-chooser {
            /*margin-right: 15px;*/
        }

        .themesflat-top .content-left ul.language-chooser li {
            padding-left: 0px;
            padding-right: 10px;
        }

        .themesflat-top .content-left ul.language-chooser li a::after {
            content: "";
            border-right: 1px solid #d8d8d8;
            margin-left: 6px;
        }

        .themesflat-top .content-left ul.language-chooser li:last-child a::after {
            border: 0px;
            content: "";
        }

        .themesflat-top .content-left ul.language-chooser li.active a,
        .themesflat-top .content-left ul.language-chooser li a:hover {
            color: #3d9be9;
            opacity: 1;
        }

        .themesflat-top .content-left ul.language-chooser li a {
            padding: 0px;
            font-size: 13px;
            color: #ffffff94;
            opacity: 0.4;
        }

        .themesflat-top .content-left ul.language-chooser li a img {
            width: 22px;
            height: 14px;
        }

        /*.themesflat-top .content-left ul.language-chooser li:nth-child(2) a img{
height: 16px;
}*/
        </style>
        <style type="text/css">
        .popup #myModal2 {
            z-index: 99999;
        }
        </style>
        <div class="themesflat_header_wrap header-style1" data-header_style="header-style1">
            <!-- Header -->
            <header id="header" class="header header-style1">
                <div class="container nav">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="header-wrap clearfix">
                                <div id="logo" class="logo">
                                    <a href="/" title="Công ty phần mềm Vinsofts">
                                        <img class="site-logo" src="/wp-content/uploads/2019/06/Logo-Vinsofts.png" alt="Công ty phần mềm Vinsofts" data-retina="/wp-content/uploads/2019/06/Logo-Vinsofts.png" />
                                    </a>
                                </div>
                                <div class="show-search">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                                <div class="nav-wrap">
                                    <div class="btn-menu">
                                        <span></span>
                                    </div><!-- //mobile menu button -->
                                    <nav id="mainnav" class="mainnav" role="navigation">
                                        <ul id="menu-main" class="menu">
                                            <li id="menu-item-2167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2167"><a href="/ve-chung-toi/">Về chúng tôi</a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-3792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3792"><a href="/ve-chung-toi/">Giới thiệu chung</a></li>
                                                    <li id="menu-item-3789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3789"><a href="/lich-su-cong-ty/">Lịch sử công ty</a></li>
                                                    <li id="menu-item-3790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3790"><a href="/doi-tac/">Đối tác</a></li>
                                                    <li id="menu-item-3791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3791"><a href="/nhan-su/">Nhân sự</a></li>
                                                    <li id="menu-item-6972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6972"><a href="/ho-so-nang-luc/">Hồ sơ năng lực</a></li>
                                                    <li id="menu-item-6108" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6108"><a href="/danh-muc/hoat-dong-noi-bat/">Hoạt động công ty</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-item-2994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2994"><a href="/dich-vu/">Dịch vụ</a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-2933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2933"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
                                                    <li id="menu-item-2934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2934"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
                                                    <li id="menu-item-2932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2932"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
                                                    <li id="menu-item-2935" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2935"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
                                                    <li id="menu-item-2943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2943"><a href="/kiem-thu-phan-mem-va-dich-vu-dam-bao-chat-luong/">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</a></li>
                                                    <li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="/kiem-tra-tu-dong/">Kiểm tra tự động</a></li>
                                                    <li id="menu-item-2942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2942"><a href="/trung-tam-phat-trien-phan-mem-offshore/">Trung tâm phát triển phần mềm offshore</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3202"><a href="/du-an-tieu-bieu/">Dự án tiêu biểu</a></li>
                                            <li id="menu-item-4112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4112"><a href="/jobs">Tuyển dụng</a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-4912" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4912"><a href="/job_cats/lap-trinh-mobile/">Lập trình Mobile</a></li>
                                                    <li id="menu-item-4911" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4911"><a href="/job_cats/lap-trinh-web/">Lập trình Web</a></li>
                                                    <li id="menu-item-4913" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4913"><a href="/job_cats/lap-trinh-blockchain/">Lập trình Blockchain</a></li>
                                                    <li id="menu-item-4810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a href="/ung-tuyen/">Ứng tuyển ngay</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="/tin-tuc/">Tin tức</a></li>
                                            <li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="/lien-he/">Liên hệ</a></li>
                                        </ul>
                                        <div class="info-top-right border-left" style="display: none">
                                            <a class="appoinment" href="#" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Yêu cầu Báo giá</a>
                                            <a class="hotline-header" href="tel:(+84) 0247 1080 285">
                                                <i class="fa fa-phone" aria-hidden="true"></i> <span>(+84) 0247 1080 285</span>
                                            </a>
                                            <i></i>
                                        </div>
                                    </nav><!-- #site-navigation -->
                                </div><!-- /.nav-wrap -->
                            </div><!-- /.header-wrap -->
                            <div class="submenu top-search widget_search">
                                <form role="search" method="get" class="search-form" action="/">
                                    <label>
                                        <span class="screen-reader-text">Tìm kiếm cho:</span>
                                        <input type="search" class="search-field" placeholder="Tìm kiếm &hellip;" value="" name="s" />
                                    </label>
                                    <input type="submit" class="search-submit" value="Tìm kiếm" />
                                </form>
                            </div>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </header><!-- /.header -->
        </div> <!-- Page Title -->
        <div class="clearfix"></div>
        <div id="content" class="page-wrap sidebar-left">
            <div class="container content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <div id="primary" class="content-area-front-page">
                            <main id="main" class="site-main" role="main">
                                <div class="entry-content">
                                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid themesflat_1596040514 vc_row-no-padding">
                                        <div class="row_overlay" style=""></div>
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                                                        <!-- START REVOLUTION SLIDER 5.4.6.3.1 fullwidth mode -->
                                                        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3.1">
                                                            <ul>
                                                                <!-- SLIDE  -->
                                                                <li data-index="rs-26" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="/wp-content/uploads/2020/06/Banner3-min-1-100x50.png" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/06/Banner3-min-1.png" alt="blank" title="Banner3-min" width="1920" height="630" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->
                                                                </li>
                                                                <!-- SLIDE  -->
                                                                <li data-index="rs-22" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="/wp-content/uploads/2020/06/vi-2-min-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/06/vi-2-min.jpg" alt="blank" title="vi-2-min" width="1920" height="630" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->
                                                                </li>
                                                                <!-- SLIDE  -->
                                                                <li data-index="rs-4" data-transition="fade,zoomin" data-slotamount="default,7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,Power4.easeInOut" data-easeout="default,Power4.easeInOut" data-masterspeed="default,2000" data-thumb="/wp-content/uploads/2019/06/mobile-vi-2-min-100x50.jpg" data-rotate="0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/06/mobile-vi-2-min.jpg" alt="blank" title="mobile-vi-2-min" width="1920" height="702" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->
                                                                    <!-- LAYER NR. 1 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-1" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['152','152','152','60']" data-fontsize="['43','43','43','36']" data-lineheight="['22','22','22','47']" data-width="['563','563','563','435']" data-height="['29','29','29','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power4.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 563px; max-width: 563px; max-width: 29px; max-width: 29px; white-space: nowrap; font-size: 43px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;">Phát triển ứng dụng di động </div>
                                                                    <!-- LAYER NR. 2 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-2" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['286','286','286','234']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Hơn 8 năm kinh nghiệm </div>
                                                                    <!-- LAYER NR. 3 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-3" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['241','241','241','189']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Đội ngũ hơn 40 LTV có trình độ cao </div>
                                                                    <!-- LAYER NR. 4 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-4" data-x="['left','left','left','center']" data-hoffset="['35','35','35','1']" data-y="['top','top','top','top']" data-voffset="['331','331','331','275']" data-fontsize="['22','22','22','20']" data-lineheight="['22','22','22','28']" data-width="['1029','1029','1029','435']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:-200px;skX:85px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 1029px; max-width: 1029px; white-space: normal; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Sử dụng công nghệ mới nhất (Swift, Kotlin, React Native, ObjC, Java, Xamarin, ...) </div>
                                                                    <!-- LAYER NR. 5 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-5" data-x="['left','left','left','center']" data-hoffset="['33','33','33','0']" data-y="['top','top','top','top']" data-voffset="['376','376','376','345']" data-fontsize="['22','22','22','20']" data-lineheight="['22','22','22','25']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Hơn 500 ứng dụng đã triển khai thành công lên các App Store </div>
                                                                    <!-- LAYER NR. 6 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-4-layer-6" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['421','421','421','410']" data-fontsize="['22','22','22','20']" data-lineheight="['22','22','22','25']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Làm hài lòng hơn 100 khách hàng trên khắp thế giới </div>
                                                                </li>
                                                                <!-- SLIDE  -->
                                                                <li data-index="rs-10" data-transition="fade,zoomin" data-slotamount="default,7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,Power4.easeInOut" data-easeout="default,Power4.easeInOut" data-masterspeed="default,2000" data-thumb="/wp-content/uploads/2019/06/web-vi-min-100x50.jpg" data-rotate="0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/06/web-vi-min.jpg" alt="blank" title="web-vi-min" width="1200" height="439" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->
                                                                    <!-- LAYER NR. 7 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-10-layer-1" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['152','152','152','51']" data-fontsize="['43','43','43','36']" data-lineheight="['22','22','22','47']" data-width="['none','none','none','435']" data-height="['29','29','29','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power4.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; max-width: 29px; max-width: 29px; white-space: nowrap; font-size: 43px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;">Phát triển ứng dụng web, thương mại điện tử </div>
                                                                    <!-- LAYER NR. 8 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-10-layer-2" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['241','241','241','181']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','434']" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Hơn 10 năm kinh nghiệm </div>
                                                                    <!-- LAYER NR. 9 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-10-layer-3" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['286','286','286','220']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Đội ngũ hơn 60 LTV có trình độ cao </div>
                                                                    <!-- LAYER NR. 10 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-10-layer-4" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['331','331','331','257']" data-fontsize="['22','22','22','20']" data-lineheight="['22','22','22','27']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Sử dụng công nghệ mới nhất trong quy trình phát triển ứng dụng web </div>
                                                                    <!-- LAYER NR. 11 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-10-layer-5" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['376','376','376','323']" data-fontsize="['22','22','22','20']" data-lineheight="['22','22','22','27']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Đã triển khai hơn 1000 website thành công cho khách hàng </div>
                                                                    <!-- LAYER NR. 12 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-10-layer-6" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['421','421','421','391']" data-fontsize="['22','22','22','20']" data-lineheight="['22','22','22','27']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Làm hài lòng hơn 500 khách hàng trên khắp thế giới </div>
                                                                </li>
                                                                <!-- SLIDE  -->
                                                                <li data-index="rs-8" data-transition="fade,zoomin" data-slotamount="default,7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,Power4.easeInOut" data-easeout="default,Power4.easeInOut" data-masterspeed="default,2000" data-thumb="/wp-content/uploads/2019/06/yc-min-100x50.jpg" data-rotate="0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/06/yc-min.jpg" alt="blank" title="yc-min" width="1200" height="439" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->
                                                                    <!-- LAYER NR. 13 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-8-layer-1" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['152','152','152','74']" data-fontsize="['43','43','43','36']" data-lineheight="['22','22','22','47']" data-width="['none','none','none','434']" data-height="['29','29','29','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power4.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; max-width: 29px; max-width: 29px; white-space: nowrap; font-size: 43px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;">Phát triển phần mềm theo yêu cầu </div>
                                                                    <!-- LAYER NR. 14 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-8-layer-3" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['241','241','241','199']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Phát triển phần mềm tương thích với hệ thống sẵn có của doanh nghiệp </div>
                                                                    <!-- LAYER NR. 15 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-8-layer-4" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['286','286','286','258']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Bảo trì và đảm bảo tích hợp liên tục cho hệ thống phần mềm doanh nghiệp </div>
                                                                    <!-- LAYER NR. 16 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-8-layer-5" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['331','331','331','321']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Tư vấn và cung cấp dịch vụ trọn gói về phần mềm cho doanh nghiệp </div>
                                                                    <!-- LAYER NR. 17 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-8-layer-6" data-x="['left','left','left','center']" data-hoffset="['35','35','35','0']" data-y="['top','top','top','top']" data-voffset="['376','376','376','381']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Triển khai các ứng dụng ERP và mini ERP cho quản trị doanh nghiệp </div>
                                                                </li>
                                                                <!-- SLIDE  -->
                                                                <li data-index="rs-5" data-transition="fade,zoomin" data-slotamount="default,7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,Power4.easeInOut" data-easeout="default,Power4.easeInOut" data-masterspeed="default,2000" data-thumb="/wp-content/uploads/2019/06/block-vi-2-min-100x50.jpg" data-rotate="0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/06/block-vi-2-min.jpg" alt="blank" title="block-vi-2-min" width="1200" height="439" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->
                                                                    <!-- LAYER NR. 18 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-5-layer-2" data-x="['left','left','left','left']" data-hoffset="['35','35','35','28']" data-y="['top','top','top','top']" data-voffset="['187','187','187','139']" data-fontsize="['43','43','43','36']" data-lineheight="['22','22','22','47']" data-width="['972','972','972','434']" data-height="['29','29','29','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power4.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; min-width: 972px; max-width: 972px; max-width: 29px; max-width: 29px; white-space: nowrap; font-size: 43px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;">Tư vấn & phát triển ứng dụng blockchain </div>
                                                                    <!-- LAYER NR. 19 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-5-layer-3" data-x="['left','left','left','left']" data-hoffset="['35','35','35','28']" data-y="['top','top','top','top']" data-voffset="['276','276','276','266']" data-fontsize="['22','22','22','20']" data-width="['none','none','none','435']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Phát triển ứng dụng blockchain cho ICO, viết Dapp trên nền Ethereum/Solidiy, Stellar, ... </div>
                                                                    <!-- LAYER NR. 20 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-5-layer-6" data-x="['left','left','left','left']" data-hoffset="['35','35','35','28']" data-y="['top','top','top','top']" data-voffset="['321','321','321','339']" data-fontsize="['22','22','22','20']" data-width="['1020','1020','1020','435']" data-height="['28','28','28','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; min-width: 1020px; max-width: 1020px; max-width: 28px; max-width: 28px; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Phát triển ứng dụng blockchain cho doanh nghiệp, ngân hàng trên nền Hyperledger </div>
                                                                    <!-- LAYER NR. 21 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-5-layer-7" data-x="['left','left','left','left']" data-hoffset="['35','35','35','30']" data-y="['top','top','top','top']" data-voffset="['367','367','367','412']" data-fontsize="['22','22','22','20']" data-width="['1020','1020','1020','434']" data-height="['28','28','28','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 1020px; max-width: 1020px; max-width: 28px; max-width: 28px; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Tư vấn, tích hợp blockchain vào hệ thống phần mềm hiện tại của doanh nghiệp </div>
                                                                    <!-- LAYER NR. 22 -->
                                                                    <div class="tp-caption   tp-resizeme" id="slide-5-layer-8" data-x="['left','left','left','left']" data-hoffset="['35','35','35','33']" data-y="['top','top','top','top']" data-voffset="['409','409','409','472']" data-fontsize="['22','22','22','20']" data-width="['1020','1020','1020','434']" data-height="['28','28','28','none']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":0,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; min-width: 1020px; max-width: 1020px; max-width: 28px; max-width: 28px; white-space: nowrap; font-size: 22px; line-height: 22px; font-weight: 500; color: #3a526a; letter-spacing: 0px;font-family:Roboto;"><i class="fa-icon-check-square-o"></i> Tư vấn xây dựng và phát triển ứng dụng DLT trên nền DAG hay Hashgraph </div>
                                                                </li>
                                                            </ul>
                                                            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                                        </div>
                                                    </div><!-- END REVOLUTION SLIDER -->
                                                    <div class="themesflat-spacer" data-desktop="26.5" data-mobile="0" data-smobile="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row-full-width vc_clearfix"></div>
                                    <div id="down-slider-home" class="vc_row wpb_row vc_row-fluid vc_custom_1561690547007">
                                        <div class="row_overlay" style=""></div>
                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="themesflat_iconbox  inline-left transparent themesflat_1493341761">
                                                        <div class="iconbox-image"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/06/mobile-app.png" alt="PHÁT TRIỂN ỨNG DỤNG MOBILE" /></div>
                                                        <div class="iconbox-icon" style="width: px;height: px;background-color:">
                                                            <div class="icon"><span class="typcn typcn-device-phone"></span></div>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h5 class="title" style="font-size: px;font-weight:">
                                                                <a href="/phat-trien-ung-dung-mobile/">PHÁT TRIỂN ỨNG DỤNG MOBILE</a>
                                                            </h5>
                                                            <p style="text-align: justify;">Thiết kế Mobile App chuyên biệt với mục tiêu mang đến trải nghiệm tối ưu, ấn tượng cho người dùng, thông tin được truyền tải hiệu quả nhất.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="themesflat_iconbox  inline-left transparent  vc_custom_1591266895665">
                                                        <div class="iconbox-image"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/06/website-1024x637-1024x637.jpg" alt="PHÁT TRIỂN ỨNG DỤNG WEB" /></div>
                                                        <div class="iconbox-icon" style="width: px;height: px;background-color:">
                                                            <div class="icon"><span class="typcn typcn-device-laptop"></span></div>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h5 class="title" style="font-size: px;font-weight:">
                                                                <a href="/phat-trien-ung-dung-web/">PHÁT TRIỂN ỨNG DỤNG WEB</a>
                                                            </h5>
                                                            <p style="text-align: justify;">Công nghệ thiết kế web tùy biến, ngôn ngữ lập trình tối ưu dễ thay đổi bố cục các khối thông tin giúp website linh hoạt với mọi thiết bị.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="themesflat_iconbox  inline-left transparent themesflat_1493341761">
                                                        <div class="iconbox-image"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/06/csd2-e1553501916731-1024x637-min.png" alt="PHÁT TRIỂN PHẦN MỀM QUẢN LÝ" /></div>
                                                        <div class="iconbox-icon" style="width: px;height: px;background-color:">
                                                            <div class="icon"><span class="fa fa-cogs"></span></div>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h5 class="title" style="font-size: px;font-weight:">
                                                                <a href="/phat-trien-phan-mem-quan-ly/">PHÁT TRIỂN PHẦN MỀM QUẢN LÝ</a>
                                                            </h5>
                                                            <p style="text-align: justify;">Phần mềm quản lý doanh nghiệp như phần mềm kế toán, phần mềm quản lý kho, quản lý nhân sự, quản lý khách hàng được tối ưu cho nhu cầu từng doanh nghiệp.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-3">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="themesflat_iconbox  inline-left transparent  vc_custom_1591267127301">
                                                        <div class="iconbox-image"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/06/blockchain-2-1024x637.jpg" alt="PHÁT TRIỂN ỨNG DỤNG BLOCKCHAIN" /></div>
                                                        <div class="iconbox-icon" style="width: px;height: px;background-color:">
                                                            <div class="icon"><span class="fa fa-cubes"></span></div>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h5 class="title" style="font-size: px;font-weight:">
                                                                <a href="/phat-trien-ung-dung-blockchain/">PHÁT TRIỂN ỨNG DỤNG BLOCKCHAIN</a>
                                                            </h5>
                                                            <p style="text-align: justify;">Công nghệ Blockchain đột phá giúp cho mọi dữ liệu khi giao dịch đều được lưu trữ, không thể sửa đổi, tối ưu tính chính xác trong quản lý.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row wpb_row vc_row-fluid themesflat_1534156349">
                                        <div class="row_overlay" style=""></div>
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner vc_custom_1534922985607">
                                                <div class="wpb_wrapper">
                                                    <div class="title-section magb-28  vc_custom_1534923083880">
                                                        <h1 class="title">
                                                            DỰ ÁN TIÊU BIỂU
                                                        </h1>
                                                    </div>
                                                    <div class="themesflat-portfolio clearfix">
                                                        <ul class="portfolio-filter">
                                                            <li class="active"><a data-filter="*" href="#">All</a></li>
                                                            <li><a data-filter=".blockchain-development" href="#" title="Lập trình Blockchain">Lập trình Blockchain</a></li>
                                                            <li><a data-filter=".mobile-development" href="#" title="Thiết kế ứng dụng Mobile App">Thiết kế ứng dụng Mobile App</a></li>
                                                            <li><a data-filter=".web-developement" href="#" title="Thiết kế ứng dụng Website">Thiết kế ứng dụng Website</a></li>
                                                        </ul>
                                                        <div class="portfolio-container  grid one-four show_filter_portfolio">
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/04/0.salon-đồng0-300x261.png" alt="Thiết kế mobile app ngành Salon tóc nữ &#8211; Salon Đồng"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app ngành Salon tóc nữ &#8211; Salon Đồng" href="/portfolios/thiet-ke-mobile-app-nganh-salon-toc-nu-salon-dong/">Thiết kế mobile app ngành Salon tóc nữ &#8211; Salon Đồng</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/04/0.dating1-300x274.png" alt="Thiết kế mobile app Kết bạn trực tuyến AROUNYA"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app Kết bạn trực tuyến AROUNYA" href="/portfolios/thiet-ke-mobile-app-ket-ban-truc-tuyen-arounya/">Thiết kế mobile app Kết bạn trực tuyến AROUNYA</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/04/0.shalala1-240x300.png" alt="Thiết kế mobile app sàn thương mại điện tử"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app sàn thương mại điện tử" href="/portfolios/thiet-ke-mobile-app-san-thuong-mai-dien-tu/">Thiết kế mobile app sàn thương mại điện tử</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/03/0run-team1-300x265.png" alt="Thiết kế mobile app Team Building &#8211; Run Team"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app Team Building &#8211; Run Team" href="/portfolios/thiet-ke-mobile-app-team-building-run-team/">Thiết kế mobile app Team Building &#8211; Run Team</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/03/0.por_.1-300x284.png" alt="Thiết kế mobile app IoT &#8211; điều khiển thiết bị trong nhà bằng Smartphone"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app IoT &#8211; điều khiển thiết bị trong nhà bằng Smartphone" href="/portfolios/thiet-ke-mobile-app-iot-dieu-khien-thiet-bi-trong-nha-bang-smartphone/">Thiết kế mobile app IoT &#8211; điều khiển thiết bị trong nhà bằng Smartphone</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item blockchain-development mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/03/0.astra1_-300x234.png" alt="Thiết kế mobile app Mạng xã hội du lịch Astra"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/ung-dung-blockchain/" rel="tag">Lập trình Blockchain</a> / <a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app Mạng xã hội du lịch Astra" href="/portfolios/thiet-ke-mobile-app-mang-xa-hoi-du-lich-astra/">Thiết kế mobile app Mạng xã hội du lịch Astra</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2020/02/0.MXH0_-196x300.png" alt="Thiết kế Mobile app Mạng xã hội thời trang"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế Mobile app Mạng xã hội thời trang" href="/portfolios/thiet-ke-mobile-app-mang-xa-hoi-thoi-trang/">Thiết kế Mobile app Mạng xã hội thời trang</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/10/1.-Bài-viết-siêu-thị-vlxd00-side-300x271.png" alt="Thiết kế mobile app siêu thị vật liệu xây dựng"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app siêu thị vật liệu xây dựng" href="/portfolios/thiet-ke-mobile-app-sieu-thi-vat-lieu-xay-dung/">Thiết kế mobile app siêu thị vật liệu xây dựng</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/10/1.Home-–-2-side-300x272.png" alt="Thiết kế mobile app học Tiếng Anh trên màn hình khoá"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app học Tiếng Anh trên màn hình khoá" href="/portfolios/thiet-ke-mobile-app-hoc-tieng-anh-tren-man-hinh-khoa/">Thiết kế mobile app học Tiếng Anh trên màn hình khoá</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/06/1.photo_2019-06-26_09-27-47-side-300x272.jpg" alt="Thiết kế mobile app ngành Giúp việc"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app ngành Giúp việc" href="/portfolios/thiet-ke-mobile-app-nganh-giup-viec/">Thiết kế mobile app ngành Giúp việc</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item web-developement">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/05/viewretreats2-300x300.png" alt="Thiết kế website ngành du lịch"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div>
                                                                        <div class="title-post"><a title="Thiết kế website ngành du lịch" href="/portfolios/thiet-ke-website-nganh-du-lich/">Thiết kế website ngành du lịch</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/04/hp102-4.jpg-300x300.png" alt="Ứng dụng di động quản lý hiệu quả quảng cáo"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Ứng dụng di động quản lý hiệu quả quảng cáo" href="/portfolios/ung-dung-di-dong-do-luong-hieu-qua-quang-cao/">Ứng dụng di động quản lý hiệu quả quảng cáo</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/04/IP.jpg-300x300.png" alt="Thiết kế mobile app &#8220;Uber trong ngành xây dựng&#8221;"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app &#8220;Uber trong ngành xây dựng&#8221;" href="/portfolios/thiet-ke-mobile-app-uber-trong-nganh-noi-that/">Thiết kế mobile app &#8220;Uber trong ngành xây dựng&#8221;</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/04/cover.jpg-300x300.png" alt="Thiết kế ứng dụng di động kết bạn"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế ứng dụng di động kết bạn" href="/portfolios/thiet-ke-ung-dung-di-dong-ket-ban/">Thiết kế ứng dụng di động kết bạn</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/04/Artboard-–-2-2-300x300.png" alt="Thiết kế mobile app đặt lịch dịch vụ Spa"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế mobile app đặt lịch dịch vụ Spa" href="/portfolios/thiet-ke-mobile-app-dat-lich-dich-vu-spa/">Thiết kế mobile app đặt lịch dịch vụ Spa</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item web-developement">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/01/Du-an-thiet-ke-web-du-lich-300x273.png" alt="Dự án thiết kế website du lịch"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div>
                                                                        <div class="title-post"><a title="Dự án thiết kế website du lịch" href="/portfolios/du-an-thiet-ke-website-du-lich/">Dự án thiết kế website du lịch</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item web-developement">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/01/Du-an-thiet-ke-web-to-chuc-su-kien-300x215.png" alt="Dự án thiết kế website chuyên sự kiện"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div>
                                                                        <div class="title-post"><a title="Dự án thiết kế website chuyên sự kiện" href="/portfolios/du-an-thiet-ke-website-chuyen-su-kien/">Dự án thiết kế website chuyên sự kiện</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item web-developement">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2019/01/Dự-án-thiết-kế-website-giới-thiệu-Hyhy-300x215.png" alt="Dự án thiết kế website giới thiệu Hy.Ly"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div>
                                                                        <div class="title-post"><a title="Dự án thiết kế website giới thiệu Hy.Ly" href="/portfolios/du-an-thiet-ke-website-gioi-thieu-hyly/">Dự án thiết kế website giới thiệu Hy.Ly</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item blockchain-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2018/10/JWC-300x300.png" alt="Dự án JWC Blockchain Ventures"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/ung-dung-blockchain/" rel="tag">Lập trình Blockchain</a></div>
                                                                        <div class="title-post"><a title="Dự án JWC Blockchain Ventures" href="/portfolios/du-an-jwc-blockchain-ventures/">Dự án JWC Blockchain Ventures</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2018/08/RBX-active.jpg-300x300.png" alt="Thiết kế app RBX active"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế app RBX active" href="/portfolios/thiet-ke-app-rbx-active/">Thiết kế app RBX active</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item web-developement">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2018/08/E-commerce-website-for-real-estate-300x300.png" alt="Thiết kế website thương mại điện tử cho bất động sản"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div>
                                                                        <div class="title-post"><a title="Thiết kế website thương mại điện tử cho bất động sản" href="/portfolios/thiet-ke-website-thuong-mai-dien-tu-cho-bat-dong-san/">Thiết kế website thương mại điện tử cho bất động sản</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2018/08/aspire-project-300x300.png" alt="Thiết kế app rèn luyện sức khỏe Aspire"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế app rèn luyện sức khỏe Aspire" href="/portfolios/thiet-ke-app-ren-luyen-suc-khoe-aspire/">Thiết kế app rèn luyện sức khỏe Aspire</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item web-developement">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2018/08/tourism-website-300x300.png" alt="Hệ thống xây dựng website du lịch"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div>
                                                                        <div class="title-post"><a title="Hệ thống xây dựng website du lịch" href="/portfolios/he-thong-xay-dung-website-du-lich/">Hệ thống xây dựng website du lịch</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item mobile-development">
                                                                <div class="wrap-border">
                                                                    <div class="featured-post"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="/wp-content/uploads/2018/08/shippy-300x300.png" alt="Thiết kế app Shippy"></div>
                                                                    <div class="portfolio-details">
                                                                        <div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div>
                                                                        <div class="title-post"><a title="Thiết kế app Shippy" href="/portfolios/thiet-ke-app-shippy/">Thiết kế app Shippy</a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5" data-vc-parallax-image="/wp-content/uploads/2019/06/mobile-vi-2-min.jpg" class="vc_row wpb_row vc_row-fluid themesflat_1538036645 vc_row-has-fill vc_row-o-content-middle vc_row-flex vc_general vc_parallax vc_parallax-content-moving">
                                        <div class="row_overlay" style=""></div>
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="themesflat-spacer" data-desktop="36" data-mobile="30" data-smobile="30">
                                                    </div>
                                                    <div class="wpb_text_column wpb_content_element  tb_needhelp1">
                                                        <div class="wpb_wrapper">
                                                            <div style="color: #fff; text-align: center;">
                                                                <p>&nbsp;</p>
                                                                <h3 style="font-size: 32px; margin-top: 25px; margin-bottom: 14px; color: #fff;"><span style="color: #424242;"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-full wp-image-7544" data-wpfc-original-src="/wp-content/uploads/2019/03/call-centre-man.png" alt="blank" width="56" height="56" /></span></h3>
                                                                <h3 style="font-size: 32px; margin-top: 25px; margin-bottom: 14px; color: #fff;"><span style="color: #424242;">Liên Hệ Ngay</span></h3>
                                                                <p style="font-size: 16px; line-height: 28px; letter-spacing: -0.06px; margin-bottom: 55px;"><span style="color: #424242;"><strong>Liên hệ Vinsofts ngay để được tư vấn và giải đáp mọi thắc mắc.</strong></span><br />
                                                                    <span style="color: #424242;"><strong>Điện thoại: 8:00 sáng &#8211; 6:00 chiều. Email: Trả lời trong vòng 2 giờ.</strong></span></p>
                                                                <p class="mpl-44"><a style="padding-left: 0px; padding-right: 10px; font-size: 25px; font-weight: 600; color: #3d9be9; border-right: 1px solid #d1d1d1;" href="tel:+842462593148"><span style="color: #377493;">(+84) 024 6259 3148</span></a><span style="color: #424242;"><strong><a style="font-size: 23px; color: #424242; padding-left: 10px; padding-right: 25px;" href="mailto:info@vinsofts.com">info@vinsofts.com</a></strong></span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="themesflat-spacer" data-desktop="55" data-mobile="30" data-smobile="30">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_row-full-width vc_clearfix"></div>
                                    <div class="vc_row wpb_row vc_row-fluid home_custom_1 vc_custom_1492229778169">
                                        <div class="row_overlay" style=""></div>
                                        <div class="wpb_column vc_column_container vc_col-sm-5">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div role="form" class="wpcf7" id="wpcf7-f644-p262-o2" lang="en-US" dir="ltr">
                                                        <div class="screen-reader-response"></div>
                                                        <form class="wpcf7-form" novalidate="novalidate" data-parsley-validate>
                                                            <div style="display: none;">
                                                                <input type="hidden" name="_wpcf7" value="644" />
                                                                <input type="hidden" name="_wpcf7_version" value="5.0.5" />
                                                                <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f644-p262-o2" />
                                                                <input type="hidden" name="_wpcf7_container_post" value="262" />
                                                            </div>
                                                            <div class="custom_form">
                                                                <h2 style="font-size:24px;">YÊU CẦU TƯ VẤN</h2>
                                                                <p>Vui lòng để lại thông tin ở form bên dưới. Đội ngũ tư vấn từ Vinsofts sẽ liên hệ với bạn trong thời gian sớm nhất.</p>
                                                                <p><span class="wpcf7-form-control-wrap how_can"><select name="how_can_1" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false">
                                                                            <option value="Dịch vụ cần tư vấn?">Dịch vụ cần tư vấn?</option>
                                                                            <option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option>
                                                                            <option value="Phát triển ứng dụng Web">Phát triển ứng dụng Web</option>
                                                                            <option value="Phát triển ứng dụng Mobile">Phát triển ứng dụng Mobile</option>
                                                                            <option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option>
                                                                            <option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option>
                                                                            <option value="Thuê nhân sự">Thuê nhân sự</option>
                                                                        </select></span><br />
                                                                    <span class="wpcf7-form-control-wrap your-name"><input required="required" type="text" name="your-name-1" data-parsley-trigger="change focusout" data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span><br />
                                                                    <span class="wpcf7-form-control-wrap your-phone"><input type="tel" data-parsley-trigger="change focusout" required="required" data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" data-parsley-pattern="[0-9]{10,11}" data-parsley-pattern-message="SĐT không đúng định dạng (VD: 0912345678, +84912345678)" name="your-phone-1" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span><br />
                                                                    <span class="wpcf7-form-control-wrap your-email"><input data-parsley-trigger="change focusout" data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" data-parsley-pattern="^[a-z A-Z].*@.+" data-parsley-pattern-message="Email không đúng định dạng" type="email" name="your-email-1" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-invalid="false" placeholder="Địa chỉ email" /></span></p>
                                                             
                                                                <p><button class="frm_yctuvan" type="submit" style="margin-top: 15px;">GỬI <i class="fa fa-chevron-right"></i></button>
                                                            </div>
                                                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                        </form>
                                                    </div>
                                                    <div class="themesflat-spacer" data-desktop="0" data-mobile="40" data-smobile="40">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_column vc_column_container vc_col-sm-7">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="title-section title_section_custom_1">
                                                        <h1 class="title">
                                                            Tin Tức
                                                        </h1>
                                                    </div>
                                                    <div class="blog-shortcode  blog-grid blog-two-columns" data-items="2">
                                                        <article class="entry format-">
                                                            <div class="entry-border">
                                                                <div class="featured-post post-home-page"><a href="/giai-phap-cho-doanh-nghiep-trong-mua-dich-covid-19/"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="600" height="350" data-wpfc-original-src="/wp-content/uploads/2020/04/0.Giai-phap-doan-nghiep-vuot-qua-dai-dich-Covid-19-600x350.png" class="attachment-themesflat-blog-grid size-themesflat-blog-grid wp-post-image" alt="blank" /></a></div>
                                                                <div class="entry-meta clearfix">
                                                                    <ul class="meta-left">
                                                                        <li class="post-date">
                                                                            20/04/2020 </li>
                                                                        <li class="post-author">
                                                                            <span class="author vcard">By<a class="url fn n" href="/author/tuoivu/" title="View all posts by Tươi Vũ" rel="author"> Tươi Vũ</a></span> </li>
                                                                        <li class="post-categories">In <a href="/danh-muc/phan-mem-quan-ly-doanh-nghiep-erp-2/" rel="category tag">Phần mềm quản lý doanh nghiệp (ERP)</a>, <a href="/danh-muc/thiet-ke-ung-dung-mobile-app/" rel="category tag">Thiết kế ứng dụng Mobile App</a></li>
                                                                    </ul>
                                                                    <ul class="meta-right">
                                                                        <li class="post-comments"><i class="fa fa-comment-o" aria-hidden="true"></i><span>Chức năng bình luận bị tắt<span class="screen-reader-text"> ở Giải pháp cho doanh nghiệp vượt qua đại dịch Covid-19</span></span></li>
                                                                    </ul>
                                                                </div><!-- /.entry-meta -->
                                                                <div class="content-post">
                                                                    <h3 class="entry-title"><a href="/giai-phap-cho-doanh-nghiep-trong-mua-dich-covid-19/">Giải pháp cho doanh nghiệp vượt qua đại dịch Covid-19</a></h3>
                                                                    <div class="entry-content hide">
                                                                        <p>1. Ảnh hưởng của Covid-19 tới các doanh nghiệp Đại dịch Covid-19 khởi nguồn từ Vũ Hán &#8211; Trung Quốc từ đầu tháng 12/2019, đến hết ngày 14/4/2020 đã lan ra tại 210 quốc gia/ vùng lãnh thổ, với gần 1,9 triệu ca nhiễm, hơn 126 nghìn ca tử vong. Đến nay, dịch bệnh vẫn chưa được kiểm soát và còn lây lan nhanh, diễn biến phức tạp tại Châu Âu, Mỹ và nhiều nước châu Á; tác động tiêu cực đối với mọi hoạt động kinh tế &#8211; xã hội toàn cầu và Việt Nam; trong đó, hầu hết các ngành, lĩnh vực kinh tế đều chịu tác động tiêu cực. Hầu hết các ngành nghề kinh doanh ở Việt Nam đều bị ảnh hưởng tiêu cực. Hàng loạt mặt bằng được doanh nghiệp “trả lại” sau vài tháng chiến đấu với đại dịch, doanh nghiệp chịu lỗ quá [&hellip;]</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article><!-- /.entry -->
                                                        <article class="entry format-">
                                                            <div class="entry-border">
                                                                <div class="featured-post post-home-page"><a href="/vinsofts-chuc-mung-ngay-quoc-te-phu-nu-8-3-2/"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="600" height="406" data-wpfc-original-src="/wp-content/uploads/2020/03/0.IMG_2390-600x406.jpg" class="attachment-themesflat-blog-grid size-themesflat-blog-grid wp-post-image" alt="blank" data-wpfc-original-srcset="/wp-content/uploads/2020/03/0.IMG_2390-600x406.jpg 600w, /wp-content/uploads/2020/03/0.IMG_2390-106x73.jpg 106w" sizes="(max-width: 600px) 100vw, 600px" /></a></div>
                                                                <div class="entry-meta clearfix">
                                                                    <ul class="meta-left">
                                                                        <li class="post-date">
                                                                            07/03/2020 </li>
                                                                        <li class="post-author">
                                                                            <span class="author vcard">By<a class="url fn n" href="/author/tuoivu/" title="View all posts by Tươi Vũ" rel="author"> Tươi Vũ</a></span> </li>
                                                                        <li class="post-categories">In <a href="/danh-muc/hoat-dong-noi-bat/" rel="category tag">Hoạt động công ty</a></li>
                                                                    </ul>
                                                                    <ul class="meta-right">
                                                                        <li class="post-comments"><i class="fa fa-comment-o" aria-hidden="true"></i><span>Chức năng bình luận bị tắt<span class="screen-reader-text"> ở Vinsofts chúc mừng ngày Quốc tế phụ nữ 8/3</span></span></li>
                                                                    </ul>
                                                                </div><!-- /.entry-meta -->
                                                                <div class="content-post">
                                                                    <h3 class="entry-title"><a href="/vinsofts-chuc-mung-ngay-quoc-te-phu-nu-8-3-2/">Vinsofts chúc mừng ngày Quốc tế phụ nữ 8/3</a></h3>
                                                                    <div class="entry-content hide">
                                                                        <p>Không chỉ riêng ngày 8/3 mà 364 ngày còn lại, các &#8220;chị em&#8221; vẫn luôn được nâng niu, trân quý. Nhân ngày 8/3, gửi tới chị em Vinsofts nói riêng, tất cả các Quý Khách hàng/ đối tác nữ nói chung luôn trẻ đẹp, hạnh phúc và nhiều niềm vui trong cuộc sống.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article><!-- /.entry -->
                                                    </div>
                                                    <ul class="themesflat_iconlist  auto_increment_number">
                                                        <li class="circle">
                                                            <div class="list-content">
                                                                <h3 class="list-title"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></h3>
                                                            </div>
                                                        </li>
                                                        <li class="circle">
                                                            <div class="list-content">
                                                                <h3 class="list-title"><a href="/phat-trien-ung-dung-web/">Phát triển ứng dụng Web</a></h3>
                                                            </div>
                                                        </li>
                                                        <li class="circle">
                                                            <div class="list-content">
                                                                <h3 class="list-title"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></h3>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .entry-content -->
                            </main><!-- #main -->
                        </div><!-- #primary -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- #content -->
        <!-- Footer -->
        <div class="footer_background">
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-widgets">
                            <div class="col-md-4 col-sm-6">
                                <div id="text-2" class="widget widget_text">
                                    <div class="textwidget">
                                        <p><a title="Financial Occult" href="/"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" style="height: 70px;" data-wpfc-original-src="/wp-content/uploads/2019/06/LOGO-FOOTER-min.png" alt="thumb" /><br />
                                            </a><a title="Financial Occult" href="#"><br />
                                            </a></p>
                                    </div>
                                </div>
                                <div id="text-5" class="widget widget_text">
                                    <div class="textwidget">
                                        <p>Với đội ngũ 70+ nhân viên và 10+ năm kinh nghiệm trong việc hợp tác và phát triển phần mềm cho các khách hàng từ khắp nơi trên thế giới, chúng tôi tự hào là một công ty gia công phần mềm hàng đầu tại Việt Nam. Chúng tôi luôn cam kết đem đến chất lượng dịch vụ cao nhất, trở thành đối tác tin cậy với bất kỳ đơn vị doanh nghiệp nào</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <div id="text-10" class="widget widget_text">
                                    <h4 class="widget-title">KẾT NỐI NHANH</h4>
                                    <div class="textwidget"></div>
                                </div>
                                <div id="widget_themesflat_socials-5" class="widget widget_themesflat_socials">
                                    <ul class="themesflat-shortcode-socials">
                                        <li class="facebook">
                                            <a class="title" href="https://www.facebook.com/vinsoftsjsc" target="_blank" rel="alternate" title="Facebook">
                                                <i class="fa fa-facebook"></i>
                                                <span>Facebook</span>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a class="title" href="https://www.youtube.com/channel/UC33N-fg42Gkp1Fcin5Zp5EA/" target="_blank" rel="alternate" title="Youtube">
                                                <i class="fa fa-youtube"></i>
                                                <span>Youtube</span>
                                            </a>
                                        </li>
                                        <li class="linkedin">
                                            <a class="title" href="https://www.linkedin.com/company/vinsofts/" target="_blank" rel="alternate" title="LinkedIn">
                                                <i class="fa fa-linkedin"></i>
                                                <span>LinkedIn</span>
                                            </a>
                                        </li>
                                    </ul><!-- /.social -->
                                </div>
                                <div id="custom_html-2" class="widget_text widget widget_custom_html">
                                    <div class="textwidget custom-html-widget">
                                        <ul class="themesflat-shortcode-socials">
                                            <li style="width: 115%;">
                                                <a href="https://goo.gl/maps/UksWXm7gqr62" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>Google map</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div id="nav_menu-2" class="widget widget_nav_menu">
                                    <h4 class="widget-title">DỊCH VỤ NỔI BẬT</h4>
                                    <div class="menu-footer_service-container">
                                        <ul id="menu-footer_service" class="menu">
                                            <li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2902"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
                                            <li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
                                            <li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2901"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
                                            <li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2904"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div id="nav_menu-14" class="widget widget_nav_menu">
                                    <h4 class="widget-title">Thông Tin</h4>
                                    <div class="menu-menu-footer-right-container">
                                        <ul id="menu-menu-footer-right" class="menu">
                                            <li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3559"><a href="/lien-he/">Liên hệ</a></li>
                                            <li id="menu-item-4013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4013"><a href="/jobs/">Tuyển dụng</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="custom_html-6" class="widget_text widget widget_custom_html">
                                    <div class="textwidget custom-html-widget">
                                        <div class="cus_dmca">
                                            <a href="//www.dmca.com/Protection/Status.aspx?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" title="DMCA.com Protection Status" class="dmca-badge"> <img src="https://images.dmca.com/Badges/dmca_protected_16_120.png?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" alt="DMCA.com Protection Status" /></a>
                                        </div>
                                        <style type="text/css">
                                        .cus_dmca a img {
                                            padding: 10px 0px 0px 48px;
                                        }
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.footer-widgets -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </footer>
            <div class="content-register-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="left">
                                <!-- Other language content here -->
                                <div class="textwidget">
                                    <p>Cơ quan chủ quản: Công ty Cổ phần Vinsofts</p>
                                    <!-- <p>Trụ sở chính: Tầng 8 Tòa nhà Sannam đường Duy Tân, phường Dịch Vọng Hậu, Q.Cầu Giấy, Hà Nội</p> -->
                                    <p>Văn phòng tại Hà Nội: Tầng 5, số 8 Phan Văn Trường, phường Dịch Vọng Hậu, Cầu Giấy, Hà Nội</p>
                                    <!-- <p>Văn phòng tại Tp.HCM: Unit P5-16.B Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, Tp.HCM</p> -->
                                    <p>Văn phòng tại Tp.HCM: P516 Block C Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, TP.HCM</p>
                                    <p>Tel:&nbsp;<a href="tel:0462593148">04.6259.3148</a>&nbsp;– Hotlline:&nbsp;<a href="tel:0961678247">0961.678.247</a>&nbsp;– Email:&nbsp;<a href="mailto:info@vinsofts.com">info@vinsofts.com</a></p>
                                    <p>Giấy phép kinh doanh số 0107354530 do Sở Kế hoạch và đầu tư cấp ngày 14/03/2016</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="right">
                                <!-- Other language content here -->
                                <div class="textwidget">
                                    <p>Bạn vui lòng đọc kỹ <a href="/vi/chinh-sach-bao-mat">Chính sách bảo mật thông tin</a> và <a href="/vi/dieu-khoan-su-dung">Điều khoản sử dụng</a>!</p>
                                    <p>Website đã được thông báo và được chấp nhận bởi Cục TMĐT và CNTT, Bộ Công Thương.</p>
                                    <p><a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43043"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-medium wp-image-5318" data-wpfc-original-src="/wp-content/uploads/2018/10/vinsofts-dathongbao-300x114.png" alt="blank" width="300" height="114" /></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Bottom -->
            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright <i class="fa fa-copyright"></i> 2012 - 2019
                                    <a href="#">Vinsofts JSC</a>. All rights reserved.</p>
                            </div>
                            <!-- Go Top -->
                            <a class="go-top show">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div>
            <div id="tawk-to-vinsofts">
                <!--Start of Tawk.to Script-->
                <!--End of Tawk.to Script-->
            </div>
        </div> <!-- Footer Background Image -->
    </div><!-- /#boxed -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.googletagmanager.com/ns.html?id=GTM-W8XSN4W" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- <link rel='stylesheet' property='stylesheet' id='rs-icon-set-fa-icon-css'  href='/wp-content/plugins/revslider/public/assets/fonts/font-awesome/css/font-awesome.css?version=7935' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='fo-child-css'  href='/wp-content/themes/fo-child/common/css/custom-css.css?version=129' type='text/css' media='all' /> -->
    <!-- <link rel='stylesheet' id='vc_typicons-css'  href='/wp-content/plugins/js_composer/assets/css/lib/typicons/src/font/typicons.min.css' type='text/css' media='all' /> -->
    <link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/kdaqoirl/4b9n.css" media="all" />
    <script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&#038;render=explicit'></script>
    <noscript id="wpfc-google-fonts">
        <link rel='stylesheet' id='themesflat-theme-slug-fonts-css' href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
        <link href="https://fonts.googleapis.com/css?family=Roboto:500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    </noscript>
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/folii5hc/4b9p.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7lob9sfs/4b9p.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/d6ougkxu/4b9p.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/eqjz612f/4b9p.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lpm2zzii/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/fte2l3l2/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mkx6gd8y/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mknr8988/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/zdl5aix/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7xsi43ue/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/976glhgy/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1eotoqvg/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/e726dwxx/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/llhj9sk8/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/erkz11cs/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1281u0q0/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lll4wyip/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/dq6j6xeh/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/869bu30y/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/47mj14y/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/qa893n0m/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/es8b11eb/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/knvqh2vr/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/49n6o1s/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/8yzbpxl8/4b9o.js'></script> -->
    <!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/g2uuwjnu/4b9o.js'></script> -->
    <script type='text/javascript'>
    /* <![CDATA[ */
    var aamLocal = { "nonce": "6207c2d185", "ajaxurl": "\/wp-admin\/admin-ajax.php" };
    /* ]]> */
    </script>
    <script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5b7f8308f31d0f771d84184a/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
    </script>
    <script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = { "apiSettings": { "root": "\/wp-json\/contact-form-7\/v1", "namespace": "contact-form-7\/v1" }, "recaptcha": { "messages": { "empty": "H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot." } }, "cached": "1" };
    /* ]]> */
    </script>
    <script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7_redirect_forms = { "5612": { "page_id": "5739", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "\/dang-ky-thanh-cong\/" }, "5444": { "page_id": "5739", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "\/dang-ky-thanh-cong\/" }, "4387": { "page_id": "0", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "" }, "4383": { "page_id": "0", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "" }, "3863": { "page_id": "0", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "" }, "3777": { "page_id": "3780", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "\/thank-you\/" }, "3769": { "page_id": "0", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "" }, "3697": { "page_id": "3780", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "\/thank-you\/" }, "1989": { "page_id": "0", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "" }, "644": { "page_id": "0", "external_url": "", "use_external_url": "", "open_in_new_tab": "", "http_build_query": "", "http_build_query_selectively": "", "http_build_query_selectively_fields": "", "after_sent_script": "", "thankyou_page_url": "" } };
    /* ]]> */
    </script>
    <script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
    <script type="text/javascript">
    function setREVStartSize(e) {
        try {
            var i = jQuery(window).width(),
                t = 9999,
                r = 0,
                n = 0,
                l = 0,
                f = 0,
                s = 0,
                h = 0;
            if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) { f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e) }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) { var u = (e.c.width(), jQuery(window).height()); if (void 0 != e.fullScreenOffsetContainer) { var c = e.fullScreenOffsetContainer.split(","); if (c) jQuery.each(c, function(e, i) { u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0)) } f = u } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
            e.c.closest(".rev_slider_wrapper").css({ height: f })
        } catch (d) { console.log("Failure at Presize of Slider:" + d) }
    };
    </script>
    <script>
    setTimeout(function() {
        // $('.popup #myModal2').slideDown();
        jQuery('.popup #myModal2').show();
        jQuery('#myModal2').modal({
            backdrop: 'static',
            keyboard: false
        });
        // $('body').addClass('modal-open');
        // $('body').append('<div class="modal-backdrop fade in"></div>');
        // $('button.close').click(function(){
        //     $('#myModal2').slideUp();
        //     $('body').removeClass('modal-open');
        //     $('div.modal-backdrop').remove();
        // })
    }, 50000);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.js" integrity="sha512-Fq/wHuMI7AraoOK+juE5oYILKvSPe6GC5ZWZnvpOO/ZPdtyA29n+a5kVLP4XaLyDy9D1IBPYzdFycO33Ijd0Pg==" crossorigin="anonymous"></script>
    <script src="https://smtpjs.com/v3/smtp.js"></script>
    <script type="text/javascript">
    function uploadFileToServer() {
        var file = event.srcElement.files[0];
        var reader = new FileReader();
        reader.readAsBinaryString(file);
        reader.onload = function() {
            var dataUri = "data:" + file.type + ";base64," + btoa(reader.result);
            Email.send({
                Host: "smtp.gmail.com",
                Username: "buituananh48@gmail.com",
                Password: "jcwwhqigddqbuyyo",
                To: "tuoivt@vinsofts.com, info@vinsofts.com, anhbt@vinsofts.net, chinhnv@vinsofts.net",
                From: "info@vinsofts.com",
                Subject: "Có file đính kèm",
                Body: "",
                Attachments: [{
                    name: file.name,
                    data: dataUri
                }]
            }).then(
                message => console.log(1)
            );
        };
        reader.onerror = function() {
            console.log('there are some problems');
        };
    }
    // function Alert() {
    //     document.getElementById()
    // }
    jQuery(document).ready(function($) {
        $(document).on('click', '.wpcf7-form-control.wpcf7-submit.frm_ycbg', function(event) {
            event.preventDefault();
            let name = $('input[name=your-name]').val(); //Tên người liên hệ
            let email = $('input[name=your-email]').val(); //Email người liên hệ
            let phone = $('input[name=your-phone]').val();
            let subject = $('input[name=your-subject]').val();
            let service = $('select[name=how_can]').val();
            let message = $('textarea[name=your-message]').val();
            // console.log(name, email, subject, service);
            if ($.trim(name) == '' && $.trim(phone) != '' && $.trim(email) != '') {
                $('input[name=your-name]').trigger('focusout');
                return false;
            }
            if ($.trim(phone) == '' && $.trim(email) != '' && $.trim(name) != '') {
                $('input[name=your-phone]').trigger('focusout');
                return false;
            }
            if ($.trim(email) == '' && $.trim(name) != '' && $.trim(phone) != '') {
                $('input[name=your-email]').trigger('focusout');
                return false;
            }
            if ($.trim(name) == '' && $.trim(phone) == '' && $.trim(email) != '') {
                $('input[name=your-name]').trigger('focusout');
                $('input[name=your-phone]').trigger('focusout');
                return false;
            }
            if ($.trim(name) == '' && $.trim(email) == '' && $.trim(phone) != '') {
                $('input[name=your-name]').trigger('focusout');
                $('input[name=your-email]').trigger('focusout');
                return false;
            }
            if ($.trim(phone) == '' && $.trim(email) == '' && $.trim(name) !== '') {
                $('input[name=your-name]').trigger('focusout');
                $('input[name=your-email]').trigger('focusout');
                return false;
            }
            if ($.trim(phone) == '' && $.trim(email) == '' && $.trim(name) == '') {
                $('input[name=your-name]').trigger('focusout');
                $('input[name=your-email]').trigger('focusout');
                $('input[name=your-phone]').trigger('focusout');
                return false;
            }
            Email.send({
                Host: "smtp.gmail.com",
                Username: "buituananh48@gmail.com",
                Password: "jcwwhqigddqbuyyo",
                To: "tuoivt@vinsofts.com, info@vinsofts.com, anhbt@vinsofts.net, chinhnv@vinsofts.net",
                From: "info@vinsofts.com",
                Subject: "Có thư yêu cầu báo giá từ vinsofts.com",
                Body: "<div><label style='color:red;'>Tên: </label> <span>" + name + "</span> <br><label style='color:red;'>SĐT: </label> <span>" + phone + "</span> <br><label style='color:red;'>Email: </label> <span>" + email + "</span> <br><label style='color:red;'>Câu hỏi: </label> <span>" + subject + "</span> <br><label style='color:red;'>Dịch vụ quan tâm: </label> <span>" + service + "</span> <br><label style='color:red;'>message: </label> <span>" + message + "</span><br></div>",
            }).then(
                message => location.href = "/thank-you/"
            );
        });        

        $(document).on('click', '.frm_yctuvan', function(event) {
            event.preventDefault();
            let name = $('input[name=your-name-1]').val(); //Tên người liên hệ
            let email = $('input[name=your-email-1]').val(); //Email người liên hệ
            let phone = $('input[name=your-phone-1]').val();
            let subject = $('input[name=your-subject]').val();
            let service = $('select[name=how_can_1]').val();
            // console.log(name, email, subject, service);
            if ($.trim(name) == '' && $.trim(phone) != '' && $.trim(email) != '') {
                $('input[name=your-name-1]').trigger('focusout');
                return false;
            }
            if ($.trim(phone) == '' && $.trim(email) != '' && $.trim(name) != '') {
                $('input[name=your-phone-1]').trigger('focusout');
                return false;
            }
            if ($.trim(email) == '' && $.trim(name) != '' && $.trim(phone) != '') {
                $('input[name=your-email-1]').trigger('focusout');
                return false;
            }
            if ($.trim(name) == '' && $.trim(phone) == '' && $.trim(email) != '') {
                $('input[name=your-name-1]').trigger('focusout');
                $('input[name=your-phone-1]').trigger('focusout');
                return false;
            }
            if ($.trim(name) == '' && $.trim(email) == '' && $.trim(phone) != '') {
                $('input[name=your-name-1]').trigger('focusout');
                $('input[name=your-email-1]').trigger('focusout');
                return false;
            }
            if ($.trim(phone) == '' && $.trim(email) == '' && $.trim(name) !== '') {
                $('input[name=your-name-1]').trigger('focusout');
                $('input[name=your-email-1]').trigger('focusout');
                return false;
            }
            if ($.trim(phone) == '' && $.trim(email) == '' && $.trim(name) == '') {
                $('input[name=your-name-1]').trigger('focusout');
                $('input[name=your-email-1]').trigger('focusout');
                $('input[name=your-phone-1]').trigger('focusout');
                return false;
            }
            Email.send({
                Host: "smtp.gmail.com",
                Username: "buituananh48@gmail.com",
                Password: "jcwwhqigddqbuyyo",
                To: "tuoivt@vinsofts.com, info@vinsofts.com, anhbt@vinsofts.net, chinhnv@vinsofts.net",
                From: "info@vinsofts.com",
                Subject: "Có thư yêu cầu báo giá từ vinsofts.com",
                Body: "<div><label style='color:red;'>Tên: </label> <span>" + name + "</span> <br><label style='color:red;'>SĐT: </label> <span>" + phone + "</span> <br><label style='color:red;'>Email: </label> <span>" + email + "</span> <br><label style='color:red;'>Câu hỏi: </label> <span>" + subject + "</span> <br><label style='color:red;'>Dịch vụ quan tâm: </label> <span>" + service + "</span></div>",
            }).then(
                message => location.href = "/thank-you/"
            );
        });
    });
    </script>
    </script>
    <script type='text/javascript'>
    //<![CDATA[
    // JavaScript Document
    // var message="NoRightClicking";
    // function defeatIE() {
    //   if (document.all) {(message);return false;}
    // }
    // function defeatNS(e) {
    //   if (document.layers||(document.getElementById&&!document.all))
    //   { if (e.which==2||e.which==3) {(message);return false;}}
    // }
    // if (document.layers) {
    //   document.captureEvents(Event.MOUSEDOWN);
    //   document.onmousedown=defeatNS;
    // } else{
    //   document.onmouseup=defeatNS;document.oncontextmenu=defeatIE;
    // }
    // document.oncontextmenu=new Function("return false")
    // //]]>
    // // enable to override webpacks publicPath
    // var webpackPublicPath = '/';
    // jQuery(document).keydown(function(event) {
    //     if (
    //       event.keyCode === 123 ||
    //       (event.ctrlKey && event.shiftKey && event.keyCode === 67) ||
    //       (event.ctrlKey && event.keyCode === 85)
    //     ) {
    //       return false;
    //     }
    // });
    // document.onselectstart = new Function('return false');
    // if (window.sidebar) {
    //     document.onmousedown = false;
    //     document.onclick = true;
    // }
    </script>
    <script>
    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
    var htmlDivCss = "";
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement("div");
        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
    }
    </script>
    <script>
    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
    var htmlDivCss = "";
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement("div");
        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
    }
    </script>
    <script type="text/javascript">
    setREVStartSize({ c: jQuery('#rev_slider_1_1'), responsiveLevels: [1240, 1240, 1240, 480], gridwidth: [1240, 1240, 1240, 480], gridheight: [630, 630, 630, 550], sliderLayout: 'fullwidth' });
    var revapi1,
        tpj = jQuery;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_1_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1_1");
        } else {
            revapi1 = tpj("#rev_slider_1_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "/wp-content/plugins/revslider/public/assets/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "on",
                    mouseScrollReverse: "default",
                    onHoverStop: "off",
                    arrows: {
                        style: "metis",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: false,
                        style: "metis",
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 20,
                        space: 5,
                        tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
                    }
                },
                responsiveLevels: [1240, 1240, 1240, 480],
                visibilityLevels: [1240, 1240, 1240, 480],
                gridwidth: [1240, 1240, 1240, 480],
                gridheight: [630, 630, 630, 550],
                lazyType: "none",
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
            jQuery('body').on('click', '.video-button', function() {
                var slide = jQuery(this).closest('li'),
                    vid = slide.find('.youtube-video').show();
                vid.find('video')[0].play();
                console.log(vid);
            });
            // change the "revapi1" parts to whatever api name is used for your slider
            revapi1.on('revolution.slide.onchange', function() {
                revapi1.find('.youtube-video').hide();
            });
        }
    }); /*ready*/
    </script>
    <script>
    var htmlDivCss = unescape(".rev_slider%20.youtube-video%20%7Bdisplay%3A%20none%7D%0A.rev_slider%20.video-button%20%7Bcursor%3A%20pointer%7D");
    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement('div');
        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
    }
    </script>
    <script>
    var htmlDivCss = unescape("%23rev_slider_1_1%20.metis.tparrows%20%7B%0A%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%20padding%3A10px%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20width%3A60px%3B%0A%20%20height%3A60px%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%20%7D%0A%20%0A%20%23rev_slider_1_1%20.metis.tparrows%3Ahover%20%7B%0A%20%20%20background%3Argba%28255%2C255%2C255%2C0.75%29%3B%0A%20%7D%0A%20%0A%20%23rev_slider_1_1%20.metis.tparrows%3Abefore%20%7B%0A%20%20color%3Argb%280%2C%200%2C%200%29%3B%20%20%0A%20%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%23rev_slider_1_1%20.metis.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20transform%3Ascale%281.5%29%3B%0A%20%20%7D%0A%20%0A%23rev_slider_1_1%20.metis%20.tp-bullet%20%7B%20%0A%20%20%20%20opacity%3A1%3B%0A%20%20%20%20width%3A50px%3B%0A%20%20%20%20height%3A50px%3B%20%20%20%20%0A%20%20%20%20padding%3A3px%3B%0A%20%20%20%20background-color%3Argba%280%2C%200%2C%200%2C0.25%29%3B%0A%20%20%20%20margin%3A0px%3B%0A%20%20%20%20box-sizing%3Aborder-box%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20%20%20border-radius%3A50%25%3B%0A%20%20%7D%0A%0A%23rev_slider_1_1%20.metis%20.tp-bullet-image%20%7B%0A%0A%20%20%20border-radius%3A50%25%3B%0A%20%20%20display%3Ablock%3B%0A%20%20%20box-sizing%3Aborder-box%3B%0A%20%20%20position%3Arelative%3B%0A%20%20%20%20-webkit-box-shadow%3A%20inset%205px%205px%2010px%200px%20rgba%280%2C0%2C0%2C0.25%29%3B%0A%20%20-moz-box-shadow%3A%20inset%205px%205px%2010px%200px%20rgba%280%2C0%2C0%2C0.25%29%3B%0A%20%20box-shadow%3A%20inset%205px%205px%2010px%200px%20rgba%280%2C0%2C0%2C0.25%29%3B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20background-size%3Acover%3B%0A%20%20background-position%3Acenter%20center%3B%0A%20%7D%20%20%0A%23rev_slider_1_1%20.metis%20.tp-bullet-title%20%7B%20%0A%20%20%20%20%20position%3Aabsolute%3B%20%0A%20%20%20%20%20bottom%3A50px%3B%0A%20%20%20%20%20margin-bottom%3A10px%3B%0A%20%20%20%20%20display%3Ainline-block%3B%0A%20%20%20%20%20left%3A50%25%3B%0A%20%20%20%20%20background%3A%23000%3B%0A%20%20%20%20%20background%3Argba%280%2C%200%2C%200%2C0.75%29%3B%0A%20%20%20%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20%20%20%20padding%3A10px%2030px%3B%0A%20%20%20%20%20border-radius%3A4px%3B%0A%20%20%20-webkit-border-radius%3A4px%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%20%20%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20%20%20transform%3A%20translatez%280.001px%29%20translatex%28-50%25%29%20translatey%2814px%29%3B%0A%20%20%20%20transform-origin%3A50%25%20100%25%3B%0A%20%20%20%20-webkit-transform%3A%20translatez%280.001px%29%20translatex%28-50%25%29%20translatey%2814px%29%3B%0A%20%20%20%20-webkit-transform-origin%3A50%25%20100%25%3B%0A%20%20%20%20opacity%3A0%3B%0A%20%20%20%20white-space%3Anowrap%3B%0A%20%7D%0A%0A%23rev_slider_1_1%20.metis%20.tp-bullet%3Ahover%20.tp-bullet-title%20%7B%0A%20%20%20%20%20transform%3Arotatex%280deg%29%20translatex%28-50%25%29%3B%0A%20%20%20%20-webkit-transform%3Arotatex%280deg%29%20translatex%28-50%25%29%3B%0A%20%20%20%20opacity%3A1%3B%0A%7D%0A%0A%23rev_slider_1_1%20.metis%20.tp-bullet.selected%2C%0A%23rev_slider_1_1%20.metis%20.tp-bullet%3Ahover%20%20%7B%0Abackground%3A%20-moz-linear-gradient%28top%2C%20%20rgba%28255%2C%20255%2C%20255%2C%201%29%200%25%2C%20rgba%28119%2C%20119%2C%20119%2C%201%29%20100%25%29%3B%0Abackground%3A%20-webkit-gradient%28left%20top%2C%20left%20bottom%2C%20color-stop%280%25%2C%20rgba%28255%2C%20255%2C%20255%2C%201%29%29%2C%20color-stop%28100%25%2C%20rgba%28119%2C%20119%2C%20119%2C%201%29%29%29%3B%0Abackground%3A%20-webkit-linear-gradient%28top%2C%20rgba%28255%2C%20255%2C%20255%2C%201%29%200%25%2C%20rgba%28119%2C%20119%2C%20119%2C%201%29%20100%25%29%3B%0Abackground%3A%20-o-linear-gradient%28top%2C%20rgba%28255%2C%20255%2C%20255%2C%201%29%200%25%2C%20rgba%28119%2C%20119%2C%20119%2C%201%29%20100%25%29%3B%0Abackground%3A%20-ms-linear-gradient%28top%2C%20rgba%28255%2C%20255%2C%20255%2C%201%29%200%25%2C%20rgba%28119%2C%20119%2C%20119%2C%201%29%20100%25%29%3B%0Abackground%3A%20linear-gradient%28to%20bottom%2C%20rgba%28255%2C%20255%2C%20255%2C%201%29%200%25%2C%20rgba%28119%2C%20119%2C%20119%2C%201%29%20100%25%29%3B%0A%20%20%7D%0A%23rev_slider_1_1%20.metis%20.tp-bullet-title%3Aafter%20%7B%0A%20%20%20%20content%3A%22%20%22%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20left%3A50%25%3B%0A%20%20%20%20margin-left%3A-8px%3B%0A%20%20%20%20width%3A%200%3B%0A%20%20%20%20height%3A%200%3B%0A%20%20%20%20border-style%3A%20solid%3B%0A%20%20%20%20border-width%3A%208px%208px%200%208px%3B%0A%20%20%20%20border-color%3A%20rgba%280%2C%200%2C%200%2C0.75%29%20transparent%20transparent%20transparent%3B%0A%20%20%20%20bottom%3A-8px%3B%0A%20%20%20%7D%0A%0A%0A%0A%2F%2A%20VERTICAL%20RIGHT%20%2A%2F%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-right%20.tp-bullet-title%20%7B%20%0A%20%20%20margin-bottom%3A0px%3B%20top%3A50%25%3B%20right%3A50px%3B%20left%3Aauto%3B%20bottom%3Aauto%3B%20margin-right%3A10px%3B%20%20transform%3A%20translateX%28-10px%29%20translateY%28-50%25%29%3B-webkit-transform%3A%20translateX%28-10px%29%20translateY%28-50%25%29%3B%20%0A%7D%20%20%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-right%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%2010px%200%2010px%2010px%3B%0A%20%20border-color%3A%20%20transparent%20transparent%20transparent%20rgba%280%2C%200%2C%200%2C0.75%29%20%3B%0A%20%20right%3A-10px%3B%0A%20%20left%3Aauto%3B%20%20%0A%20%20bottom%3Aauto%3B%0A%20%20top%3A10px%3B%20%20%20%20%0A%7D%0A%0A%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-right%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateY%28-50%25%29%20translateX%280px%29%3B%0A%20%20-webkit-transform%3AtranslateY%28-50%25%29%20translateX%280px%29%3B%0A%7D%0A%0A%2F%2A%20VERTICAL%20LEFT%20%26%26%20CENTER%2A%2F%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%2C%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-center%20.tp-bullet-title%20%7B%20%0A%20%20%20margin-bottom%3A0px%3B%20top%3A50%25%3B%20left%3A50px%3B%20right%3Aauto%3B%20bottom%3Aauto%3B%20margin-left%3A10px%3B%20%20transform%3A%20translateX%2810px%29%20translateY%28-50%25%29%3B-webkit-transform%3A%20translateX%2810px%29%20translateY%28-50%25%29%3B%20%0A%7D%20%20%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%3Aafter%2C%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-center%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%2010px%2010px%2010px%200%3B%0A%20%20border-color%3A%20%20transparent%20rgba%280%2C%200%2C%200%2C0.75%29%20%20transparent%20transparent%20%3B%0A%20%20left%3A-2px%3B%0A%20%20right%3Aauto%3B%20%20%0A%20%20bottom%3Aauto%3B%0A%20%20top%3A10px%3B%20%20%20%20%0A%7D%0A%0A%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet%3Ahover%20.tp-bullet-title%2C%0A%23rev_slider_1_1%20.metis.nav-dir-vertical.nav-pos-hor-center%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateY%28-50%25%29%20translateX%280px%29%3B%0A%20%20-webkit-transform%3AtranslateY%28-50%25%29%20translateX%280px%29%3B%0A%7D%0A%0A%0A%2F%2A%20HORIZONTAL%20TOP%20%2A%2F%0A%23rev_slider_1_1%20.metis.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%20%7B%20%0A%20%20%20margin-bottom%3A0px%3B%20top%3A50px%3B%20left%3A50%25%3B%20bottom%3Aauto%3B%20margin-top%3A10px%3B%20right%3Aauto%3B%20transform%3A%20translateX%28-50%25%29%20translateY%2810px%29%3B-webkit-transform%3A%20translateX%28-50%25%29%20translateY%2810px%29%3B%20%0A%7D%20%20%0A%23rev_slider_1_1%20.metis.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%200%2010px%2010px%2010px%3B%0A%20%20border-color%3A%20%20transparent%20transparent%20rgba%280%2C%200%2C%200%2C0.75%29%20transparent%3B%0A%20%20right%3Aauto%3B%0A%20%20left%3A50%25%3B%0A%20%20margin-left%3A-10px%3B%0A%20%20bottom%3Aauto%3B%0A%20%20top%3A-10px%3B%0A%20%20%20%20%0A%7D%0A%0A%0A%23rev_slider_1_1%20.metis.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A");
    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
    if (htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    } else {
        var htmlDiv = document.createElement('div');
        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
    }
    </script>
    <script type="text/javascript">
    var recaptchaWidgets = [];
    var recaptchaCallback = function() {
        var forms = document.getElementsByTagName('form');
        var pattern = /(^|\s)g-recaptcha(\s|$)/;
        for (var i = 0; i < forms.length; i++) {
            var divs = forms[i].getElementsByTagName('div');
            for (var j = 0; j < divs.length; j++) {
                var sitekey = divs[j].getAttribute('data-sitekey');
                if (divs[j].className && divs[j].className.match(pattern) && sitekey) {
                    var params = {
                        'sitekey': sitekey,
                        'type': divs[j].getAttribute('data-type'),
                        'size': divs[j].getAttribute('data-size'),
                        'theme': divs[j].getAttribute('data-theme'),
                        'badge': divs[j].getAttribute('data-badge'),
                        'tabindex': divs[j].getAttribute('data-tabindex')
                    };
                    var callback = divs[j].getAttribute('data-callback');
                    if (callback && 'function' == typeof window[callback]) {
                        params['callback'] = window[callback];
                    }
                    var expired_callback = divs[j].getAttribute('data-expired-callback');
                    if (expired_callback && 'function' == typeof window[expired_callback]) {
                        params['expired-callback'] = window[expired_callback];
                    }
                    var widget_id = grecaptcha.render(divs[j], params);
                    recaptchaWidgets.push(widget_id);
                    break;
                }
            }
        }
    };
    document.addEventListener('wpcf7submit', function(event) {
        switch (event.detail.status) {
            case 'spam':
            case 'mail_sent':
            case 'mail_failed':
                for (var i = 0; i < recaptchaWidgets.length; i++) {
                    grecaptcha.reset(recaptchaWidgets[i]);
                }
        }
    }, false);
    </script>
    <script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
    </script>
    <script defer src='/wp-content/cache/wpfc-minified/rpc0zj1/4b9o.js' type="text/javascript"></script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        function wpfcgl() { var wgh = document.querySelector('noscript#wpfc-google-fonts').innerText,
                wgha = wgh.match(/<link[^\>]+>/gi); for (i = 0; i < wgha.length; i++) { var wrpr = document.createElement('div');
                wrpr.innerHTML = wgha[i];
                document.body.appendChild(wrpr.firstChild); } } wpfcgl(); });
    </script>
    <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        jQuery(document).ready(function() {
            // jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
            //     jQuery(".home-page-form-contact").hide('fade');
            // });
            // var date = new Date();
            // var minutes = 1;
            // date.setTime(date.getTime() + (minutes * 60 * 1000));
            // if (jQuery.cookie("popup_1_2") == null) {
            //     setTimeout(function(){
            //         jQuery(".popup #myModal2").show();
            //     }, 30000);
            //     jQuery.cookie("popup_1_2", "foo", { expires: date });
            // }
            // jQuery('#myModal2').modal({
            //     backdrop: 'static',
            //     keyboard: false
            // });
            jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
            jQuery(".info-top-right.border-left .appoinment").attr('data-target', '#myModal2');
            jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop', 'static');
            jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard', 'false');
            jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
            jQuery(".btn-our-members").attr('data-toggle', 'modal');
            jQuery(".btn-our-members").attr('data-target', '#myModa1');
            jQuery(".btn-our-members").attr('href', '#');
            // jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
            jQuery(".btn-our-members").click(function() {
                var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
                var content = parent.find('.team-desc').html();
                var title = parent.find('.team-info .team-name').html();
                var title_mini = parent.find('.team-info .team-subtitle').html();
                var image_member = parent.find('.team-image').html();
                var social = parent.find('.box-social-links .social-links').html();
                // jQuery(".popup-2 .modal-body").html(content);
                jQuery(".popup-2 .modal-content .modal-title").html(title);
                jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
                jQuery(".popup-2 .modal-body .content-member").html(content);
                jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
                jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
            })
        });
    });
    </script>
    <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        jQuery(document).ready(function() {
            jQuery(".themesflat-boxed .themesflat-button.themesflat-archive").addClass('btn-readmore');
            jQuery(".themesflat-boxed .themesflat-button.themesflat-archive").text(function(i, oldText) {
                return oldText === 'Read More ' ? 'Xem Chi Tiết ' : oldText;
            });
        });
    });
    </script>
    <script>
    (function() {
        document.addEventListener("DOMContentLoaded", function() {
            var e = "dmca-badge";
            var t = "refurl";
            if (!document.getElementsByClassName) {
                document.getElementsByClassName = function(e) {
                    var t = document.getElementsByTagName("a"),
                        n = [],
                        r = 0,
                        i;
                    while (i = t[r++]) {
                        i.className == e ? n[n.length] = i : null
                    }
                    return n
                }
            }
            var n = document.getElementsByClassName(e);
            if (n[0].getAttribute("href").indexOf("refurl") < 0) {
                for (var r = 0; r < n.length; r++) {
                    var i = n[r];
                    i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
                }
            }
        }, false)
    })()
    </script>
</body>

</html><!-- WP Fastest Cache file was created in 8.8866229057312 seconds, on 29-07-20 16:35:22 -->