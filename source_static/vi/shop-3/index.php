<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/shop-3/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/shop-3/";
		</script>

		<p>You are being redirected to <a href="/shop-3/">/shop-3/</a></p>
	</body>
</html>
