<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/thank-you/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/thank-you/";
		</script>

		<p>You are being redirected to <a href="/thank-you/">/thank-you/</a></p>
	</body>
</html>
