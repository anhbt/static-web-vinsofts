<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/landing-page/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/landing-page/";
		</script>

		<p>You are being redirected to <a href="/landing-page/">/landing-page/</a></p>
	</body>
</html>
