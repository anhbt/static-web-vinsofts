<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/portfolios/webhero-don-vi-thiet-ke-website/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/portfolios/webhero-don-vi-thiet-ke-website/";
		</script>

		<p>You are being redirected to <a href="/portfolios/webhero-don-vi-thiet-ke-website/">/portfolios/webhero-don-vi-thiet-ke-website/</a></p>
	</body>
</html>
