<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/by-technology/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/by-technology/";
		</script>

		<p>You are being redirected to <a href="/by-technology/">/by-technology/</a></p>
	</body>
</html>
