﻿<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="/xmlrpc.php">
<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W8XSN4W');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125473191-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-125473191-1');
</script>
<title>Điều khoản MXH Astra - Công ty phần mềm Vinsofts</title>
<!-- This site is optimized with the Yoast SEO Premium plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
<link rel="canonical" href="/dieu-khoan-mxh-astra/" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Điều khoản MXH Astra - Công ty phần mềm Vinsofts" />
<meta property="og:description" content="Điều khoản dịch vụ Chào mừng bạn đến với Mạng xã hội du lịch Astra! Astra là Mạng xã hội tiên phong trên thế giới về du lịch. Dựa trên nền tảng công nghệ blockchain, Astra xây dựng một cộng đồng những người ưa xê dịch, tích cực, sáng tạo, luôn bắt kịp với những &hellip;" />
<meta property="og:url" content="/dieu-khoan-mxh-astra/" />
<meta property="og:site_name" content="Công ty phần mềm Vinsofts" />
<meta property="article:publisher" content="https://www.facebook.com/vinsoftsjsc" />
<meta property="fb:app_id" content="1096476827181655" />
<meta property="og:image" content="/wp-content/uploads/2018/08/all-vi.jpg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Điều khoản dịch vụ Chào mừng bạn đến với Mạng xã hội du lịch Astra! Astra là Mạng xã hội tiên phong trên thế giới về du lịch. Dựa trên nền tảng công nghệ blockchain, Astra xây dựng một cộng đồng những người ưa xê dịch, tích cực, sáng tạo, luôn bắt kịp với những [&hellip;]" />
<meta name="twitter:title" content="Điều khoản MXH Astra - Công ty phần mềm Vinsofts" />
<meta name="twitter:image" content="/wp-content/uploads/2018/08/all-vi.jpg" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"\/","name":"C\u00f4ng ty ph\u1ea7n m\u1ec1m Vinsofts","potentialAction":{"@type":"SearchAction","target":"\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO Premium plugin. -->
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Công ty phần mềm Vinsofts &raquo;" href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Công ty phần mềm Vinsofts &raquo;" href="/comments/feed/" />
<!-- <link rel='stylesheet' id='vc_extend_shortcode-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes.css?version=8713' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_extend_style-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes-3rd.css?version=1743' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='bootstrap-css'  href='/wp-content/themes/fo/css/bootstrap.css?version=6649' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?version=880' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qjpyrk45/4b9o.css" media="all"/>
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<!-- <link rel='stylesheet' id='qts_front_styles-css'  href='/wp-content/plugins/qtranslate-slug/assets/css/qts-default.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_main-css'  href='/wp-content/themes/fo/css/main.css?version=5247' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-style-css'  href='/wp-content/themes/fo-child/style.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='font-fontawesome-css'  href='/wp-content/themes/fo/css/font-awesome.css?version=1759' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-ionicons-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=9363' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_simpleline-css-css'  href='/wp-content/themes/fo/css/simple-line-icons.css?version=2958' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_ion_icon-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=2681' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/9jminkn5/4b9o.css" media="all"/>
<!--[if lte IE 9]><link rel='stylesheet' id='ie9-css'  href='/wp-content/themes/fo/css/ie.css?version=3765' type='text/css' media='all' /><![endif]-->
<!-- <link rel='stylesheet' id='themesflat_logo-css'  href='/wp-content/themes/fo/css/logo.css?version=9625' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_animate-css'  href='/wp-content/themes/fo/css/animate.css?version=8957' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_responsive-css'  href='/wp-content/themes/fo/css/responsive.css?version=8551' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-inline-css-css'  href='/wp-content/themes/fo/css/inline-css.css?version=1288' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/mmoo7qcx/4b9o.css" media="all"/>
<style id='themesflat-inline-css-inline-css' type='text/css'>
.logo{padding-top:24px; padding-left:15px; }
.footer{padding-top:64px; padding-bottom:74px; }
.page-title{padding-top:21px; }
.logo img, .logo svg { height:70px; }
.page-title .overlay{ background: #ffffff}.page-title {background: url() center /cover no-repeat;}.page-title h1 {color:#21536c!important;
}
.breadcrumbs span,.breadcrumbs span a, .breadcrumbs a {color:#595959!important;
}
body,button,input,select,textarea { font-family:Poppins ; }
body,button,input,select,textarea { font-weight:400;}
body,button,input,select,textarea { font-style:normal; }
body,button,input,select,textarea { font-size:14px; }
body,button,input,select,textarea { line-height:25px ; }
h1,h2,h3,h5,h6 { font-family:Poppins;}
h1,h2,h3,h4,h5,h6 { font-weight:600;}
h1,h2,h3,h4,h5,h6  { font-style:normal; }
#mainnav > ul > li > a { font-family:Poppins;}
#mainnav > ul > li > a { font-weight:600;}
#mainnav > ul > li > a  { font-style:normal; }
#mainnav ul li a { font-size:14px;}
#mainnav > ul > li > a { line_height100px;}
h1 { font-size:32px; }
h2 { font-size:25px; }
h3 { font-size:22px; }
h4 { font-size:18px; }
h5 { font-size:15px; }
h6 { font-size:14px; }
.iconbox .box-header .box-icon span,a:hover, a:focus,.portfolio-filter li a:hover, .portfolio-filter li.active a,.themesflat-portfolio .item .category-post a,.color_theme,.widget ul li a:hover,.footer-widgets ul li a:hover,.footer a:hover,.themesflat-top a:hover,.themesflat-portfolio .portfolio-container.grid2 .title-post a:hover,.themesflat-button.no-background, .themesflat-button.blog-list-small,.show-search a i:hover,.widget.widget_categories ul li a:hover,.breadcrumbs span a:hover, .breadcrumbs a:hover,.comment-list-wrap .comment-reply-link,.portfolio-single .content-portfolio-detail h3,.portfolio-single .content-portfolio-detail ul li:before, .themesflat-list-star li:before, .themesflat-list li:before,.navigation.posts-navigation .nav-links li a .meta-nav,.testimonial-sliders.style3 .author-name a,ul.iconlist .list-title a:hover,.themesflat_iconbox .iconbox-icon .icon span,.bottom .copyright a,.top_bar2 .wrap-header-content ul li i { color:#337493;}
#Ellipse_7 circle,.testimonial-sliders .logo_svg path { fill:#337493;}
.info-top-right a.appoinment, .wrap-header-content a.appoinment,button, input[type=button], input[type=reset], input[type=submit],.go-top:hover,.portfolio-filter.filter-2 li a:hover, .portfolio-filter.filter-2 li.active a,.themesflat-socials li a:hover, .entry-footer .social-share-article ul li a:hover,.themesflat-button,.featured-post.blog-slider .flex-prev, .featured-post.blog-slider .flex-next,mark, ins,#themesflat-portfolio-carousel ul.flex-direction-nav li a, .flex-direction-nav li a,.navigation.posts-navigation .nav-links li a:after,.title_related_portfolio:after,.navigation.loadmore a:hover,.owl-theme .owl-controls .owl-nav [class*=owl-],.widget.widget_tag_cloud .tagcloud a,.btn-menu:before, .btn-menu:after, .btn-menu span,.themesflat_counter.style2 .themesflat_counter-icon .icon,widget a.appoinment,.themesflat_imagebox .imagebox-image:after,.nav-widget a.appoinment { background-color:#337493; }
.themesflat_btnslider:not(:hover) {
background-color:#337493!important;
}
.loading-effect-2 > span, .loading-effect-2 > span:before, .loading-effect-2 > span:after,textarea:focus, input[type=text]:focus, input[type=password]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=date]:focus, input[type=month]:focus, input[type=time]:focus, input[type=week]:focus, input[type=number]:focus, input[type=email]:focus, input[type=url]:focus, input[type=search]:focus, input[type=tel]:focus, input[type=color]:focus,select:focus,.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span,.navigation.loadmore a:hover { border-color:#337493}
{
border-color:#337493!important;
}
{
color: #fff !important;
}
{
background-color: #2e363a !important;
}
#Financial_Occult text,#F__x26__O tspan { fill:#595959;}
body { color:#595959}
a,.portfolio-filter li a,.themesflat-portfolio .item .category-post a:hover,.title-section .title,ul.iconlist .list-title a,.breadcrumbs span a, .breadcrumbs a,.breadcrumbs span,h1, h2, h3, h4, h5, h6,strong,.testimonial-content blockquote,.testimonial-content .author-info,.sidebar .widget ul li a,.themesflat_counter.style2 .themesflat_counter-content-right,.themesflat_counter.style2 .themesflat_counter-content-left,.title_related_portfolio,.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation .current, .page-links a:hover, .page-links a:focus,.widget_search .search-form input[type=search],.entry-meta ul,.entry-meta ul.meta-right,.entry-footer strong,.widget.widget_archive ul li:before, .widget.widget_categories ul li:before, .widget.widget_recent_entries ul li:before { color:#595959}
.owl-theme .owl-dots .owl-dot span,.widget .widget-title:after, .widget .widget-title:before,ul.iconlist li.circle:before { background-color:#595959}
.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation:not(.loadmore) .current, .page-links a:hover, .page-links a:focus, .page-links > span { border-color:#595959}
.themesflat-top { background-color:#21536c ; } 
.themesflat-top .border-left:before, .themesflat-widget-languages:before, .themesflat-top .border-right:after { background-color: rgba(255,255,255,0.2);}.themesflat-top,.info-top-right,.themesflat-top a, .themesflat-top .themesflat-socials li a { color:#ffffff ;} 
.themesflat_header_wrap.header-style1,.nav.header-style2,.themesflat_header_wrap.header-style3,.nav.header-style4,.header.widget-header .nav { background-color:#fff;}
#mainnav > ul > li > a { color:#595959;}
#mainnav > ul > li > a:hover,#mainnav > ul > li.current-menu-item > a { color:#377493 !important;}
#mainnav ul.sub-menu > li > a { color:#ffffff!important;}
#mainnav ul.sub-menu { background-color:#377493;}
#mainnav ul.sub-menu > li > a:hover { background-color:#aac9ce!important;}
#mainnav ul.sub-menu > li { border-color:#dde9eb!important;}
.footer_background:before { background-color:#21536c;}
.footer a, .footer, .themesflat-before-footer .custom-info > div,.footer-widgets ul li a,.footer-widgets .company-description p { color:#f9f9f9;}
.bottom { background-color:#337493;}
.bottom .copyright p,.bottom #menu-bottom li a { color:#e5e5e5;}
.white #Financial_Occult text,.white #F__x26__O tspan {
fill: #fff; }.parsley-type, .parsley-required, .parsley-pattern {
    	color: red;
    }
</style>
<link rel='https://api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.8" />
<link rel='shortlink' href='/?p=8679' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fdieu-khoan-mxh-astra%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fdieu-khoan-mxh-astra%2F&#038;format=xml" />
<meta name="generator" content="qTranslate-X 3.4.6.8" />
<link hreflang="x-default" href="/dieu-khoan-mxh-astra/" rel="alternate" />
<link hreflang="vi" href="/dieu-khoan-mxh-astra/" rel="alternate" />
<link hreflang="en" href="/en/term-of-service-astra-network/" rel="alternate" />
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?version=370" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.6.3.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/10/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="/wp-content/uploads/2018/10/cropped-favicon-270x270.png" />
<style type="text/css" id="wp-custom-css">
.page-wrap.sidebar-left #primary.content-area {
/*     margin-top: -40px; */
}
.single-post .page-wrap.sidebar-left #primary.content-area {
margin-top: 0;
}
.themesflat-portfolio .item .featured-post {
text-align: center;
}
.themesflat-portfolio .item .featured-post img, .content-area.fullwidth .themesflat_iconbox.inline-left img {
height: 220px;
width: 100%;
object-fit: cover;
}
/* .content-area.fullwidth .themesflat_iconbox.transparent .title {
height: 55px;
display: flex;
align-items: center;
margin-top: 0;
}
.content-area.fullwidth .themesflat_iconbox.transparent .iconbox-content p:nth-child(2) {
height: 125px;
overflow: hidden;
}
*/
.header.header-sticky .wpmenucartli {
display:none !important;
}
.header.header-sticky #menu-main li:first-child {
display: inline-block !important;
}
#mainnav>ul>li {
margin-left: 25px !important;
}		</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<script data-wpfc-render="false">var Wpfcll={s:[],osl:0,scroll:false,i:function(){Wpfcll.ss();window.addEventListener('load',function(){window.addEventListener("DOMSubtreeModified",function(e){Wpfcll.osl=Wpfcll.s.length;Wpfcll.ss();if(Wpfcll.s.length > Wpfcll.osl){Wpfcll.ls(false);}},false);Wpfcll.ls(true);});window.addEventListener('scroll',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('resize',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('click',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});},c:function(e,pageload){var w=document.documentElement.clientHeight || body.clientHeight;var n=0;if(pageload){n=0;}else{n=(w > 800) ? 800:200;n=Wpfcll.scroll ? 800:n;}var er=e.getBoundingClientRect();var t=0;var p=e.parentNode;if(typeof p.getBoundingClientRect=="undefined"){var pr=false;}else{var pr=p.getBoundingClientRect();}if(er.x==0 && er.y==0){for(var i=0;i < 10;i++){if(p){if(pr.x==0 && pr.y==0){p=p.parentNode;if(typeof p.getBoundingClientRect=="undefined"){pr=false;}else{pr=p.getBoundingClientRect();}}else{t=pr.top;break;}}};}else{t=er.top;}if(w - t+n > 0){return true;}return false;},r:function(e,pageload){var s=this;var oc,ot;try{oc=e.getAttribute("data-wpfc-original-src");ot=e.getAttribute("data-wpfc-original-srcset");if(s.c(e,pageload)){if(oc || ot){if(e.tagName=="DIV" || e.tagName=="A"){e.style.backgroundImage="url("+oc+")";e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");}else{if(oc){e.setAttribute('src',oc);}if(ot){e.setAttribute('srcset',ot);}e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");if(e.tagName=="IFRAME"){e.onload=function(){if(typeof window.jQuery !="undefined"){if(jQuery.fn.fitVids){jQuery(e).parent().fitVids({customSelector:"iframe[src]"});}}var s=e.getAttribute("src").match(/templates\/youtube\.html\#(.+)/);var y="https://www.youtube.com/embed/";if(s){try{var i=e.contentDocument || e.contentWindow;if(i.location.href=="about:blank"){e.setAttribute('src',y+s[1]);}}catch(err){e.setAttribute('src',y+s[1]);}}}}}}else{if(e.tagName=="NOSCRIPT"){if(jQuery(e).attr("data-type")=="wpfc"){e.removeAttribute("data-type");jQuery(e).after(jQuery(e).text());}}}}}catch(error){console.log(error);console.log("==>",e);}},ss:function(){var i=Array.prototype.slice.call(document.getElementsByTagName("img"));var f=Array.prototype.slice.call(document.getElementsByTagName("iframe"));var d=Array.prototype.slice.call(document.getElementsByTagName("div"));var a=Array.prototype.slice.call(document.getElementsByTagName("a"));var n=Array.prototype.slice.call(document.getElementsByTagName("noscript"));this.s=i.concat(f).concat(d).concat(a).concat(n);},ls:function(pageload){var s=this;[].forEach.call(s.s,function(e,index){s.r(e,pageload);});}};document.addEventListener('DOMContentLoaded',function(){wpfci();});function wpfci(){Wpfcll.i();}</script>
</head>
<body class="page-template page-template-tpl page-template-page_fullwidth page-template-tplpage_fullwidth-php page page-id-8679  has-topbar header_sticky page-fullwidth wide sidebar-left bottom-center wpb-js-composer js-comp-ver-5.4.7 vc_responsive vi">
<div class="themesflat-boxed">
<!-- Preloader -->
<div class="preloader">
<div class="clear-loading loading-effect-2">
<span></span>
</div>
</div>
<!-- Top -->
<div class="themesflat-top header-style1">    
<div class="container">
<div class="container-inside">
<div class="content-left">
<ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-chooser">
<li class="lang-vi active"><a href="/vi/dieu-khoan-mxh-astra/" hreflang="vi" title="VI (vi)" class="qtranxs_image qtranxs_image_vi"><img src="/wp-content/plugins/qtranslate-x/flags/vn.png" alt="VI (vi)" /><span style="display:none">VI</span></a></li>
<li class="lang-en"><a href="/en/term-of-service-astra-network/" hreflang="en" title="EN (en)" class="qtranxs_image qtranxs_image_en"><img src="/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN (en)" /><span style="display:none">EN</span></a></li>
</ul><div class="qtranxs_widget_end"></div>
<ul>
<li class="border-right">
<i class="fa fa-phone"></i><a href="tel:0247 1080 285" target="_top"> (+84) 0247 1080 285</a>     
</li>
<li>
<i class="fa fa-envelope"></i> <a href="mailto:info@vinsofts.com" target="_top">  info@vinsofts.com</a> 
</li>
</ul>	
</div><!-- /.col-md-7 -->
<div class="content-right">
<ul class="themesflat-socials">
<li class="facebook">
<a class="title" href="">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="/dieu-khoan-mxh-astra/linkedin.com/company/vinsofts" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
<div class="info-top-right border-left">
<span><i class="fa fa-question-circle"></i>Bạn cần tư vấn?</span>
<a class="appoinment" href="#">Yêu cầu báo giá</a>
</div>            </div><!-- /.col-md-5 -->
</div><!-- /.container -->
</div><!-- /.container -->        
</div><!-- /.top -->
<div class="popup">
<div id="myModal2" class="modal fade in" role="dialog">
<div class="modal-dialog modal-lg">
        <!-- Modal content form contact-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div role="form" class="wpcf7" id="wpcf7-f3777-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate" data-parsley-validate>
                        <!-- data-parsley-validate -->
                        <div class="row gutter-10 contactform4">
                            <h4 class="title-form" style="display:none">Quý khách vui lòng điền form bên dưới và gửi cho chúng tôi. Chúng tôi sẽ liên hệ lại ngay.</h4>
                            <div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name">
                                <input type="text" name="your-name" required="required" data-parsley-trigger="change focusout"
                                data-parsley-required-message="Vui lòng nhập đầy đủ thông tin" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" placeholder="Họ tên *" /></span>
                                            </div>
                            <div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email">
                                <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"required="required" data-parsley-trigger="change focusout"
                                data-parsley-required-message="Vui lòng nhập đầy đủ thông tin"  data-parsley-pattern="^[a-z A-Z].*@.+"
                                data-parsley-pattern-message="Email không đúng định dạng" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
                            <div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone">
                                <input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"data-parsley-trigger="change focusout" required="required"
                                data-parsley-required-message="Vui lòng nhập đầy đủ thông tin"  data-parsley-pattern="[0-9]{10,11}"
                                data-parsley-pattern-message="SĐT không đúng định dạng (VD: 0912345678, +84912345678)" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" /></span> </div>
                            <div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Câu hỏi" /></span> </div>
                            <div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can">
                                <select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false">
                                        <option value="Bạn quan tâm đến dịch vụ nào?">Bạn quan tâm đến dịch vụ nào?</option>
                                        <option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option>
                                        <option value="Phát triển ứng dụng web, thiết kế website">Phát triển ứng dụng web, thiết kế website</option>
                                        <option value="Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin">Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin</option>
                                        <option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option>
                                        <option value="Mua Phần mềm quản trị doanh nghiệp (ERP)">Mua Phần mềm quản trị doanh nghiệp (ERP)</option>
                                        <option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option>
                                        <option value="Thuê nhân sự cho dự án">Thuê nhân sự cho dự án</option>
                                    </select></span></div>
                            <div class="col-sm-6">
                                <span>Đính kèm file:</span><br />
                                <span class="wpcf7-form-control-wrap file-978"><input onChange="uploadFileToServer()" type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" />
                            </div>
                            <div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung chi tiết"></textarea></span> </div>
                         
                            <div class="col-sm-2 col-xs-12"><input type="submit" value="GỬI" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
                        </div>
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Form contact-->
<div id="myModal3" class="modal fade in" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content form contact-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<div role="form" class="wpcf7" id="wpcf7-f3777-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/dieu-khoan-mxh-astra/#wpcf7-f3777-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3777" />
<input type="hidden" name="_wpcf7_version" value="5.0.5" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3777-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<div class="row gutter-10 contactform4">
<h4 class="title-form" style="display:none">Quý khách vui lòng điền form bên dưới và gửi cho chúng tôi. Chúng tôi sẽ liên hệ lại ngay.</h4>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ tên *" /></span></div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" /></span> </div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Câu hỏi" /></span> </div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can"><select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false"><option value="Bạn quan tâm đến dịch vụ nào?">Bạn quan tâm đến dịch vụ nào?</option><option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option><option value="Phát triển ứng dụng web, thiết kế website">Phát triển ứng dụng web, thiết kế website</option><option value="Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin">Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin</option><option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option><option value="Mua Phần mềm quản trị doanh nghiệp (ERP)">Mua Phần mềm quản trị doanh nghiệp (ERP)</option><option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option><option value="Thuê nhân sự cho dự án">Thuê nhân sự cho dự án</option></select></span></div>
<div class="col-sm-6">
<span>Đính kèm file:</span><br />
<span class="wpcf7-form-control-wrap file-978"><input type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" /></span>
</div>
<div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung chi tiết"></textarea></span> </div>
<div class="col-sm-10 col-xs-12 captcha-form-vins">
<div class="wpcf7-form-control-wrap"><div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
<noscript>
<div style="width: 302px; height: 422px;">
<div style="width: 302px; height: 422px; position: relative;">
<div style="width: 302px; height: 422px; position: absolute;">
<iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
</iframe>
</div>
<div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
</textarea>
</div>
</div>
</div>
</noscript>
</div>
</div>
<div class="col-sm-2 col-xs-12"><input type="submit" value="GỬI" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>          </div>
</div>
</div>
</div>
</div>
<div id="popup-team-member" class="popup-2">
<div class="modal fade" id="myModa1" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="row">
<div class="image-members-full col-md-4 col-sm-6 col-xs-12">
<div class="image">
</div>
</div>
<div class="title-modal col-md-8 col-sm-6 col-xs-12">
<div class="title">
<h3 class="modal-title"></h3>
<span class="modal-title-mini"></span>
<div class="social-content">
</div>
</div>
</div>
</div>
</div>
<div class="line-color"></div>
<div class="modal-body">
<div class="content-member">
</div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
</div>
<style type="text/css">
.themesflat-top .content-left {
float: none;
}
.themesflat-top .content-left ul{
float: left;
}
.themesflat-top .content-left ul.language-chooser{
/*margin-right: 15px;*/
}
.themesflat-top .content-left ul.language-chooser li{
padding-left: 0px;
padding-right: 10px;
}
.themesflat-top .content-left ul.language-chooser li a::after {
content: "";
border-right: 1px solid #d8d8d8;
margin-left: 6px;
}
.themesflat-top .content-left ul.language-chooser li:last-child a::after {
border: 0px;
content: "";
}
.themesflat-top .content-left ul.language-chooser li.active a,
.themesflat-top .content-left ul.language-chooser li a:hover{
color: #3d9be9;
opacity: 1;
}
.themesflat-top .content-left ul.language-chooser li a{
padding: 0px;
font-size: 13px;
color: #ffffff94;
opacity: 0.4;
}
.themesflat-top .content-left ul.language-chooser li a img{
width: 22px;
height: 14px;
}
/*.themesflat-top .content-left ul.language-chooser li:nth-child(2) a img{
height: 16px;
}*/
</style>
<style type="text/css">
.popup #myModal2{
z-index: 99999;
}
</style><div class="themesflat_header_wrap header-style1" data-header_style="header-style1">
<!-- Header -->
<header id="header" class="header header-style1" >
<div class="container nav">
<div class="row">
<div class="col-md-12">
<div class="header-wrap clearfix">
<div id="logo" class="logo" >                  
<a href="/"  title="Công ty phần mềm Vinsofts">
<img class="site-logo"  src="/wp-content/uploads/2019/06/Logo-Vinsofts.png" alt="Công ty phần mềm Vinsofts"  data-retina="/wp-content/uploads/2019/06/Logo-Vinsofts.png" />
</a>
</div>
<div class="show-search">
<a href="#"><i class="fa fa-search"></i></a>         
</div> 
<div class="nav-wrap">
<div class="btn-menu">
<span></span>
</div><!-- //mobile menu button -->
<nav id="mainnav" class="mainnav" role="navigation">
<ul id="menu-main" class="menu"><li id="menu-item-2167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2167"><a href="/ve-chung-toi/">Về chúng tôi</a>
<ul class="sub-menu">
<li id="menu-item-3792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3792"><a href="/ve-chung-toi/">Giới thiệu chung</a></li>
<li id="menu-item-3789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3789"><a href="/lich-su-cong-ty/">Lịch sử công ty</a></li>
<li id="menu-item-3790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3790"><a href="/doi-tac/">Đối tác</a></li>
<li id="menu-item-3791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3791"><a href="/nhan-su/">Nhân sự</a></li>
<li id="menu-item-6972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6972"><a href="/ho-so-nang-luc/">Hồ sơ năng lực</a></li>
<li id="menu-item-6108" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6108"><a href="/danh-muc/hoat-dong-noi-bat/">Hoạt động công ty</a></li>
</ul>
</li>
<li id="menu-item-2994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2994"><a href="/dich-vu/">Dịch vụ</a>
<ul class="sub-menu">
<li id="menu-item-2933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2933"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
<li id="menu-item-2934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2934"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
<li id="menu-item-2932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2932"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
<li id="menu-item-2935" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2935"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
<li id="menu-item-2943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2943"><a href="/kiem-thu-phan-mem-va-dich-vu-dam-bao-chat-luong/">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</a></li>
<li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="/kiem-tra-tu-dong/">Kiểm tra tự động</a></li>
<li id="menu-item-2942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2942"><a href="/trung-tam-phat-trien-phan-mem-offshore/">Trung tâm phát triển phần mềm offshore</a></li>
</ul>
</li>
<li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3202"><a href="/du-an-tieu-bieu/">Dự án tiêu biểu</a></li>
<li id="menu-item-4112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4112"><a href="/jobs">Tuyển dụng</a>
<ul class="sub-menu">
<li id="menu-item-4912" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4912"><a href="/job_cats/lap-trinh-mobile/">Lập trình Mobile</a></li>
<li id="menu-item-4911" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4911"><a href="/job_cats/lap-trinh-web/">Lập trình Web</a></li>
<li id="menu-item-4913" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4913"><a href="/job_cats/lap-trinh-blockchain/">Lập trình Blockchain</a></li>
<li id="menu-item-4810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a href="/ung-tuyen/">Ứng tuyển ngay</a></li>
</ul>
</li>
<li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="/tin-tuc/">Tin tức</a></li>
<li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="/lien-he/">Liên hệ</a></li>
</ul>        <div class="info-top-right border-left" style="display: none">
<a class="appoinment" href="#" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Yêu cầu Báo giá</a>
<a class="hotline-header" href="tel:(+84) 0247 1080 285">
<i class="fa fa-phone" aria-hidden="true"></i> <span>(+84) 0247 1080 285</span>
</a>
<i></i>
</div>
</nav><!-- #site-navigation -->  
</div><!-- /.nav-wrap -->                                
</div><!-- /.header-wrap -->
<div class="submenu top-search widget_search">
<form role="search" method="get" class="search-form" action="/">
<label>
<span class="screen-reader-text">Tìm kiếm cho:</span>
<input type="search" class="search-field" placeholder="Tìm kiếm &hellip;" value="" name="s" />
</label>
<input type="submit" class="search-submit" value="Tìm kiếm" />
</form>                </div> 
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->    
</header><!-- /.header --></div> 	<!-- Page Title -->
<!-- Page title -->
<div class="page-title">
<div class="overlay"></div>   
<div class="container"> 
<div class="row">
<div class="col-md-12 page-title-container">
<div class="breadcrumb-trail breadcrumbs">
<span class="trail-browse"></span> <span class="trail-begin"><a href="/" title="Công ty phần mềm Vinsofts">Home</a></span>
<span class="sep">&gt;</span> <span class="trail-end">Điều khoản MXH Astra</span>
</div> 
<h1>Điều khoản MXH Astra</h1>            </div><!-- /.col-md-12 -->  
</div><!-- /.row -->  
</div><!-- /.container -->                      
</div><!-- /.page-title --> 	
<div id="content" class="page-wrap sidebar-left">
<div class="container content-wrapper">
<div class="row">
<div class="col-md-12">
<div id="primary" class="content-area-full-width">
<main id="main" class="site-main" role="main">
<p><strong>Điều khoản dịch vụ</strong></p>
<p>Chào mừng bạn đến với Mạng xã hội du lịch Astra!</p>
<p>Astra là Mạng xã hội tiên phong trên thế giới về du lịch. Dựa trên nền tảng công nghệ blockchain, Astra xây dựng một cộng đồng những người ưa xê dịch, tích cực, sáng tạo, luôn bắt kịp với những cải tiến mới mẻ; kết nối các cá nhân với nhau và với các nhà cung cấp dịch vụ; đồng thời hỗ trợ phát triển các doanh nghiệp trong hệ sinh thái của chúng tôi. Để sử dụng dịch vụ của Astra và trở thành một thành viên trong cộng đồng yêu du lịch trên toàn cầu, bạn vui lòng xem kỹ các điều khoản dịch vụ dưới đây do Astra Network xây dựng. Các điều khoản này sẽ điều chỉnh mọi hoạt động của bạn trên Mạng xã hội du lịch Astra và cách bạn sử dụng các tính năng, dịch vụ và công nghệ mà chúng tôi cung cấp.</p>
<p>Mạng xã hội Astra đem đến cho bạn những trải nghiệm miễn phí về du lịch trên nền tảng online, nơi mỗi cái chạm tay của bạn đều có thể mở ra một thế giới mới với những danh lam thắng cảnh, món ăn, nền văn hóa khác biệt. Tại Astra, chúng tôi quan niệm những đóng góp của người dùng chính là điều làm nên sức hấp dẫn cho một mạng xã hội, bởi vậy, người dùng không những không phải trả phí để sử dụng dịch vụ của Astra, mà còn có thể nhận về những phần thưởng tương đương với giá trị của các nội dung mà mình xây dựng và chia sẻ trên Mạng xã hội.</p>
<p>Đổi lại, để duy trì cộng đồng và nền tảng, chúng tôi sử dụng nguồn lực là chi phí mà các doanh nghiệp và tổ chức trả để quảng cáo sản phẩm và dịch vụ của họ trên Mạng xã hội Astra. Các doanh nghiệp và tổ chức nêu trên đều là các đối tác trong hệ sinh thái của Astra Network, tức là các nhà cung cấp dịch vụ du lịch. Khi tham gia Mạng xã hội Astra, bạn cho phép chúng tôi hiển thị các quảng cáo này trên bảng tin của bạn. Chúng tôi cam kết mang đến những quảng cáo với thông tin chất lượng, đã được kiểm duyệt và xác minh kỹ lưỡng nhằm cung cấp tới bạn những lựa chọn thú vị cho nhu cầu du lịch của bạn trong tương lai. Chúng tôi sử dụng các dữ liệu thu thập được về hành vi và tương tác của bạn trên Mạng xã hội Astra để quyết định các nội dung quảng cáo nào được phép xuất hiện trên bảng tin mà bạn sẽ đọc.</p>
<p>Chúng tôi không bán dữ liệu cá nhân của bạn cho các bên thứ ba (doanh nghiệp, tổ chức, cơ quan sử dụng dịch vụ quảng cáo) và cũng không để lộ các thông tin nhận dạng của bạn (tên, địa chỉ email, số điện thoại và những thông tin liên hệ khác) với các đơn vị kể trên trừ khi được bạn cho phép. Thay vào đó, khi mua quảng cáo, các bên thứ ba có trách nhiệm cung cấp cho chúng tôi thông tin về các nhóm đối tượng, nghề nghiệp, độ tuổi, sở thích mà họ muốn tiếp cận và chúng tôi sẽ sử dụng thuật toán để hiển thị quảng cáo của họ tới những người dùng hiện đang quan tâm và có nhu cầu với loại sản phẩm và dịch vụ họ cung cấp. Chúng tôi sẽ đánh giá hiệu quả quảng cáo và thông báo với các doanh nghiệp, tổ chức để họ nắm được phản hồi của người dùng Astra với nội dung của họ như thế nào.</p>
<ol>
<li><strong>Dịch vụ chúng tôi cung cấp và các tính năng của chúng tôi</strong></li>
</ol>
<p>Astra xây dựng một cộng đồng của những người yêu và có mong muốn tìm hiểu về du lịch, nơi các thông tin đã được kiểm chứng, giàu tính tin cậy và các trải nghiệm phong phú được trao đổi, từ đó truyền cảm hứng xê dịch. Thông qua những trải nghiệm được chia sẻ, chúng tôi kết nối các nền văn hóa trên khắp các châu lục với nhau và hướng đến tạo lập một cộng đồng du lịch thông minh trong tương lai không xa. Để hiện thực hóa được những mục tiêu này, chúng tôi mang đến cho người dùng những sản phẩm và dịch vụ như dưới đây:</p>
<ul>
<li>Kết nối: Astra kết nối bạn với các cá nhân và tổ chức mà bạn quan tâm, gắn kết bạn với cộng đồng những người yêu du lịch trên khắp thế giới. Không chỉ vậy, Astra còn là cầu nối gắn kết người dùng với nhà cung cấp dịch vụ du lịch, tạo điều kiện để nhà cung cấp dịch vụ lắng nghe phản hồi của khách hàng, thấu hiểu nhu cầu của họ và nỗ lực đem đến cho họ những trải nghiệm tốt nhất. Với vòng tròn tương tác như vậy, chúng tôi tin rằng mô hình Mạng xã hội Astra là mô hình đem lại cho cộng đồng nhiều giá trị &#8211; hay còn gọi là “du lịch kết nối”. Với nhà cung cấp dịch vụ du lịch, đây là môi trường để bạn kết nối với khách hàng thông qua kênh thông tin đã được cá nhân hóa và dễ dàng tương tác. Với người dùng, du lịch không còn là hành trình đơn độc, là những chuyến đi mà dư âm của nó kết thúc ngay khi bạn trở về, mà là hành trình của sự kết nối và kéo dài. Tại Astra, chúng tôi quan niệm một hành trình thú vị phải là hành trình truyền cảm hứng cho nhiều hành trình khác.</li>
<li>Chia sẻ: Astra xây dựng không gian mở giúp người dùng thoải mái chia sẻ các trải nghiệm về du lịch. Bạn mới tham quan một địa điểm nổi tiếng vào tuần trước, ngạc nhiên trước ẩm thực hấp dẫn của nơi đây, thích thú trước những trải nghiệm thú vị nhưng cũng không ít lần chưa hài lòng về chất lượng dịch vụ hay chi phí? Đừng ngần ngại chia sẻ câu chuyện của bạn với cộng đồng du lịch Astra để nhận về những phần thưởng có giá trị quy đổi cao và cả sự tín nhiệm của những người dùng khác. Tại Astra, chúng tôi không trả công ưu tiên cho số ít users có khả năng xây dựng nội dung mà thúc đẩy quyền lợi bình đẳng giữa tất cả người dùng bằng cách trao thưởng cho tất cả dựa trên giá trị mà người dùng đóng góp thông qua kinh nghiệm cá nhân của họ.</li>
<li>Cơ chế trả thưởng: Khi tham gia Mạng xã hội Astra, xây dựng và đăng tải các nội dung chân thực, thú vị về du lịch, bạn sẽ nhận về các phần thưởng tương xứng với giá trị nội dung mà mình đóng góp. Công cụ đo đếm giá trị nội dung của Astra là Proof of Rating, công thức tính toán bao gồm các thông số về số lượng các loại tương tác, chỉ số uy tín của người đăng bài, chỉ số uy tín của người tương tác, thông số về loại content.Trong hệ sinh thái Astra, đơn vị điểm thưởng là Astra Point. Người dùng tham gia xây dựng nội dung càng sớm, xếp hạng (rank) và độ uy tín càng cao, giá trị mà họ nhận về sẽ càng lớn. Phần thưởng mà người dùng nhận được sẽ có giá trị quy đổi thành các sản phẩm và dịch vụ tùy theo nhu cầu của người dùng.</li>
<li>Tích lũy kiến thức: Astra xây dựng kho trải nghiệm du lịch tập trung và không giới hạn giúp người dùng tích lũy kiến thức cho mục đích du lịch trong tương lai. Tham gia Mạng xã hội Astra, bạn sẽ có cơ hội tiếp cận với một cộng đồng du lịch rộng lớn và làm giàu vốn kiến thức cá nhân bằng chính những kinh nghiệm mà người khác chia sẻ. Đội ngũ xây dựng Astra cũng đã phát triển bộ công cụ lọc thông tin để đảm bảo đem đến cho người dùng những trải nghiệm du lịch gián tiếp chân thực và hữu ích, giảm thiểu tối đa các thông tin sai lệch.</li>
<li>Cung cấp các ưu đãi dịch vụ du lịch: Tham gia Mạng xã hội Astra, người dùng có cơ hội nhận được nhiều ưu đãi lớn do các nhà cung cấp dịch vụ du lịch phát hành như voucher nghỉ dưỡng miễn phí tại khách sạn hoặc resort, discount khi dùng bữa tại các nhà hàng cao cấp. Ngoài ra, người dùng cũng có thể mua các ưu đãi này bằng cách sử dụng các điểm thưởng đã được tích lũy trong tài khoản cá nhân tại Astra khi tham gia xây dựng nội dung.</li>
<li>Cung cấp dịch vụ tiếp thị dành cho các đơn vị và tổ chức: Astra xây dựng các tài khoản doanh nghiệp với các công cụ hỗ trợ và marketing hiệu quả để các đơn vị tiếp cận và quảng bá hình ảnh của mình tới công chúng, thu hút khách hàng tiềm năng. Ở một thị trường đã được định hướng sẵn cho mảng du lịch như Astra, các nhà cung cấp dịch vụ du lịch sẽ tiết giảm được thời gian, chi phí trong việc lan tỏa hình ảnh của mình tới các khách hàng, đối tác. Thông qua kênh trung gian là Astra, nhà cung cấp dịch vụ cũng có thể lắng nghe phản hồi của khách hàng, kịp thời cải thiện chất lượng và ghi điểm trong mắt các khách hàng tiềm năng với phong cách chuyên nghiệp.</li>
<li>Sử dụng và phát triển công nghệ tiên tiến để hỗ trợ cộng đồng: Chúng tôi sử dụng và phát triển các công nghệ tiên tiến như blockchain, trí tuệ nhân tạo và hệ thống máy học để đảm bảo người dùng của Astra có thể trải nghiệm các dịch vụ tiện ích và thông minh nhất về du lịch. Chúng tôi đã phát triển công cụ Proof of Rating để đo lường giá trị nội dung mà người dùng đăng tải, thể hiện sự tôn trọng dành cho người dùng và tinh thần minh bạch thông qua cơ chế trả thưởng hấp dẫn. Một bộ công cụ khác mà chúng tôi sử dụng là Proof of Trust, có vai trò tương tự như một lớp lọc thông tin, giúp hạn chế tối đa các review ảo, giả mạo, lừa đảo; bảo vệ cộng đồng khỏi những nhân tố độc hại, các tư tưởng cực đoan, sai lệch, gây chia rẽ.</li>
<li>Nghiên cứu các phương thức cải thiện dịch vụ: Thông qua dữ liệu về hành vi và tương tác thu thập được từ người dùng, chúng tôi phân tích và đưa ra những cải tiến mới nhằm giúp người dùng có những trải nghiệm tuyệt vời hơn. Đồng thời, chúng tôi cũng tiến hành các cuộc khảo sát và thử nghiệm để nắm bắt quan điểm của người dùng về các tính năng mới.</li>
</ul>
<ol start="2">
<li><strong>Cách chúng tôi duy trì dịch vụ </strong></li>
</ol>
<p>Các trải nghiệm của bạn trên Mạng xã hội Astra là hoàn toàn miễn phí. Bạn không phải trả bất cứ một khoản phí nào để sử dụng các dịch vụ và tính năng của Astra. Hơn thế, bạn có cơ hội nhận về những phần thưởng có giá trị quy đổi cao. Đổi lại, chúng tôi vận hành và duy trì dịch vụ của mình bằng cách thu phí từ các đơn vị có nhu cầu quảng cáo trên Mạng xã hội Astra. Các nhà cung cấp dịch vụ du lịch và doanh nghiệp có thể hợp tác quảng cáo với chúng tôi dưới 3 hình thức: quảng cáo tiếp cận (quảng cáo với độ phủ cao, không phân biệt rõ đối tượng dựa trên các đặc điểm về nhân khẩu học), quảng cáo hiển thị (quảng cáo nhắm đến các nhóm đối tượng mục tiêu cụ thể, rõ ràng, mức độ chuẩn xác cao) và quảng cáo đề xuất (quảng cáo gợi ý cho người dùng các doanh nghiệp và tổ chức được người dùng đánh giá với mức độ từ tốt đến xuất sắc).</p>
<p>Khi mua các gói quảng cáo của chúng tôi, các đơn vị có trách nhiệm cung cấp các thông tin về đối tượng mục tiêu mà họ muốn tiếp cận. Chúng tôi sử dụng các dữ liệu thu thập được về hành vi và tương tác của bạn trên Mạng xã hội Astra để quyết định các nội dung quảng cáo nào được phép xuất hiện trên bảng tin mà bạn sẽ đọc. Như vậy, chúng tôi không bán dữ liệu cá nhân của bạn và cũng không để lộ các thông tin liên hệ mà bạn cung cấp cho chúng tôi với một bên thứ ba. Chúng tôi tôn trọng quyền riêng tư của người dùng và cam kết đem lại những trải nghiệm thú vị.</p>
<ol start="3">
<li><strong>Cam kết của bạn với Astra và cộng đồng của chúng tôi</strong></li>
</ol>
<p>Chúng tôi cung cấp các dịch vụ này cho bạn và người khác nhằm thúc đẩy sứ mệnh của mình. Để đổi lại, chúng tôi cần bạn thực hiện các cam kết sau:</p>
<p><strong>Đối tượng có thể sử dụng Mạng xã hội du lịch Astra </strong></p>
<p>Chúng tôi muốn dịch vụ, sản phẩm của mình được toàn diện nhất có thể nhưng vẫn phải đảm bảo an toàn, bảo mật và tuân thủ pháp luật. Chính vì vậy, Astra cần bạn cam kết, tuân thủ một vài hạn chế sau để trở thành một thành viên của cộng đồng Mạng xã hội Du lịch Astra.</p>
<ul>
<li>Bạn phải sử dụng chính tên thật của bạn, cung cấp thông tin chính xác về bản thân.</li>
<li>Chỉ tạo một tài khoản (của riêng bạn) và sử dụng dòng thời gian cho mục đích cá nhân.</li>
<li>Không chia sẻ mật khẩu, cấp cho người khác quyền truy cập vào tài khoản Astra của bạn hoặc chuyển tài khoản của bạn cho bất kỳ một ai khác mà không được chúng tôi cho phép.</li>
</ul>
<p>Chúng tôi luôn cố gắng đưa Mạng xã hội Du lịch Astra đến rộng rãi với mọi người trên toàn thế giới. Và bạn sẽ có thể sử dụng Mạng xã hội Du lịch Astra nếu:</p>
<ul>
<li>Bạn đủ 13 tuổi trở lên (hoặc ở độ tuổi hợp pháp tối thiểu tại quốc gia bạn để sử dụng Mạng xã hội Du lịch Astra).</li>
<li>Bạn không phải tội phạm tình dục đã bị kết án.</li>
<li>Bạn chưa bị chúng tôi vô hiệu hóa tài khoản trước đây do vi phạm pháp luật hoặc vi phạm bất kỳ chính sách nào của chúng tôi.</li>
<li>Bạn không bị cấm nhận các dịch vụ, sản phẩm của chúng tôi theo luật hiện hành hoặc không bị cấm thực hiện các khoản thanh toán liên quan đến dịch vụ nếu bạn thuộc danh sách bên bị từ chối đang áp dụng.</li>
</ul>
<p><strong>Những điều bạn có thể chia sẻ và thực hiện trên Mạng xã hội Du lịch Astra</strong></p>
<p>Chúng tôi muốn mọi người sử dụng Mạng xã hội Du lịch Astra để thể hiện bản thân và đánh giá chân thực về các địa điểm du lịch trên phương diện cởi mở và an toàn, đồng thời chúng tôi tôn trọng tự do ngôn luận và tôn trọng người dùng. Chúng tôi hy vọng và không ngừng nỗ lực để xây dựng Mạng xã hội Du lịch Astra trở thành không gian cộng đồng thú vị, truyền cảm hứng về du lịch. Đổi lại, chúng tôi cần bạn cam kết với chúng tôi rằng bạn sẽ không tham gia những hành vi được mô tả dưới đây (hoặc tạo điều kiện hay hỗ trợ người khác thực hiện các hành vi đó) để đảm bảo môi trường Mạng xã hội Du lịch Astra luôn minh bạch, công bằng và hấp dẫn:</p>
<ul>
<li>Bạn không được xây dựng, đăng tải hay chia sẻ bất kỳ nội dung nào chống lại nhà nước Cộng hòa xã hội chủ nghĩa Việt Nam; gây phương hại đến an ninh quốc gia, trật tự, an toàn xã hội; đe dọa đến khối đại đoàn kết toàn dân; tuyên truyền chiến tranh và xung đột; gây thù hận, mâu thuẫn giữa các dân tộc, sắc tộc, tôn giáo; tuyên truyền, kích động bạo lực, căm ghét, dâm ô, đồi trụy, tội ác, tệ nạn xã hội, mê tín dị đoan; phá hoại thuần phong, mỹ tục của dân tộc; đe dọa xói mòn bản sắc văn hóa dân tộc; tiết lộ bí mật nhà nước, bí mật quân sự, an ninh, kinh tế, đối ngoại và những thông tin mật quan trọng của Nhà nước Cộng hòa xã hội chủ nghĩa Việt Nam.</li>
<li>Bạn không được thực hiện các hành vi trái với luật pháp của Nhà nước Cộng hòa Xã hội chủ nghĩa Việt Nam, ví dụ: đăng tải các thông tin mua &#8211; bán các mặt hàng cấm theo luật của Nhà nước Cộng hòa xã hội chủ nghĩa Việt Nam trên Mạng xã hội Du lịch Astra, truyền bá các văn bản thông tin chống phá Nhà nước, bôi nhọ dân tộc, lan truyền các hình thức văn bản thông tin bị cấm theo quy định của Nhà nước và pháp luật.</li>
<li>Bạn không được lan truyền trên diễn đàn những nội dung, đường dẫn, liên kết… bài xích, bôi nhọ, làm mất danh dự người khác, xúc phạm và xâm phạm quyền lợi, uy tín, đời tư của các cá nhân hay thành viên khác. Bạn không được xây dựng và đăng tải các nội dung thông tin xuyên tạc, vu khống, xúc phạm uy tín của tổ chức; danh dự, nhân phẩm của công dân; gây hiểu nhầm, phân biệt đối xử hoặc lừa đảo.</li>
<li>Bạn không được mạo danh người khác hoặc cung cấp thông tin không chính xác. Không nhất thiết bạn phải tiết lộ danh tính của mình trên Astra Network những bạn phải cung cấp cho chúng tôi thông tin chính xác và cập nhật (bao gồm thông tin đăng ký). Ngoài ra, bạn không được mạo danh người khác và bạn không thể tạo tài khoản cho người khác trừ khi được họ cho phép.</li>
<li>Bạn không được gây rối, phá hoại hệ thống thiết bị và cản trở trái pháp luật việc quản lý, cung cấp, sử dụng các dịch vụ Internet và thông tin điện tử trên Internet. Đồng thời, bạn không được đánh cắp và sử dụng trái phép mật khẩu, khoá mật mã và thông tin riêng của các tổ chức, cá nhân trên Internet. Tuyệt đối nghiêm cấm việc gửi hoặc đăng tải thông tin, phần mềm có đính kèm virus hoặc các thành phần gây hại khác, hay sử dụng bất kỳ công cụ nào can thiệp và làm phương hại đến hệ thống của Mạng xã hội du lịch Astra.</li>
</ul>
<p>Chúng tôi có thể gỡ hoặc hạn chế quyền truy cập vào nội dung vi phạm các quy định này.</p>
<p><strong>Các quyền bạn cấp cho chúng tôi</strong></p>
<p>Chúng tôi cần bạn cấp một số quyền để cung cấp dịch vụ:</p>
<ol>
<li>Quyền sử dụng nội dung của bạn:</li>
</ol>
<ul>
<li>Một số nội dung mà bạn chia sẻ hoặc tải lên, chẳng hạn như ảnh hay video, có thể được luật sở hữu trí tuệ bảo vệ.</li>
<li>Bạn có quyền sở hữu trí tuệ (chẳng hạn như bản quyền hoặc nhãn hiệu hàng hóa) đối với mọi nội dung mà mình tạo và chia sẻ trên Astra Network. Ngoài ra, quyền của bạn đối với nội dung của bạn không có thay đổi gì.</li>
<li>Tuy nhiên, để chúng tôi cung cấp dịch vụ, bạn cần cấp cho chúng tôi một số quyền pháp lý (gọi là &#8220;giấy phép&#8221;) để sử dụng nội dung đó. Việc này chỉ nhằm mục đích cung cấp và cải thiện dịch vụ, sản phẩm của chúng tôi được mô tả trong Phần 1 ở trên. Cụ thể, khi bạn chia sẻ, đăng hoặc tải lên nội dung được bảo vệ theo quyền sở hữu trí tuệ trên hoặc liên quan đến dịch vụ, sản phẩm của chúng tôi, bạn cấp cho chúng tôi một giấy phép không độc quyền, có thể chuyển nhượng, có thể cấp phép lại và miễn phí bản quyền trên toàn thế giới để lưu trữ, sử dụng, phân phối, sửa đổi, chạy, sao chép, trình diễn hoặc hiển thị công khai, dịch và tạo sản phẩm phái sinh từ nội dung của bạn. Bạn có thể xóa từng hoặc tất cả nội dung bằng cách xóa tài khoản của mình. Giấy phép này sẽ hết hiệu lực khi nội dung của bạn bị xóa khỏi hệ thống của chúng tôi.</li>
</ul>
<ol>
<li>    Quyền sử dụng tên, ảnh đại diện và thông tin về hành động của bạn với quảng cáo và nội dung được tài trợ: Bạn cho phép chúng tôi sử dụng tên, ảnh đại diện và thông tin về hành động của bạn trên Mạng xã hội Du lịch Astra bên cạnh hoặc liên quan đến quảng cáo, ưu đãi và các nội dung được tài trợ khác do chúng tôi hiển thị trên các Sản phẩm của mình mà không cần trả thù lao cho bạn. Ví dụ: chúng tôi có thể cho bạn bè của bạn biết rằng bạn quan tâm đến một sự kiện được quảng cáo hoặc đã thích Trang do một thương hiệu (đã trả tiền cho chúng tôi để hiển thị quảng cáo của họ trên Astra) tạo ra. Các quảng cáo như vậy sẽ chỉ hiển thị với những người được bạn cho phép xem hành động của mình trên Astra. Bạn có thể tìm hiểu thêm về cài đặt và tùy chọn quảng cáo.</li>
<li>Quyền cập nhật phần mềm: Việc bạn tải xuống hoặc sử dụng phần mềm của chúng tôi đồng nghĩa với việc bạn cho phép chúng tôi tải xuống và cài đặt các bản cập nhật phần mềm, nếu có.</li>
<li>Quyền cấm truy cập: Chúng tôi có toàn quyền, vào mọi lúc, cấm hoặc từ chối truy cập của người dùng vào Mạng xã hội Du lịch Astra hoặc bất kỳ phần nào của website ngay lập tức và không cần báo trước nếu chúng tôi cho rằng người dùng đã vi phạm bất cứ điều khoản nào trong bản Quy định này, hoặc việc cấm truy cập xuất phát từ nhận định của chúng tôi, khi chúng tôi cho rằng từ chối đó phù hợp và cần thiết trong thẩm quyền của chúng tôi.</li>
<li><strong> Các quy định bổ sung</strong></li>
</ol>
<p><strong>Chúng tôi sẽ cập nhật các điều khoản</strong></p>
<p>Chúng tôi không ngừng nỗ lực cải thiện các dịch vụ của mình và phát triển những tính năng mới để tạo ra sản phẩm tốt hơn cho bạn cũng như cho cộng đồng của chúng tôi. Do đó, đôi khi chúng tôi cần cập nhật các Điều khoản này để phản ánh chính xác những dịch vụ và cách làm của mình. Chúng tôi sẽ chỉ thực hiện các thay đổi trong trường hợp những quy định này không còn phù hợp nữa hoặc chưa đầy đủ và chỉ khi các thay đổi đó là hợp lý cũng như khi đã xem xét thích đáng lợi ích của bạn.</p>
<p>Chúng tôi sẽ thông báo cho bạn (có thể bằng email hoặc thông qua dịch vụ, sản phẩm của mình) ít nhất 30 ngày trước khi thực hiện các thay đổi đối với những Điều khoản này, đồng thời cho bạn cơ hội xem lại trước khi các thay đổi đó có hiệu lực, trừ khi chúng tôi phải thực hiện các thay đổi theo yêu cầu của luật pháp. Sau khi bất kỳ Điều khoản đã được cập nhật nào có hiệu lực, nếu tiếp tục sử dụng dịch vụ, sản phẩm của chúng tôi thì bạn sẽ chịu sự ràng buộc của các điều khoản đó.</p>
<p>Chúng tôi hy vọng bạn sẽ tiếp tục sử dụng dịch vụ, sản phẩm của chúng tôi. Tuy nhiên, nếu không đồng ý với các Điều khoản cập nhật và không còn muốn tham gia cộng đồng Mạng xã hội Du lịch Astra nữa, bạn có thể xóa tài khoản của mình bất kỳ lúc nào.</p>
<p><strong>Tạm ngừng hoặc chấm dứt tài khoản</strong></p>
<p>Chúng tôi muốn Mạng xã hội Du lịch Astra trở thành nơi mà mọi người cảm thấy an toàn và được chào đón khi thể hiện bản thân, cũng như chia sẻ suy nghĩ và ý tưởng của mình, cùng trải nghiệm du lịch và ghi lại những cảm xúc, đánh giá chân thực nhất về các địa điểm du lịch.</p>
<p>Nếu chúng tôi xác định được là bạn vi phạm rõ ràng, nghiêm trọng hoặc nhiều lần các Điều khoản hoặc Chính sách của Astra, chúng tôi có thể tạm ngừng hoặc vô hiệu hóa vĩnh viễn quyền truy cập vào tài khoản của bạn. Chúng tôi cũng có thể tạm ngừng hoặc vô hiệu hóa tài khoản nếu bạn nhiều lần xâm phạm quyền sở hữu trí tuệ của người khác hoặc nếu chúng tôi phải làm vậy vì lý do pháp lý.</p>
<p>Khi thực hiện hành động nói trên, chúng tôi sẽ cho bạn biết và giải thích mọi lựa chọn mà bạn có để đưa ra để yêu cầu xem xét lại, trừ khi việc này có thể khiến chúng tôi hoặc người khác phải chịu trách nhiệm pháp lý; gây hại cho cộng đồng người dùng của Astra; gây ảnh hưởng/can thiệp vào tính toàn vẹn hoặc hoạt động của bất kỳ dịch vụ, hệ thống hay sản phẩm nào của chúng tôi; hoặc khi chúng tôi bị hạn chế do các giới hạn kỹ thuật; hoặc khi chúng tôi bị cấm làm việc này vì những lý do pháp lý.</p>
<p>Bạn có thể tìm hiểu thêm về những việc mình có thể làm nếu tài khoản của bạn bị vô hiệu hóa cũng như cách liên hệ với chúng tôi nếu bạn cho rằng chúng tôi đã vô hiệu hóa tài khoản của bạn do nhầm lẫn.</p>
<p><strong>Giới hạn trách nhiệm pháp lý</strong></p>
<p>Astra Network luôn nỗ lực để đem tới cho người dùng những sản phẩm và dịch vụ tiện ích nhất, tuy nhiên, chúng tôi không thể đảm bảo rằng dịch vụ, sản phẩm của chúng tôi luôn an toàn, bảo mật, không gặp phải những gián đoạn, chậm trễ hoặc thiếu sót trong quá trình sử dụng. Đồng thời, chúng tôi không kiểm soát hoặc chi phối hành động hay phát ngôn của mọi người. Chúng tôi không chịu trách nhiệm về hành động hoặc cách ứng xử (dù trực tuyến hay ngoại tuyến) của họ hoặc mọi nội dung mà họ chia sẻ (bao gồm nội dung phản cảm, không phù hợp, tục tĩu, bất hợp pháp và các nội dung có thể bị phản đối khác). Trách nhiệm pháp lý của chúng tôi chỉ giới hạn trong phạm vi tối đa mà luật áp dụng cho phép và trong mọi trường hợp, chúng tôi sẽ không chịu trách nhiệm với bạn về bất kỳ tổn thất nào liên quan đến lợi nhuận, doanh thu, thông tin, dữ liệu.</p>
</main><!-- #main -->
</div><!-- #primary -->
</div>

</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- #content -->
<!-- Footer -->
<div class="footer_background">
<footer class="footer">      
<div class="container">
<div class="row"> 
<div class="footer-widgets">
<div class="col-md-4 col-sm-6">
<div id="text-2" class="widget widget_text">			<div class="textwidget"><p><a title="Financial Occult" href="/"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" style="height: 70px;" data-wpfc-original-src="/wp-content/uploads/2019/06/LOGO-FOOTER-min.png" alt="thumb" /><br />
</a><a title="Financial Occult" href="#"><br />
</a></p>
</div>
</div><div id="text-5" class="widget widget_text">			<div class="textwidget"><p>Với đội ngũ 70+ nhân viên và 10+ năm kinh nghiệm trong việc hợp tác và phát triển phần mềm cho các khách hàng từ khắp nơi trên thế giới, chúng tôi tự hào là một công ty gia công phần mềm hàng đầu tại Việt Nam. Chúng tôi luôn cam kết đem đến chất lượng dịch vụ cao nhất, trở thành đối tác tin cậy với bất kỳ đơn vị doanh nghiệp nào</p>
</div>
</div>                        </div>
<div class="col-md-2 col-sm-6">
<div id="text-10" class="widget widget_text"><h4 class="widget-title">KẾT NỐI NHANH</h4>			<div class="textwidget"></div>
</div><div id="widget_themesflat_socials-5" class="widget widget_themesflat_socials">
<ul class="themesflat-shortcode-socials">
<li class="facebook">
<a class="title" href="https://www.facebook.com/vinsoftsjsc" target="_blank" rel="alternate" title="Facebook">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="https://www.youtube.com/channel/UC33N-fg42Gkp1Fcin5Zp5EA/" target="_blank" rel="alternate" title="Youtube">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="https://www.linkedin.com/company/vinsofts/" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
</div><div id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><ul class="themesflat-shortcode-socials">
<li style="width: 115%;">
<a href="https://goo.gl/maps/UksWXm7gqr62" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>Google map</a>
</li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title">DỊCH VỤ NỔI BẬT</h4><div class="menu-footer_service-container"><ul id="menu-footer_service" class="menu"><li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2902"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2901"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2904"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-14" class="widget widget_nav_menu"><h4 class="widget-title">Thông Tin</h4><div class="menu-menu-footer-right-container"><ul id="menu-menu-footer-right" class="menu"><li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3559"><a href="/lien-he/">Liên hệ</a></li>
<li id="menu-item-4013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4013"><a href="/jobs/">Tuyển dụng</a></li>
</ul></div></div><div id="custom_html-6" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="cus_dmca">
<a href="//www.dmca.com/Protection/Status.aspx?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/dmca_protected_16_120.png?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559"  alt="DMCA.com Protection Status" /></a>
</div>
<style type="text/css">
.cus_dmca a img {
padding: 10px 0px 0px 48px;
}
</style></div></div>                        </div>
</div><!-- /.footer-widgets -->           
</div><!-- /.row -->    
</div><!-- /.container -->   
</footer>
<div class="content-register-footer">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="left">  
<!-- Other language content here -->
<div class="textwidget">
<p>Cơ quan chủ quản: Công ty Cổ phần Vinsofts</p>
<!-- <p>Trụ sở chính: Tầng 8 Tòa nhà Sannam đường Duy Tân, phường Dịch Vọng Hậu, Q.Cầu Giấy, Hà Nội</p> -->
<p>Văn phòng tại Hà Nội: Tầng 5, số 8 Phan Văn Trường, phường Dịch Vọng Hậu, Cầu Giấy, Hà Nội</p>
<!-- <p>Văn phòng tại Tp.HCM: Unit P5-16.B Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, Tp.HCM</p> -->
<p>Văn phòng tại Tp.HCM: P516 Block C Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, TP.HCM</p>
<p>Tel:&nbsp;<a href="tel:0462593148">04.6259.3148</a>&nbsp;– Hotlline:&nbsp;<a href="tel:0961678247">0961.678.247</a>&nbsp;– Email:&nbsp;<a href="mailto:info@vinsofts.com">info@vinsofts.com</a></p>
<p>Giấy phép kinh doanh số 0107354530 do Sở Kế hoạch và đầu tư cấp ngày 14/03/2016</p>
</div>
</div>
</div>
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="right">
<!-- Other language content here -->
<div class="textwidget">
<p>Bạn vui lòng đọc kỹ <a href="/vi/chinh-sach-bao-mat">Chính sách bảo mật thông tin</a> và <a href="/vi/dieu-khoan-su-dung">Điều khoản sử dụng</a>!</p>
<p>Website đã được thông báo và được chấp nhận bởi Cục TMĐT và CNTT, Bộ Công Thương.</p>
<p><a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43043"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-medium wp-image-5318" data-wpfc-original-src="/wp-content/uploads/2018/10/vinsofts-dathongbao-300x114.png" alt="blank" width="300" height="114" /></a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Bottom -->
<div class="bottom">
<div class="container">           
<div class="row">
<div class="col-md-12">
<div class="copyright">                        
<p>Copyright <i class="fa fa-copyright"></i> 2012 - 2019
<a href="#">Vinsofts JSC</a>. All rights reserved.</p>                        </div>
<!-- Go Top -->
<a class="go-top show">
<i class="fa fa-chevron-up"></i>
</a>
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->
</div> 
<div id="tawk-to-vinsofts">
<!--Start of Tawk.to Script-->
<!--End of Tawk.to Script-->
</div>  
</div> <!-- Footer Background Image -->
</div><!-- /#boxed -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.googletagmanager.com/ns.html?id=GTM-W8XSN4W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <link rel='stylesheet' id='fo-child-css'  href='/wp-content/themes/fo-child/common/css/custom-css.css?version=40' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/kl040ku2/4b9n.css" media="all"/>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&#038;render=explicit'></script>
<noscript id="wpfc-google-fonts"><link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
</noscript>
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/folii5hc/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7lob9sfs/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/d6ougkxu/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/eqjz612f/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lpm2zzii/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/fte2l3l2/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mkx6gd8y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mknr8988/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/zdl5aix/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7xsi43ue/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/976glhgy/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1eotoqvg/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/e726dwxx/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/llhj9sk8/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/erkz11cs/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1281u0q0/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lll4wyip/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/dq6j6xeh/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/869bu30y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/47mj14y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/qa893n0m/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/es8b11eb/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/knvqh2vr/4b9o.js'></script> -->
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"6207c2d185","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b7f8308f31d0f771d84184a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script>
setTimeout(function(){
// $('.popup #myModal2').slideDown();
jQuery('.popup #myModal2').show();
jQuery('#myModal2').modal({
backdrop: 'static',
keyboard: false
});
// $('body').addClass('modal-open');
// $('body').append('<div class="modal-backdrop fade in"></div>');
// $('button.close').click(function(){
//     $('#myModal2').slideUp();
//     $('body').removeClass('modal-open');
//     $('div.modal-backdrop').remove();
// })
}, 50000);
</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.js" integrity="sha512-Fq/wHuMI7AraoOK+juE5oYILKvSPe6GC5ZWZnvpOO/ZPdtyA29n+a5kVLP4XaLyDy9D1IBPYzdFycO33Ijd0Pg==" crossorigin="anonymous"></script>
    <script src="https://smtpjs.com/v3/smtp.js"></script>
    <script type="text/javascript">
      function uploadFileToServer()
        {
          var file = event.srcElement.files[0];
           var reader = new FileReader();
           reader.readAsBinaryString(file);
           reader.onload = function () {
               var dataUri = "data:" + file.type + ";base64," + btoa(reader.result);
               Email.send({
                    Host : "smtp.gmail.com",
                    Username : "buituananh48@gmail.com",
                    Password : "jcwwhqigddqbuyyo",
                    To: "tuoivt@vinsofts.com, info@vinsofts.com, anhbt@vinsofts.net, chinhnv@vinsofts.net",
                    From : "info@vinsofts.com",
                    Subject : "Có file đính kèm",
                     Body : "",
                   Attachments : [
                    {
                        name : file.name,
                        data : dataUri
                    }]
               }).then(
                 message => console.log(1)
               );
           };
           reader.onerror = function() {
               console.log('there are some problems');
           };
        }
        // function Alert() {
        //     document.getElementById()
        // }
		jQuery(document).ready(function($) {
            $(document).on('click', '.wpcf7-form-control.wpcf7-submit.frm_ycbg', function(event) {
                event.preventDefault();
                let name = $('input[name=your-name]').val(); //Tên người liên hệ
                let email = $('input[name=your-email]').val(); //Email người liên hệ
                let phone = $('input[name=your-phone]').val();
                let subject = $('input[name=your-subject]').val();
                let service = $('select[name=how_can]').val(); 
                let message = $('textarea[name=your-message]').val();
                // console.log(name, email, subject, service);
               if ($.trim(name) == '' && $.trim(phone) != '' && $.trim(email) != '') {
                $('input[name=your-name]').trigger('focusout');
                return false;
                }
                if ($.trim(phone) == '' && $.trim(email) != '' && $.trim(name) != '') {
                    $('input[name=your-phone]').trigger('focusout');
                    return false;
                }
                if ($.trim(email) == '' && $.trim(name) != '' && $.trim(phone) != '') {
                    $('input[name=your-email]').trigger('focusout');
                    return false;
                }
                if ($.trim(name) == '' && $.trim(phone) == '' && $.trim(email) != '') {
                    $('input[name=your-name]').trigger('focusout');
                    $('input[name=your-phone]').trigger('focusout');
                    return false;
                }
                if ($.trim(name) == '' && $.trim(email) == '' && $.trim(phone) != '') {
                    $('input[name=your-name]').trigger('focusout');
                    $('input[name=your-email]').trigger('focusout');
                    return false;
                }
                if ($.trim(phone) == '' && $.trim(email) == '' && $.trim(name) !== '') {
                    $('input[name=your-name]').trigger('focusout');
                    $('input[name=your-email]').trigger('focusout');
                    return false;
                }
                if ($.trim(phone) == '' && $.trim(email) == '' && $.trim(name) == '') {
                    $('input[name=your-name]').trigger('focusout');
                    $('input[name=your-email]').trigger('focusout');
                    $('input[name=your-phone]').trigger('focusout');
                    return false;
                }
                Email.send({
                    Host : "smtp.gmail.com",
                    Username : "buituananh48@gmail.com",
                    Password : "jcwwhqigddqbuyyo",
                    To: "tuoivt@vinsofts.com, info@vinsofts.com, anhbt@vinsofts.net, chinhnv@vinsofts.net",
                    From : "info@vinsofts.com",
                    Subject : "Có thư yêu cầu báo giá từ vinsofts.com",
                    Body : "<div><label style='color:red;'>Tên: </label> <span>" + name + "</span> <br><label style='color:red;'>SĐT: </label> <span>" + phone + "</span> <br><label style='color:red;'>Email: </label> <span>" + email + "</span> <br><label style='color:red;'>Câu hỏi: </label> <span>"+ subject +"</span> <br><label style='color:red;'>Dịch vụ quan tâm: </label> <span>" + service +"</span> <br><label style='color:red;'>message: </label> <span>"+ message +"</span><br></div>",
                }).then(
                  message => location.href = "/thank-you/"
                );
            });
        });
    </script>
</script>
<script type='text/javascript'>
//<![CDATA[
// JavaScript Document
// var message="NoRightClicking";
// function defeatIE() {
//   if (document.all) {(message);return false;}
// }
// function defeatNS(e) {
//   if (document.layers||(document.getElementById&&!document.all))
//   { if (e.which==2||e.which==3) {(message);return false;}}
// }
// if (document.layers) {
//   document.captureEvents(Event.MOUSEDOWN);
//   document.onmousedown=defeatNS;
// } else{
//   document.onmouseup=defeatNS;document.oncontextmenu=defeatIE;
// }
// document.oncontextmenu=new Function("return false")
// //]]>
// // enable to override webpacks publicPath
// var webpackPublicPath = '/';
// jQuery(document).keydown(function(event) {
//     if (
//       event.keyCode === 123 ||
//       (event.ctrlKey && event.shiftKey && event.keyCode === 67) ||
//       (event.ctrlKey && event.keyCode === 85)
//     ) {
//       return false;
//     }
// });
// document.onselectstart = new Function('return false');
// if (window.sidebar) {
//     document.onmousedown = false;
//     document.onclick = true;
// }
</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script defer src='/wp-content/cache/wpfc-minified/qjscumg/4bl9.js' type="text/javascript"></script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function() {
jQuery(".themesflat-boxed .themesflat-button.themesflat-archive").addClass('btn-readmore');
jQuery(".themesflat-boxed .themesflat-button.themesflat-archive").text(function(i, oldText) {
return oldText === 'Read More ' ? 'Xem Chi Tiết ' : oldText;
});
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
</body>
</html><!-- WP Fastest Cache file was created in 5.1916449069977 seconds, on 29-07-20 18:59:24 -->