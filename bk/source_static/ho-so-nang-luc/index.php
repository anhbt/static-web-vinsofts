<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=https://drive.google.com/file/d/19cJ2sOH-iN5F0Yr1QF2-L3Mw7Hm811hL/view">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "https://drive.google.com/file/d/19cJ2sOH-iN5F0Yr1QF2-L3Mw7Hm811hL/view";
		</script>

		<p>You are being redirected to <a href="https://drive.google.com/file/d/19cJ2sOH-iN5F0Yr1QF2-L3Mw7Hm811hL/view">https://drive.google.com/file/d/19cJ2sOH-iN5F0Yr1QF2-L3Mw7Hm811hL/view</a></p>
	</body>
</html>
