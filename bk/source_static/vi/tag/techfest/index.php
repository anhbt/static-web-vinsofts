<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/tag/techfest/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/tag/techfest/";
		</script>

		<p>You are being redirected to <a href="/tag/techfest/">/tag/techfest/</a></p>
	</body>
</html>
