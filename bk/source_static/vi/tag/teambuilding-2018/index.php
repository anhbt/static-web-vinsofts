<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/tag/teambuilding-2018/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/tag/teambuilding-2018/";
		</script>

		<p>You are being redirected to <a href="/tag/teambuilding-2018/">/tag/teambuilding-2018/</a></p>
	</body>
</html>
