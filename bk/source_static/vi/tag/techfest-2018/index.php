<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/tag/techfest-2018/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/tag/techfest-2018/";
		</script>

		<p>You are being redirected to <a href="/tag/techfest-2018/">/tag/techfest-2018/</a></p>
	</body>
</html>
