<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/jobs/page/3/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/jobs/page/3/";
		</script>

		<p>You are being redirected to <a href="/jobs/page/3/">/jobs/page/3/</a></p>
	</body>
</html>
