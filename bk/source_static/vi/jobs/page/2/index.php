<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/jobs/page/2/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/jobs/page/2/";
		</script>

		<p>You are being redirected to <a href="/jobs/page/2/">/jobs/page/2/</a></p>
	</body>
</html>
