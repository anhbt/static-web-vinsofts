<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/thiet-ke-app/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/thiet-ke-app/";
		</script>

		<p>You are being redirected to <a href="/thiet-ke-app/">/thiet-ke-app/</a></p>
	</body>
</html>
