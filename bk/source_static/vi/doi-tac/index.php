<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/doi-tac/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/doi-tac/";
		</script>

		<p>You are being redirected to <a href="/doi-tac/">/doi-tac/</a></p>
	</body>
</html>
