<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/booking-apps-take-over-several-industries-by-surprise/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/booking-apps-take-over-several-industries-by-surprise/";
		</script>

		<p>You are being redirected to <a href="/booking-apps-take-over-several-industries-by-surprise/">/booking-apps-take-over-several-industries-by-surprise/</a></p>
	</body>
</html>
