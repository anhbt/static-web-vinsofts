<!DOCTYPE html>
<html>
	<head>
		<title>Redirecting...</title>
		<meta http-equiv="refresh" content="0;url=/by-solutions/">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "/by-solutions/";
		</script>

		<p>You are being redirected to <a href="/by-solutions/">/by-solutions/</a></p>
	</body>
</html>
