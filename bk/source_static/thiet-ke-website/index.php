<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Thiết kế website - Công ty phần mềm Vinsofts</title>
<!-- <link rel="stylesheet" href="/wp-content/themes/fo-child/common/css/custom-landing-page.css?rand=316" type="text/css" media="screen" /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/87sdc2zn/4bvo.css" media="screen"/>
<link rel="pingback" href="/xmlrpc.php">
<title>Thiết kế website - Công ty phần mềm Vinsofts</title>
<!-- This site is optimized with the Yoast SEO Premium plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
<link rel="canonical" href="/thiet-ke-website/" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Thiết kế website - Công ty phần mềm Vinsofts" />
<meta property="og:url" content="/thiet-ke-website/" />
<meta property="og:site_name" content="Công ty phần mềm Vinsofts" />
<meta property="article:publisher" content="https://www.facebook.com/vinsoftsjsc" />
<meta property="fb:app_id" content="1096476827181655" />
<meta property="og:image" content="/wp-content/uploads/2018/11/web-1.jpg" />
<meta property="og:image:secure_url" content="/wp-content/uploads/2018/11/web-1.jpg" />
<meta property="og:image:width" content="1920" />
<meta property="og:image:height" content="869" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Thiết kế website - Công ty phần mềm Vinsofts" />
<meta name="twitter:image" content="/wp-content/uploads/2018/11/web-1.jpg" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"\/","name":"C\u00f4ng ty ph\u1ea7n m\u1ec1m Vinsofts","potentialAction":{"@type":"SearchAction","target":"\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO Premium plugin. -->
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Công ty phần mềm Vinsofts &raquo;" href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Công ty phần mềm Vinsofts &raquo;" href="/comments/feed/" />
<!-- <link rel='stylesheet' id='vc_extend_shortcode-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes.css?version=1335' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_extend_style-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes-3rd.css?version=4277' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='bootstrap-css'  href='/wp-content/themes/fo/css/bootstrap.css?version=6713' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?version=4521' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qjpyrk45/4b9o.css" media="all"/>
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<!-- <link rel='stylesheet' id='qts_front_styles-css'  href='/wp-content/plugins/qtranslate-slug/assets/css/qts-default.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_main-css'  href='/wp-content/themes/fo/css/main.css?version=8361' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-style-css'  href='/wp-content/themes/fo-child/style.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='font-fontawesome-css'  href='/wp-content/themes/fo/css/font-awesome.css?version=4255' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-ionicons-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=5998' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_simpleline-css-css'  href='/wp-content/themes/fo/css/simple-line-icons.css?version=7683' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_ion_icon-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=2296' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/9jminkn5/4b9o.css" media="all"/>
<!--[if lte IE 9]><link rel='stylesheet' id='ie9-css'  href='/wp-content/themes/fo/css/ie.css?version=4483' type='text/css' media='all' /><![endif]-->
<!-- <link rel='stylesheet' id='themesflat_logo-css'  href='/wp-content/themes/fo/css/logo.css?version=6696' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_animate-css'  href='/wp-content/themes/fo/css/animate.css?version=3161' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_responsive-css'  href='/wp-content/themes/fo/css/responsive.css?version=3077' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-inline-css-css'  href='/wp-content/themes/fo/css/inline-css.css?version=8030' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/mmoo7qcx/4b9o.css" media="all"/>
<style id='themesflat-inline-css-inline-css' type='text/css'>
.logo{padding-top:24px; padding-left:15px; }
.footer{padding-top:64px; padding-bottom:74px; }
.page-title{padding-top:21px; }
.logo img, .logo svg { height:70px; }
.page-title .overlay{ background: #ffffff}.page-title {background: url() center /cover no-repeat;}.page-title h1 {color:#21536c!important;
}
.breadcrumbs span,.breadcrumbs span a, .breadcrumbs a {color:#595959!important;
}
body,button,input,select,textarea { font-family:Poppins ; }
body,button,input,select,textarea { font-weight:400;}
body,button,input,select,textarea { font-style:normal; }
body,button,input,select,textarea { font-size:14px; }
body,button,input,select,textarea { line-height:25px ; }
h1,h2,h3,h5,h6 { font-family:Poppins;}
h1,h2,h3,h4,h5,h6 { font-weight:600;}
h1,h2,h3,h4,h5,h6  { font-style:normal; }
#mainnav > ul > li > a { font-family:Poppins;}
#mainnav > ul > li > a { font-weight:600;}
#mainnav > ul > li > a  { font-style:normal; }
#mainnav ul li a { font-size:14px;}
#mainnav > ul > li > a { line_height100px;}
h1 { font-size:32px; }
h2 { font-size:25px; }
h3 { font-size:22px; }
h4 { font-size:18px; }
h5 { font-size:15px; }
h6 { font-size:14px; }
.iconbox .box-header .box-icon span,a:hover, a:focus,.portfolio-filter li a:hover, .portfolio-filter li.active a,.themesflat-portfolio .item .category-post a,.color_theme,.widget ul li a:hover,.footer-widgets ul li a:hover,.footer a:hover,.themesflat-top a:hover,.themesflat-portfolio .portfolio-container.grid2 .title-post a:hover,.themesflat-button.no-background, .themesflat-button.blog-list-small,.show-search a i:hover,.widget.widget_categories ul li a:hover,.breadcrumbs span a:hover, .breadcrumbs a:hover,.comment-list-wrap .comment-reply-link,.portfolio-single .content-portfolio-detail h3,.portfolio-single .content-portfolio-detail ul li:before, .themesflat-list-star li:before, .themesflat-list li:before,.navigation.posts-navigation .nav-links li a .meta-nav,.testimonial-sliders.style3 .author-name a,ul.iconlist .list-title a:hover,.themesflat_iconbox .iconbox-icon .icon span,.bottom .copyright a,.top_bar2 .wrap-header-content ul li i { color:#337493;}
#Ellipse_7 circle,.testimonial-sliders .logo_svg path { fill:#337493;}
.info-top-right a.appoinment, .wrap-header-content a.appoinment,button, input[type=button], input[type=reset], input[type=submit],.go-top:hover,.portfolio-filter.filter-2 li a:hover, .portfolio-filter.filter-2 li.active a,.themesflat-socials li a:hover, .entry-footer .social-share-article ul li a:hover,.themesflat-button,.featured-post.blog-slider .flex-prev, .featured-post.blog-slider .flex-next,mark, ins,#themesflat-portfolio-carousel ul.flex-direction-nav li a, .flex-direction-nav li a,.navigation.posts-navigation .nav-links li a:after,.title_related_portfolio:after,.navigation.loadmore a:hover,.owl-theme .owl-controls .owl-nav [class*=owl-],.widget.widget_tag_cloud .tagcloud a,.btn-menu:before, .btn-menu:after, .btn-menu span,.themesflat_counter.style2 .themesflat_counter-icon .icon,widget a.appoinment,.themesflat_imagebox .imagebox-image:after,.nav-widget a.appoinment { background-color:#337493; }
.themesflat_btnslider:not(:hover) {
background-color:#337493!important;
}
.loading-effect-2 > span, .loading-effect-2 > span:before, .loading-effect-2 > span:after,textarea:focus, input[type=text]:focus, input[type=password]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=date]:focus, input[type=month]:focus, input[type=time]:focus, input[type=week]:focus, input[type=number]:focus, input[type=email]:focus, input[type=url]:focus, input[type=search]:focus, input[type=tel]:focus, input[type=color]:focus,select:focus,.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span,.navigation.loadmore a:hover { border-color:#337493}
{
border-color:#337493!important;
}
{
color: #fff !important;
}
{
background-color: #2e363a !important;
}
#Financial_Occult text,#F__x26__O tspan { fill:#595959;}
body { color:#595959}
a,.portfolio-filter li a,.themesflat-portfolio .item .category-post a:hover,.title-section .title,ul.iconlist .list-title a,.breadcrumbs span a, .breadcrumbs a,.breadcrumbs span,h1, h2, h3, h4, h5, h6,strong,.testimonial-content blockquote,.testimonial-content .author-info,.sidebar .widget ul li a,.themesflat_counter.style2 .themesflat_counter-content-right,.themesflat_counter.style2 .themesflat_counter-content-left,.title_related_portfolio,.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation .current, .page-links a:hover, .page-links a:focus,.widget_search .search-form input[type=search],.entry-meta ul,.entry-meta ul.meta-right,.entry-footer strong,.widget.widget_archive ul li:before, .widget.widget_categories ul li:before, .widget.widget_recent_entries ul li:before { color:#595959}
.owl-theme .owl-dots .owl-dot span,.widget .widget-title:after, .widget .widget-title:before,ul.iconlist li.circle:before { background-color:#595959}
.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation:not(.loadmore) .current, .page-links a:hover, .page-links a:focus, .page-links > span { border-color:#595959}
.themesflat-top { background-color:#21536c ; } 
.themesflat-top .border-left:before, .themesflat-widget-languages:before, .themesflat-top .border-right:after { background-color: rgba(255,255,255,0.2);}.themesflat-top,.info-top-right,.themesflat-top a, .themesflat-top .themesflat-socials li a { color:#ffffff ;} 
.themesflat_header_wrap.header-style1,.nav.header-style2,.themesflat_header_wrap.header-style3,.nav.header-style4,.header.widget-header .nav { background-color:#fff;}
#mainnav > ul > li > a { color:#595959;}
#mainnav > ul > li > a:hover,#mainnav > ul > li.current-menu-item > a { color:#377493 !important;}
#mainnav ul.sub-menu > li > a { color:#ffffff!important;}
#mainnav ul.sub-menu { background-color:#377493;}
#mainnav ul.sub-menu > li > a:hover { background-color:#aac9ce!important;}
#mainnav ul.sub-menu > li { border-color:#dde9eb!important;}
.footer_background:before { background-color:#21536c;}
.footer a, .footer, .themesflat-before-footer .custom-info > div,.footer-widgets ul li a,.footer-widgets .company-description p { color:#f9f9f9;}
.bottom { background-color:#337493;}
.bottom .copyright p,.bottom #menu-bottom li a { color:#e5e5e5;}
.white #Financial_Occult text,.white #F__x26__O tspan {
fill: #fff; }test_filter_render
</style>
<!-- <link rel='stylesheet' id='js_composer_front-css'  href='/wp-content/plugins/js_composer/assets/css/js_composer.min.css?version=3747' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qhynyhyt/4b9n.css" media="all"/>
<link rel='https://api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.8" />
<link rel='shortlink' href='/?p=5574' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fthiet-ke-website%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fthiet-ke-website%2F&#038;format=xml" />
<meta name="generator" content="qTranslate-X 3.4.6.8" />
<link hreflang="x-default" href="/thiet-ke-website/" rel="alternate" />
<link hreflang="vi" href="/thiet-ke-website/" rel="alternate" />
<link hreflang="en" href="/en/landing-page-2/" rel="alternate" />
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?version=5812" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.6.3.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/10/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="/wp-content/uploads/2018/10/cropped-favicon-270x270.png" />
<style type="text/css" id="wp-custom-css">
.page-wrap.sidebar-left #primary.content-area {
/*     margin-top: -40px; */
}
.single-post .page-wrap.sidebar-left #primary.content-area {
margin-top: 0;
}
.themesflat-portfolio .item .featured-post {
text-align: center;
}
.themesflat-portfolio .item .featured-post img, .content-area.fullwidth .themesflat_iconbox.inline-left img {
height: 220px;
width: 100%;
object-fit: cover;
}
/* .content-area.fullwidth .themesflat_iconbox.transparent .title {
height: 55px;
display: flex;
align-items: center;
margin-top: 0;
}
.content-area.fullwidth .themesflat_iconbox.transparent .iconbox-content p:nth-child(2) {
height: 125px;
overflow: hidden;
}
*/
.header.header-sticky .wpmenucartli {
display:none !important;
}
.header.header-sticky #menu-main li:first-child {
display: inline-block !important;
}
#mainnav>ul>li {
margin-left: 25px !important;
}		</style>
<style type="text/css" data-type="vc_custom-css">#section2 #user-col .item-user .vc_column-inner>.wpb_wrapper .vc_single_image-wrapper img {
/*border-radius: 50%;*/
width: 100%; 
border: 1px solid #3c9be9;
}
.khung-anh .wpb_single_image img {
border: 1px solid #3c9be9;
border-radius: 50%;
width: 65px;
height: 65px;
}</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1541670040635{background-image: url(/wp-content/uploads/2018/11/web-1.jpg?id=5943) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1541670010790{background-position: 0 0 !important;background-repeat: no-repeat !important;}.vc_custom_1541153631177{margin-bottom: 0px !important;}.vc_custom_1541153639370{margin-bottom: 0px !important;}.vc_custom_1541153646756{margin-bottom: 0px !important;}.vc_custom_1541153653558{margin-bottom: 0px !important;}.vc_custom_1541144513606{margin-top: 0px !important;}.vc_custom_1540982363965{margin-bottom: 20px !important;}.vc_custom_1541581367777{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541662875814{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541662976677{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541665981134{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541581817375{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541663003798{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541663046045{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541665998210{padding-right: 5px !important;padding-left: 5px !important;}.vc_custom_1541153631177{margin-bottom: 0px !important;}.vc_custom_1541153639370{margin-bottom: 0px !important;}.vc_custom_1541153646756{margin-bottom: 0px !important;}.vc_custom_1541153653558{margin-bottom: 0px !important;}.vc_custom_1541144513606{margin-top: 0px !important;}.vc_custom_1540982363965{margin-bottom: 20px !important;}.vc_custom_1541499306625{margin-top: -30px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<script data-wpfc-render="false">var Wpfcll={s:[],osl:0,scroll:false,i:function(){Wpfcll.ss();window.addEventListener('load',function(){window.addEventListener("DOMSubtreeModified",function(e){Wpfcll.osl=Wpfcll.s.length;Wpfcll.ss();if(Wpfcll.s.length > Wpfcll.osl){Wpfcll.ls(false);}},false);Wpfcll.ls(true);});window.addEventListener('scroll',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('resize',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('click',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});},c:function(e,pageload){var w=document.documentElement.clientHeight || body.clientHeight;var n=0;if(pageload){n=0;}else{n=(w > 800) ? 800:200;n=Wpfcll.scroll ? 800:n;}var er=e.getBoundingClientRect();var t=0;var p=e.parentNode;if(typeof p.getBoundingClientRect=="undefined"){var pr=false;}else{var pr=p.getBoundingClientRect();}if(er.x==0 && er.y==0){for(var i=0;i < 10;i++){if(p){if(pr.x==0 && pr.y==0){p=p.parentNode;if(typeof p.getBoundingClientRect=="undefined"){pr=false;}else{pr=p.getBoundingClientRect();}}else{t=pr.top;break;}}};}else{t=er.top;}if(w - t+n > 0){return true;}return false;},r:function(e,pageload){var s=this;var oc,ot;try{oc=e.getAttribute("data-wpfc-original-src");ot=e.getAttribute("data-wpfc-original-srcset");if(s.c(e,pageload)){if(oc || ot){if(e.tagName=="DIV" || e.tagName=="A"){e.style.backgroundImage="url("+oc+")";e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");}else{if(oc){e.setAttribute('src',oc);}if(ot){e.setAttribute('srcset',ot);}e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");if(e.tagName=="IFRAME"){e.onload=function(){if(typeof window.jQuery !="undefined"){if(jQuery.fn.fitVids){jQuery(e).parent().fitVids({customSelector:"iframe[src]"});}}var s=e.getAttribute("src").match(/templates\/youtube\.html\#(.+)/);var y="https://www.youtube.com/embed/";if(s){try{var i=e.contentDocument || e.contentWindow;if(i.location.href=="about:blank"){e.setAttribute('src',y+s[1]);}}catch(err){e.setAttribute('src',y+s[1]);}}}}}}else{if(e.tagName=="NOSCRIPT"){if(jQuery(e).attr("data-type")=="wpfc"){e.removeAttribute("data-type");jQuery(e).after(jQuery(e).text());}}}}}catch(error){console.log(error);console.log("==>",e);}},ss:function(){var i=Array.prototype.slice.call(document.getElementsByTagName("img"));var f=Array.prototype.slice.call(document.getElementsByTagName("iframe"));var d=Array.prototype.slice.call(document.getElementsByTagName("div"));var a=Array.prototype.slice.call(document.getElementsByTagName("a"));var n=Array.prototype.slice.call(document.getElementsByTagName("noscript"));this.s=i.concat(f).concat(d).concat(a).concat(n);},ls:function(pageload){var s=this;[].forEach.call(s.s,function(e,index){s.r(e,pageload);});}};document.addEventListener('DOMContentLoaded',function(){wpfci();});function wpfci(){Wpfcll.i();}</script>
</head>
<body>
<style>
.header {
margin-bottom: 0;
}
.full-content-landing #section1{
background-image: url(/wp-content/uploads/2018/11/web-1.jpg);
background: /wp-content/uploads/2018/11/web-1.jpg;
}
.full-content-landing #section2{
background-image: none;
background: none;
}
.full-content-landing #section3{
background-image: none;
background: none;
}
.full-content-landing #section4{
background-image: none;
background: none;
}
.full-content-landing #section5{
background-image: none;
background: none;
}
.full-content-landing #section6{
background-image: none;
background: none;
}
.full-content-landing #section7{
background-image: none;
background: none;
}
.full-content-landing #section8{
background-image: none;
background: none;
}
.full-content-landing #section9{
background-image: none;
background: none;
}
.full-content-landing #section10{
background-image: none;
background: none;
}
</style>
<div class="themesflat-boxed">
<!-- Preloader -->
<div class="preloader">
<div class="clear-loading loading-effect-2">
<span></span>
</div>
</div>
<!-- Top -->
<div class="themesflat-top header-style1">    
<div class="container">
<div class="container-inside">
<div class="content-left">
<ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-chooser">
<li class="lang-vi active"><a href="/vi/thiet-ke-website/" hreflang="vi" title="VI (vi)" class="qtranxs_image qtranxs_image_vi"><img src="/wp-content/plugins/qtranslate-x/flags/vn.png" alt="VI (vi)" /><span style="display:none">VI</span></a></li>
<li class="lang-en"><a href="/en/landing-page-2/" hreflang="en" title="EN (en)" class="qtranxs_image qtranxs_image_en"><img src="/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN (en)" /><span style="display:none">EN</span></a></li>
</ul><div class="qtranxs_widget_end"></div>
<ul>
<li class="border-right">
<i class="fa fa-phone"></i><a href="tel:0247 1080 285" target="_top"> (+84) 0247 1080 285</a>     
</li>
<li>
<i class="fa fa-envelope"></i> <a href="mailto:info@vinsofts.com" target="_top">  info@vinsofts.com</a> 
</li>
</ul>	
</div><!-- /.col-md-7 -->
<div class="content-right">
<ul class="themesflat-socials">
<li class="facebook">
<a class="title" href="">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="/thiet-ke-website/linkedin.com/company/vinsofts" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
<div class="info-top-right border-left">
<span><i class="fa fa-question-circle"></i>Bạn cần tư vấn?</span>
<a class="appoinment" href="#">Yêu cầu báo giá</a>
</div>            </div><!-- /.col-md-5 -->
</div><!-- /.container -->
</div><!-- /.container -->        
</div><!-- /.top -->
<div class="popup">
<!--Form contact-->
<div id="myModal2" class="modal fade in" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content form contact-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<div role="form" class="wpcf7" id="wpcf7-f3777-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/thiet-ke-website/#wpcf7-f3777-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3777" />
<input type="hidden" name="_wpcf7_version" value="5.0.5" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3777-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<div class="row gutter-10 contactform4">
<h4 class="title-form" style="display:none">Quý khách vui lòng điền form bên dưới và gửi cho chúng tôi. Chúng tôi sẽ liên hệ lại ngay.</h4>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ tên *" /></span></div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *" /></span> </div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Câu hỏi" /></span> </div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can"><select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false"><option value="Bạn quan tâm đến dịch vụ nào?">Bạn quan tâm đến dịch vụ nào?</option><option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option><option value="Phát triển ứng dụng web, thiết kế website">Phát triển ứng dụng web, thiết kế website</option><option value="Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin">Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin</option><option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option><option value="Mua Phần mềm quản trị doanh nghiệp (ERP)">Mua Phần mềm quản trị doanh nghiệp (ERP)</option><option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option><option value="Thuê nhân sự cho dự án">Thuê nhân sự cho dự án</option></select></span></div>
<div class="col-sm-6">
<span>Đính kèm file:</span><br />
<span class="wpcf7-form-control-wrap file-978"><input type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" /></span>
</div>
<div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung chi tiết"></textarea></span> </div>
<div class="col-sm-10 col-xs-12 captcha-form-vins">
<div class="wpcf7-form-control-wrap"><div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
<noscript>
<div style="width: 302px; height: 422px;">
<div style="width: 302px; height: 422px; position: relative;">
<div style="width: 302px; height: 422px; position: absolute;">
<iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
</iframe>
</div>
<div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
</textarea>
</div>
</div>
</div>
</noscript>
</div>
</div>
<div class="col-sm-2 col-xs-12"><input type="submit" value="GỬI" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>          </div>
</div>
</div>
</div>
</div>
<div id="popup-team-member" class="popup-2">
<div class="modal fade" id="myModa1" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="row">
<div class="image-members-full col-md-4 col-sm-6 col-xs-12">
<div class="image">
</div>
</div>
<div class="title-modal col-md-8 col-sm-6 col-xs-12">
<div class="title">
<h3 class="modal-title"></h3>
<span class="modal-title-mini"></span>
<div class="social-content">
</div>
</div>
</div>
</div>
</div>
<div class="line-color"></div>
<div class="modal-body">
<div class="content-member">
</div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
</div>
<style type="text/css">
.themesflat-top .content-left {
float: none;
}
.themesflat-top .content-left ul{
float: left;
}
.themesflat-top .content-left ul.language-chooser{
/*margin-right: 15px;*/
}
.themesflat-top .content-left ul.language-chooser li{
padding-left: 0px;
padding-right: 10px;
}
.themesflat-top .content-left ul.language-chooser li a::after {
content: "";
border-right: 1px solid #d8d8d8;
margin-left: 6px;
}
.themesflat-top .content-left ul.language-chooser li:last-child a::after {
border: 0px;
content: "";
}
.themesflat-top .content-left ul.language-chooser li.active a,
.themesflat-top .content-left ul.language-chooser li a:hover{
color: #3d9be9;
opacity: 1;
}
.themesflat-top .content-left ul.language-chooser li a{
padding: 0px;
font-size: 13px;
color: #ffffff94;
opacity: 0.4;
}
.themesflat-top .content-left ul.language-chooser li a img{
width: 22px;
height: 14px;
}
/*.themesflat-top .content-left ul.language-chooser li:nth-child(2) a img{
height: 16px;
}*/
</style>
<style type="text/css">
.popup #myModal2{
z-index: 99999;
}
</style><div class="themesflat_header_wrap header-style1" data-header_style="header-style1">
<!-- Header -->
<header id="header" class="header header-style1" >
<div class="container nav">
<div class="row">
<div class="col-md-12">
<div class="header-wrap clearfix">
<div id="logo" class="logo" >                  
<a href="/"  title="Công ty phần mềm Vinsofts">
<img class="site-logo"  src="/wp-content/uploads/2019/06/Logo-Vinsofts.png" alt="Công ty phần mềm Vinsofts"  data-retina="/wp-content/uploads/2019/06/Logo-Vinsofts.png" />
</a>
</div>
<div class="show-search">
<a href="#"><i class="fa fa-search"></i></a>         
</div> 
<div class="nav-wrap">
<div class="btn-menu">
<span></span>
</div><!-- //mobile menu button -->
<nav id="mainnav" class="mainnav" role="navigation">
<ul id="menu-main" class="menu"><li id="menu-item-2167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2167"><a href="/ve-chung-toi/">Về chúng tôi</a>
<ul class="sub-menu">
<li id="menu-item-3792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3792"><a href="/ve-chung-toi/">Giới thiệu chung</a></li>
<li id="menu-item-3789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3789"><a href="/lich-su-cong-ty/">Lịch sử công ty</a></li>
<li id="menu-item-3790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3790"><a href="/doi-tac/">Đối tác</a></li>
<li id="menu-item-3791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3791"><a href="/nhan-su/">Nhân sự</a></li>
<li id="menu-item-6972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6972"><a href="/ho-so-nang-luc/">Hồ sơ năng lực</a></li>
<li id="menu-item-6108" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6108"><a href="/danh-muc/hoat-dong-noi-bat/">Hoạt động công ty</a></li>
</ul>
</li>
<li id="menu-item-2994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2994"><a href="/dich-vu/">Dịch vụ</a>
<ul class="sub-menu">
<li id="menu-item-2933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2933"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
<li id="menu-item-2934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2934"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
<li id="menu-item-2932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2932"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
<li id="menu-item-2935" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2935"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
<li id="menu-item-2943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2943"><a href="/kiem-thu-phan-mem-va-dich-vu-dam-bao-chat-luong/">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</a></li>
<li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="/kiem-tra-tu-dong/">Kiểm tra tự động</a></li>
<li id="menu-item-2942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2942"><a href="/trung-tam-phat-trien-phan-mem-offshore/">Trung tâm phát triển phần mềm offshore</a></li>
</ul>
</li>
<li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3202"><a href="/du-an-tieu-bieu/">Dự án tiêu biểu</a></li>
<li id="menu-item-4112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4112"><a href="/jobs">Tuyển dụng</a>
<ul class="sub-menu">
<li id="menu-item-4912" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4912"><a href="/job_cats/lap-trinh-mobile/">Lập trình Mobile</a></li>
<li id="menu-item-4911" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4911"><a href="/job_cats/lap-trinh-web/">Lập trình Web</a></li>
<li id="menu-item-4913" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4913"><a href="/job_cats/lap-trinh-blockchain/">Lập trình Blockchain</a></li>
<li id="menu-item-4810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a href="/ung-tuyen/">Ứng tuyển ngay</a></li>
</ul>
</li>
<li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="/tin-tuc/">Tin tức</a></li>
<li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="/lien-he/">Liên hệ</a></li>
</ul>        <div class="info-top-right border-left" style="display: none">
<a class="appoinment" href="#" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Yêu cầu Báo giá</a>
<a class="hotline-header" href="tel:(+84) 0247 1080 285">
<i class="fa fa-phone" aria-hidden="true"></i> <span>(+84) 0247 1080 285</span>
</a>
<i></i>
</div>
</nav><!-- #site-navigation -->  
</div><!-- /.nav-wrap -->                                
</div><!-- /.header-wrap -->
<div class="submenu top-search widget_search">
<form role="search" method="get" class="search-form" action="/">
<label>
<span class="screen-reader-text">Tìm kiếm cho:</span>
<input type="search" class="search-field" placeholder="Tìm kiếm &hellip;" value="" name="s" />
</label>
<input type="submit" class="search-submit" value="Tìm kiếm" />
</form>                </div> 
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->    
</header><!-- /.header --></div> 	</div>
<div class="full-content-landing">
<div class="header-landing-page">
<div class="menu">
<div class="menu-menu-landingpage2-container"><ul id="menu-menu-landingpage2" class="menu"><li id="menu-item-5730" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5730"><a>Form đăng ký</a></li>
<li id="menu-item-5731" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5731"><a>Giới thiệu</a></li>
<li id="menu-item-5834" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5834"><a>Giải pháp</a></li>
<li id="menu-item-5835" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5835"><a>Khách hàng</a></li>
</ul></div>			</div>
</div>
<div id="section1" class="vc_row wpb_row vc_row-fluid full-width vc_custom_1541670040635 vc_row-has-fill vc_row-o-full-height vc_row-o-columns-middle vc_row-o-equal-height vc_row-flex"><div class="row_overlay" style=""></div><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid container vc_custom_1541670010790 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-7"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft" >
<div class="wpb_wrapper">
<p><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-medium wp-image-5478" data-wpfc-original-src="/wp-content/uploads/2018/10/vinsofts-300x167.png" alt="blank" width="300" height="167" data-wpfc-original-srcset="/wp-content/uploads/2018/10/vinsofts-300x167.png 300w, /wp-content/uploads/2018/10/vinsofts-600x335.png 600w, /wp-content/uploads/2018/10/vinsofts-768x428.png 768w, /wp-content/uploads/2018/10/vinsofts.png 850w" sizes="(max-width: 300px) 100vw, 300px" /></p>
<p>&nbsp;</p>
<h2 class="slide--headline"><span style="font-size: 36pt; color: #ffffff;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Quản trị doanh nghiệp trong tầm tay\n\nKiểm soát doanh nghiệp mọi lúc, mọi nơi\n\nButton TRẢI NGHIỆM NGAY&quot;}" data-sheets-userformat="{&quot;2&quot;:707,&quot;3&quot;:&#091;null,0&#093;,&quot;4&quot;:&#091;null,2,16573901&#093;,&quot;9&quot;:1,&quot;10&quot;:1,&quot;12&quot;:0}">XÂY DỰNG WEBSITE</span></h2>
<h2 class="slide--headline"><span style="font-size: 36pt; color: #ffffff;" data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Quản trị doanh nghiệp trong tầm tay\n\nKiểm soát doanh nghiệp mọi lúc, mọi nơi\n\nButton TRẢI NGHIỆM NGAY&quot;}" data-sheets-userformat="{&quot;2&quot;:707,&quot;3&quot;:&#091;null,0&#093;,&quot;4&quot;:&#091;null,2,16573901&#093;,&quot;9&quot;:1,&quot;10&quot;:1,&quot;12&quot;:0}">THEO PHONG CÁCH RIÊNG</span></h2>
<p>&nbsp;</p>
<h6 class="slide--headline"><span style="font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #ffffff;">Giá tốt nhất thị trường &#8211; Bảo hành vĩnh viễn &#8211; Miễn phí Hosting 1 năm</span></h6>
<p>&nbsp;</p>
<div>
<p><button class="btn1-click-input" style="width: 350px;">Trải nghiệm ngay <i class="fa fa-chevron-right"></i></button></p>
</div>
</div>
</div>
</div></div></div><div class="form wpb_column vc_column_container vc_col-sm-5"><div class="vc_column-inner"><div class="wpb_wrapper"><div role="form" class="wpcf7" id="wpcf7-f5612-p5574-o2" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/thiet-ke-website/#wpcf7-f5612-p5574-o2" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="5612" />
<input type="hidden" name="_wpcf7_version" value="5.0.5" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5612-p5574-o2" />
<input type="hidden" name="_wpcf7_container_post" value="5574" />
</div>
<div class="contact-landing-page-1">
<div class="title-form">
<h2 style="font-size:24px;font-style: initial;">YÊU CẦU TƯ VẤN</h2>
<p>Vui lòng để lại thông tin ở form bên dưới. Đội ngũ tư vấn từ Vinsofts sẽ liên hệ với bạn trong thời gian sớm nhất.</p>
</p></div>
<div class="meta-input">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span><br />
<span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span><br />
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email*" /></span><br />
<span class="wpcf7-form-control-wrap your-tuvan"><input type="text" name="your-tuvan" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Nội dung cần tư vấn" /></span>
</div>
<div class="col-sm-12 captcha-form-vins">
<div class="wpcf7-form-control-wrap"><div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
<noscript>
<div style="width: 302px; height: 422px;">
<div style="width: 302px; height: 422px; position: relative;">
<div style="width: 302px; height: 422px; position: absolute;">
<iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
</iframe>
</div>
<div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
</textarea>
</div>
</div>
</div>
</noscript>
</div></div>
<div class="btn-form"><button class="frm_yctuvan" type="submit" style="margin-top: 15px;">ĐĂNG KÝ</button></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div><div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div></div></div></div></div><div id="section2" class="vc_row wpb_row vc_row-fluid full-width themesflat_1540549749 vc_row-o-full-height vc_row-o-columns-middle vc_row-o-equal-height vc_row-flex"><div class="row_overlay" style=""></div><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid container vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><h2 style="color: #000000;line-height: 45px;text-align: center" class="vc_custom_heading wpb_animate_when_almost_visible wpb_fadeInLeftBig fadeInLeftBig" >CHÀO MỪNG BẠN ĐẾN VỚI VINSOFTS<br />
CÔNG TY THIẾT KẾ WEBSITE CHẤT LƯỢNG HÀNG ĐẦU VIỆT NAM </h2><div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
<div class="wpb_text_column wpb_content_element" >
<div class="wpb_wrapper">
</div>
</div>
</div></div></div></div><div id="user-col" class="vc_row wpb_row vc_inner vc_row-fluid container vc_row-o-equal-height vc_row-o-content-middle vc_row-flex"><div class="item-user wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="72" height="72" data-wpfc-original-src="/wp-content/uploads/2018/11/2.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft vc_custom_1541153631177" >
<div class="wpb_wrapper">
<p style="text-align: center;"><span style="font-size: 14pt;"><strong>Kinh nghiệm</strong></span></p>
<p>10+ năm kinh nghiệm thiết kế website, hợp tác với 10+ quốc gia lớn như Việt Nam, Nhật Bản, Mỹ,&#8230;</p>
</div>
</div>
</div></div></div><div class="item-user wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="70" height="70" data-wpfc-original-src="/wp-content/uploads/2018/11/12.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeftBig fadeInLeftBig vc_custom_1541153639370" >
<div class="wpb_wrapper">
<p style="text-align: center;"><strong><span style="font-size: 14pt;">Chuyên gia hàng đầu</span></strong></p>
<p>50+ lập trình viên hàng đầu trong lĩnh vực Web</p>
</div>
</div>
</div></div></div><div class="item-user wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="72" height="72" data-wpfc-original-src="/wp-content/uploads/2018/11/21-1.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeftBig fadeInLeftBig vc_custom_1541153646756" >
<div class="wpb_wrapper">
<p style="text-align: center;"><span style="font-size: 14pt;"><strong>Dự án thực hiện</strong></span></p>
<p style="text-align: justify;">Dự án: 500+ dự án thành công đến từ nhiều ngành nghề như bất động sản, thương mại điện tử, giáo dục &#8230;</p>
</div>
</div>
</div></div></div><div class="item-user wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="72" height="72" data-wpfc-original-src="/wp-content/uploads/2018/11/3-1.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeftBig fadeInLeftBig vc_custom_1541153653558" >
<div class="wpb_wrapper">
<p style="text-align: center;"><span style="font-size: 14pt;"><strong>Hỗ trợ tuyệt vời</strong></span></p>
<p style="text-align: justify;">Đội ngũ tư vấn túc trực 24/7 hỗ trợ bạn mọi lúc mọi nơi</p>
</div>
</div>
</div></div></div></div></div></div></div></div><div id="section3" class="vc_row wpb_row vc_row-fluid full-width themesflat_1540549749 vc_row-o-full-height vc_row-o-columns-middle vc_row-o-equal-height vc_row-flex"><div class="row_overlay" style=""></div><div class="container wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid container vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1540982363965"><div class="wpb_wrapper"><h2 style="color: #000000;text-align: center" class="vc_custom_heading wpb_animate_when_almost_visible wpb_fadeInLeftBig fadeInLeftBig" >GIẢI PHÁP THIẾT KẾ WEBSITE CỦA VINSOFTS </h2><div class="vc_empty_space"   style="height: 25px" ><span class="vc_empty_space_inner"></span></div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeftBig fadeInLeftBig" >
<div class="wpb_wrapper">
<p style="text-align: center;"><span style="font-size: 14pt; color: #000000;">Để xây dựng một website thật chuyên nghiệp và ấn tượng với khách hàng, bạn cần lựa chọn một công ty uy tín hàng đầu như Vinsofts. Vậy giải pháp mà Vinsofts mang đến cho các doanh nghiệp là gì?</span></p>
</div>
</div>
</div></div></div></div><div class="vc_row wpb_row vc_inner vc_row-fluid container vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="880" height="738" data-wpfc-original-src="/wp-content/uploads/2018/11/web-2.gif" class="vc_single_image-img attachment-full" alt="blank" /></div>
</figure>
</div>
</div></div></div><div class="khung-anh wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="72" height="72" data-wpfc-original-src="/wp-content/uploads/2018/11/Untitled-1-Recovered.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  vc_custom_1541581367777" >
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #355ebd; font-size: 12pt;">PHÁT TRIỂN THEO NHU CẦU DN</span></h3>
</div>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown vc_custom_1541662875814" >
<div class="wpb_wrapper">
<p style="text-align: justify;"><strong>Tính năng của website được thiết kế riêng phù hợp với doanh nghiệp, nâng cao trải nghiệm khách hàng</strong></p>
</div>
</div>
<div  class="wpb_single_image wpb_content_element vc_align_center">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="72" height="72" data-wpfc-original-src="/wp-content/uploads/2018/11/6.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown vc_custom_1541662976677" >
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #2c52ab; font-size: 12pt;">WEBSITE TỐI ƯU CHUẨN SEO</span></h3>
</div>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown vc_custom_1541665981134" >
<div class="wpb_wrapper">
<p style="text-align: justify;"><strong> Website được thiết kế thân thiện với bộ máy tìm kiếm của Google, giúp website của bạn dễ dàng lên TOP</strong></p>
</div>
</div>
</div></div></div><div class="khung-anh wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="70" height="70" data-wpfc-original-src="/wp-content/uploads/2018/11/10.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  vc_custom_1541581817375" >
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #2e5abf; font-size: 12pt;">GIÁ ƯU ĐÃI &amp; TƯ VẤN TẬN TÂM</span></h3>
</div>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown vc_custom_1541663003798" >
<div class="wpb_wrapper">
<p style="text-align: justify;"><span style="color: #000000;"><strong> Bạn sẽ được sử dụng dịch vụ tốt nhất với mức giá cạnh tranh nhất thị trường Việt Nam</strong></span></p>
</div>
</div>
<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="70" height="70" data-wpfc-original-src="/wp-content/uploads/2018/11/11.jpg" class="vc_single_image-img attachment-thumbnail" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown vc_custom_1541663046045" >
<div class="wpb_wrapper">
<h3 style="text-align: center;"><span style="color: #365bb3; font-size: 12pt;">TƯ VẤN MARKETING HIỆU QUẢ</span></h3>
</div>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bounceInDown bounceInDown vc_custom_1541665998210" >
<div class="wpb_wrapper">
<p style="text-align: justify;"><span style="color: #000000;"><strong>Hỗ trợ tư vấn Marketing Online cho mọi doanh nghiệp, mọi lĩnh vực trên thị trường</strong></span></p>
</div>
</div>
</div></div></div></div></div></div></div></div><div id="section4" class="vc_row wpb_row vc_row-fluid full-width themesflat_1540549749 vc_row-o-full-height vc_row-o-columns-middle vc_row-o-equal-height vc_row-flex"><div class="row_overlay" style=""></div><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid container vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><h2 style="color: #000000;text-align: center" class="vc_custom_heading wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft" >KHÁCH HÀNG NÓI GÌ VỀ DỊCH VỤ THIẾT KẾ WEBSITE CỦA VINSOFTS</h2><div class="vc_empty_space"   style="height: 32px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div><div id="user-col" class="vc_row wpb_row vc_inner vc_row-fluid container"><div class="item-user wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper vc_box_shadow_border  vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="300" height="300" data-wpfc-original-src="/wp-content/uploads/2018/11/33-300x300.png" class="vc_single_image-img attachment-medium" alt="blank" data-wpfc-original-srcset="/wp-content/uploads/2018/11/33-300x300.png 300w, /wp-content/uploads/2018/11/33-100x100.png 100w, /wp-content/uploads/2018/11/33.png 600w, /wp-content/uploads/2018/11/33-150x150.png 150w, /wp-content/uploads/2018/11/33-570x570.png 570w, /wp-content/uploads/2018/11/33-120x120.png 120w" sizes="(max-width: 300px) 100vw, 300px" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bottom-to-top bottom-to-top" >
<div class="wpb_wrapper">
<p style="text-align: center;"><em><span style="color: #3366ff;"><strong>Ms Hoa</strong></span><br />
</em></p>
<p style="text-align: center;"><em>&#8220;Từng chi tiết nhỏ của website đều được thiết kế chỉnh chu, đem đến một website hoàn hảo về mọi mặt, phù hợp với lĩnh vực kinh doanh của chúng tôi. Cảm ơn Vinsofts rất nhiều &#8220;</em></p>
</div>
</div>
</div></div></div><div class="item-user wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper vc_box_shadow_border  vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="300" height="300" data-wpfc-original-src="/wp-content/uploads/2018/11/14-300x300.jpg" class="vc_single_image-img attachment-medium" alt="blank" data-wpfc-original-srcset="/wp-content/uploads/2018/11/14-300x300.jpg 300w, /wp-content/uploads/2018/11/14-100x100.jpg 100w, /wp-content/uploads/2018/11/14-600x600.jpg 600w, /wp-content/uploads/2018/11/14-150x150.jpg 150w, /wp-content/uploads/2018/11/14-768x768.jpg 768w, /wp-content/uploads/2018/11/14-570x570.jpg 570w, /wp-content/uploads/2018/11/14-120x120.jpg 120w, /wp-content/uploads/2018/11/14.jpg 960w" sizes="(max-width: 300px) 100vw, 300px" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bottom-to-top bottom-to-top" >
<div class="wpb_wrapper">
<p style="text-align: center;"><em><strong>Mr Chinh</strong><br />
</em></p>
<p style="text-align: center;"><em>&#8220;Chuyên nghiệp và ấn tượng. Đó là những gì Vinsofts mang lại cho website của chúng tôi. Tôi rất hài lòng với thiết kế cũng như sự hỗ trợ nhiệt tình của Vinsofts&#8221; </em></p>
</div>
</div>
</div></div></div><div class="item-user wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_center">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper vc_box_shadow_border  vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="300" height="300" data-wpfc-original-src="/wp-content/uploads/2018/11/30-300x300.jpg" class="vc_single_image-img attachment-medium" alt="blank" data-wpfc-original-srcset="/wp-content/uploads/2018/11/30-300x300.jpg 300w, /wp-content/uploads/2018/11/30-100x100.jpg 100w, /wp-content/uploads/2018/11/30-600x600.jpg 600w, /wp-content/uploads/2018/11/30-150x150.jpg 150w, /wp-content/uploads/2018/11/30-768x768.jpg 768w, /wp-content/uploads/2018/11/30-570x570.jpg 570w, /wp-content/uploads/2018/11/30-120x120.jpg 120w, /wp-content/uploads/2018/11/30.jpg 960w" sizes="(max-width: 300px) 100vw, 300px" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_bottom-to-top bottom-to-top" >
<div class="wpb_wrapper">
<p style="text-align: center;"><em><strong>Mr Duong</strong><br />
</em></p>
<p style="text-align: center;"><em>&#8220;Website do Vinsofts thiết kế được bố trí hợp lý, hiệu quả, nâng tầm thương hiệu cho doanh nghiệp. Chúng tôi rất hài lòng về dịch vụ thiết kế website của Vinsofst&#8221;</em></p>
</div>
</div>
</div></div></div></div></div></div></div></div><div id="last-section" class="vc_row wpb_row vc_row-fluid full-width themesflat_1540814237"><div class="row_overlay" style=""></div><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInLeft fadeInLeft" >
<div class="wpb_wrapper">
<h2 style="text-align: center;"><span style="color: #000000;">BIẾN Ý TƯỞNG CỦA BẠN THÀNH HIỆN THỰC</span></h2>
<p>&nbsp;</p>
<p style="text-align: center;"><span style="font-size: 14pt; color: #ffffff;"><span style="color: #000000;">Bạn sẽ dành bao nhiêu thời gian để suy nghĩ và xây dựng cho mình một ý tưởng? </span></span><span style="font-size: 14pt; color: #ffffff;"><span style="color: #000000;">Vinsofts sẽ giúp bạn hiện thực ý tưởng đó</span></span></p>
<p style="text-align: center;"><span style="font-size: 14pt; color: #ffffff;"><span style="color: #000000;"> bằng việc thiết kế một website ấn tượng, chuyên nghiệp.</span></span></p>
</div>
</div>
<div class="vc_empty_space"   style="height: 35px" ><span class="vc_empty_space_inner"></span></div>
<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_flipInX flipInX" >
<div class="wpb_wrapper">
<p style="text-align: center;"><button class="btn1-click-scroll">Đăng ký ngay</button></p>
</div>
</div>
<div class="wpb_text_column wpb_content_element" >
<div class="wpb_wrapper">
<h5 style="text-align: center;"><span style="color: #ffffff;">(+84) 24 6259 3148 | VINSOFTS ., JSC</span></h5>
</div>
</div>
</div></div></div></div>
<div>
<div class="quick-action">
<a href="tel:/#">
<div class="quick-action-item" id="phone"></div>
</a>
<a href="#" target="_blank">
<div class="quick-action-item" id="facebook"></div>
</a>
<div class="quick-action-item" id="register">
<div class="quick-register-des view-d">
<img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" data-wpfc-original-src="#" alt="AMIS">
<div class="register-text"></div>
</div>
</div>
</div>
</div>
<!-- <div id="back-top">
<a href="#top"><span></span></a>
</div> -->
</div>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- #content -->
<!-- Footer -->
<div class="footer_background">
<footer class="footer">      
<div class="container">
<div class="row"> 
<div class="footer-widgets">
<div class="col-md-4 col-sm-6">
<div id="text-2" class="widget widget_text">			<div class="textwidget"><p><a title="Financial Occult" href="/"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" style="height: 70px;" data-wpfc-original-src="/wp-content/uploads/2019/06/LOGO-FOOTER-min.png" alt="thumb" /><br />
</a><a title="Financial Occult" href="#"><br />
</a></p>
</div>
</div><div id="text-5" class="widget widget_text">			<div class="textwidget"><p>Với đội ngũ 70+ nhân viên và 10+ năm kinh nghiệm trong việc hợp tác và phát triển phần mềm cho các khách hàng từ khắp nơi trên thế giới, chúng tôi tự hào là một công ty gia công phần mềm hàng đầu tại Việt Nam. Chúng tôi luôn cam kết đem đến chất lượng dịch vụ cao nhất, trở thành đối tác tin cậy với bất kỳ đơn vị doanh nghiệp nào</p>
</div>
</div>                        </div>
<div class="col-md-2 col-sm-6">
<div id="text-10" class="widget widget_text"><h4 class="widget-title">KẾT NỐI NHANH</h4>			<div class="textwidget"></div>
</div><div id="widget_themesflat_socials-5" class="widget widget_themesflat_socials">
<ul class="themesflat-shortcode-socials">
<li class="facebook">
<a class="title" href="https://www.facebook.com/vinsoftsjsc" target="_blank" rel="alternate" title="Facebook">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="https://www.youtube.com/channel/UC33N-fg42Gkp1Fcin5Zp5EA/" target="_blank" rel="alternate" title="Youtube">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="https://www.linkedin.com/company/vinsofts/" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
</div><div id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><ul class="themesflat-shortcode-socials">
<li style="width: 115%;">
<a href="https://goo.gl/maps/UksWXm7gqr62" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>Google map</a>
</li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title">DỊCH VỤ NỔI BẬT</h4><div class="menu-footer_service-container"><ul id="menu-footer_service" class="menu"><li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2902"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2901"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2904"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-14" class="widget widget_nav_menu"><h4 class="widget-title">Thông Tin</h4><div class="menu-menu-footer-right-container"><ul id="menu-menu-footer-right" class="menu"><li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3559"><a href="/lien-he/">Liên hệ</a></li>
<li id="menu-item-4013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4013"><a href="/jobs/">Tuyển dụng</a></li>
</ul></div></div><div id="custom_html-6" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="cus_dmca">
<a href="//www.dmca.com/Protection/Status.aspx?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/dmca_protected_16_120.png?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559"  alt="DMCA.com Protection Status" /></a>
</div>
<style type="text/css">
.cus_dmca a img {
padding: 10px 0px 0px 48px;
}
</style></div></div>                        </div>
</div><!-- /.footer-widgets -->           
</div><!-- /.row -->    
</div><!-- /.container -->   
</footer>
<div class="content-register-footer">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="left">  
<!-- Other language content here -->
<div class="textwidget">
<p>Cơ quan chủ quản: Công ty Cổ phần Vinsofts</p>
<!-- <p>Trụ sở chính: Tầng 8 Tòa nhà Sannam đường Duy Tân, phường Dịch Vọng Hậu, Q.Cầu Giấy, Hà Nội</p> -->
<p>Văn phòng tại Hà Nội: Tầng 5, số 8 Phan Văn Trường, phường Dịch Vọng Hậu, Cầu Giấy, Hà Nội</p>
<!-- <p>Văn phòng tại Tp.HCM: Unit P5-16.B Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, Tp.HCM</p> -->
<p>Văn phòng tại Tp.HCM: P516 Block C Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, TP.HCM</p>
<p>Tel:&nbsp;<a href="tel:0462593148">04.6259.3148</a>&nbsp;– Hotlline:&nbsp;<a href="tel:0961678247">0961.678.247</a>&nbsp;– Email:&nbsp;<a href="mailto:info@vinsofts.com">info@vinsofts.com</a></p>
<p>Giấy phép kinh doanh số 0107354530 do Sở Kế hoạch và đầu tư cấp ngày 14/03/2016</p>
</div>
</div>
</div>
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="right">
<!-- Other language content here -->
<div class="textwidget">
<p>Bạn vui lòng đọc kỹ <a href="/vi/chinh-sach-bao-mat">Chính sách bảo mật thông tin</a> và <a href="/vi/dieu-khoan-su-dung">Điều khoản sử dụng</a>!</p>
<p>Website đã được thông báo và được chấp nhận bởi Cục TMĐT và CNTT, Bộ Công Thương.</p>
<p><a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43043"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-medium wp-image-5318" data-wpfc-original-src="/wp-content/uploads/2018/10/vinsofts-dathongbao-300x114.png" alt="blank" width="300" height="114" /></a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Bottom -->
<div class="bottom">
<div class="container">           
<div class="row">
<div class="col-md-12">
<div class="copyright">                        
<p>Copyright <i class="fa fa-copyright"></i> 2012 - 2019
<a href="#">Vinsofts JSC</a>. All rights reserved.</p>                        </div>
<!-- Go Top -->
<a class="go-top show">
<i class="fa fa-chevron-up"></i>
</a>
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->
</div> 
</div> <!-- Footer Background Image -->
</div><!-- /#boxed -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.googletagmanager.com/ns.html?id=GTM-W8XSN4W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <link rel='stylesheet' id='fo-child-css'  href='/wp-content/themes/fo-child/common/css/custom-css.css?version=137' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='animate-css-css'  href='/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min.css?version=4027' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/m9qyyedy/4ct9.css" media="all"/>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&#038;render=explicit'></script>
<noscript id="wpfc-google-fonts"><link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
<link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
</noscript>
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/folii5hc/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7lob9sfs/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/d6ougkxu/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/eqjz612f/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lpm2zzii/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/fte2l3l2/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mkx6gd8y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mknr8988/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/zdl5aix/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7xsi43ue/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/976glhgy/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1eotoqvg/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/e726dwxx/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/llhj9sk8/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/erkz11cs/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1281u0q0/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lll4wyip/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/dq6j6xeh/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/869bu30y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/47mj14y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/qa893n0m/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/es8b11eb/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/knvqh2vr/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/49n6o1s/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/etkqo5pl/4ct9.js'></script> -->
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"6207c2d185","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"6207c2d185","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script src='/wp-content/cache/wpfc-minified/dtpdndol/4ct9.js' type="text/javascript"></script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script defer src='/wp-content/cache/wpfc-minified/dtpdndol/4ct9.js' type="text/javascript"></script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// hide #back-top first
// jQuery("#back-top").hide();
// fade in #back-top
// jQuery(function () {
//     jQuery(window).scroll(function () {
//         if (jQuery(this).scrollTop() > 100) {
//             jQuery('#back-top').fadeIn();
//         } else {
//             jQuery('#back-top').fadeOut();
//         }
//     });
//     // scroll body to 0px on click
//     jQuery('#back-top a').click(function () {
//         jQuery('body,html').animate({
//             scrollTop: 0
//         }, 800);
//         return false;
//     });
// });
jQuery('.full-content-landing .btn1-click-input').click(function(){
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
jQuery('.full-content-landing .btn1-click-scroll,.full-content-landing #register.quick-action-item').click(function(){
jQuery('html,body').animate({
scrollTop: jQuery(".full-content-landing").offset().top},
'slow');    
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
// landing-page
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("section1");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(2) a").addClass("section2");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(3) a").addClass("section3");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(4) a").addClass("section4");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(5) a").addClass("section5");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(6) a").addClass("section6");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(7) a").addClass("section7");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(8) a").addClass("section8");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(9) a").addClass("section9");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(10) a").addClass("section10");
var i = 0;
jQuery(".full-content-landing .header-landing-page .menu ul li a").click(function(e){
// console.log(jQuery(this).attr('href'));
if(i != jQuery(this).attr('class')){
jQuery(".full-content-landing .header-landing-page .menu ul li a").not(this).removeClass("active");
jQuery(this).addClass("active");
// console.log(jQuery(this).attr('class'));
var id = jQuery(this).attr('class').replace(' active', '');
var position = jQuery('#'+id).offset().top;
if (jQuery(this).attr('class') == 'section1 active') {
position = 0;
}
jQuery("body, html").animate({
scrollTop: position + 1
} /* speed */ );
}
i = jQuery(this).attr('class');
});
if (jQuery(window).scrollTop() == 0) {
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("active");
}
jQuery(window).scroll(function(){
var scroll = jQuery(window).scrollTop();
jQuery('.full-width').each(function(){
var topElement_2 = jQuery(this).offset().top;
// console.log(top, scroll, 'dsadfshaj');
if((scroll) > topElement_2){
// console.log(scroll);
var a = jQuery(this).attr('id');
// console.log(a);
jQuery('.full-content-landing .header-landing-page .menu ul li a.'+a).addClass('active');
jQuery(".full-content-landing .header-landing-page .menu ul li a").not('.full-content-landing .header-landing-page .menu ul li a.'+a).removeClass("active");
}
})   	
});
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// hide #back-top first
// jQuery("#back-top").hide();
// fade in #back-top
// jQuery(function () {
//     jQuery(window).scroll(function () {
//         if (jQuery(this).scrollTop() > 100) {
//             jQuery('#back-top').fadeIn();
//         } else {
//             jQuery('#back-top').fadeOut();
//         }
//     });
//     // scroll body to 0px on click
//     jQuery('#back-top a').click(function () {
//         jQuery('body,html').animate({
//             scrollTop: 0
//         }, 800);
//         return false;
//     });
// });
jQuery('.full-content-landing .btn1-click-input').click(function(){
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
jQuery('.full-content-landing .btn1-click-scroll,.full-content-landing #register.quick-action-item').click(function(){
jQuery('html,body').animate({
scrollTop: jQuery(".full-content-landing").offset().top},
'slow');    
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
// landing-page
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("section1");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(2) a").addClass("section2");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(3) a").addClass("section3");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(4) a").addClass("section4");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(5) a").addClass("section5");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(6) a").addClass("section6");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(7) a").addClass("section7");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(8) a").addClass("section8");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(9) a").addClass("section9");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(10) a").addClass("section10");
var i = 0;
jQuery(".full-content-landing .header-landing-page .menu ul li a").click(function(e){
// console.log(jQuery(this).attr('href'));
if(i != jQuery(this).attr('class')){
jQuery(".full-content-landing .header-landing-page .menu ul li a").not(this).removeClass("active");
jQuery(this).addClass("active");
// console.log(jQuery(this).attr('class'));
var id = jQuery(this).attr('class').replace(' active', '');
var position = jQuery('#'+id).offset().top;
if (jQuery(this).attr('class') == 'section1 active') {
position = 0;
}
jQuery("body, html").animate({
scrollTop: position + 1
} /* speed */ );
}
i = jQuery(this).attr('class');
});
if (jQuery(window).scrollTop() == 0) {
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("active");
}
jQuery(window).scroll(function(){
var scroll = jQuery(window).scrollTop();
jQuery('.full-width').each(function(){
var topElement_2 = jQuery(this).offset().top;
// console.log(top, scroll, 'dsadfshaj');
if((scroll) > topElement_2){
// console.log(scroll);
var a = jQuery(this).attr('id');
// console.log(a);
jQuery('.full-content-landing .header-landing-page .menu ul li a.'+a).addClass('active');
jQuery(".full-content-landing .header-landing-page .menu ul li a").not('.full-content-landing .header-landing-page .menu ul li a.'+a).removeClass("active");
}
})   	
});
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
</body>
</html><noscript id="wpfc-google-fonts"><link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
<link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
</noscript>
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/folii5hc/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7lob9sfs/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/d6ougkxu/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/eqjz612f/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lpm2zzii/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/fte2l3l2/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mkx6gd8y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mknr8988/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/zdl5aix/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7xsi43ue/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/976glhgy/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1eotoqvg/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/e726dwxx/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/llhj9sk8/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/erkz11cs/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1281u0q0/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lll4wyip/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/dq6j6xeh/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/869bu30y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/47mj14y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/qa893n0m/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/es8b11eb/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/knvqh2vr/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/49n6o1s/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/etkqo5pl/4ct9.js'></script> -->
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"6207c2d185","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"6207c2d185","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script src='/wp-content/cache/wpfc-minified/dtpdndol/4ct9.js' type="text/javascript"></script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script defer src='/wp-content/cache/wpfc-minified/dtpdndol/4ct9.js' type="text/javascript"></script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// hide #back-top first
// jQuery("#back-top").hide();
// fade in #back-top
// jQuery(function () {
//     jQuery(window).scroll(function () {
//         if (jQuery(this).scrollTop() > 100) {
//             jQuery('#back-top').fadeIn();
//         } else {
//             jQuery('#back-top').fadeOut();
//         }
//     });
//     // scroll body to 0px on click
//     jQuery('#back-top a').click(function () {
//         jQuery('body,html').animate({
//             scrollTop: 0
//         }, 800);
//         return false;
//     });
// });
jQuery('.full-content-landing .btn1-click-input').click(function(){
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
jQuery('.full-content-landing .btn1-click-scroll,.full-content-landing #register.quick-action-item').click(function(){
jQuery('html,body').animate({
scrollTop: jQuery(".full-content-landing").offset().top},
'slow');    
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
// landing-page
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("section1");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(2) a").addClass("section2");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(3) a").addClass("section3");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(4) a").addClass("section4");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(5) a").addClass("section5");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(6) a").addClass("section6");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(7) a").addClass("section7");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(8) a").addClass("section8");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(9) a").addClass("section9");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(10) a").addClass("section10");
var i = 0;
jQuery(".full-content-landing .header-landing-page .menu ul li a").click(function(e){
// console.log(jQuery(this).attr('href'));
if(i != jQuery(this).attr('class')){
jQuery(".full-content-landing .header-landing-page .menu ul li a").not(this).removeClass("active");
jQuery(this).addClass("active");
// console.log(jQuery(this).attr('class'));
var id = jQuery(this).attr('class').replace(' active', '');
var position = jQuery('#'+id).offset().top;
if (jQuery(this).attr('class') == 'section1 active') {
position = 0;
}
jQuery("body, html").animate({
scrollTop: position + 1
} /* speed */ );
}
i = jQuery(this).attr('class');
});
if (jQuery(window).scrollTop() == 0) {
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("active");
}
jQuery(window).scroll(function(){
var scroll = jQuery(window).scrollTop();
jQuery('.full-width').each(function(){
var topElement_2 = jQuery(this).offset().top;
// console.log(top, scroll, 'dsadfshaj');
if((scroll) > topElement_2){
// console.log(scroll);
var a = jQuery(this).attr('id');
// console.log(a);
jQuery('.full-content-landing .header-landing-page .menu ul li a.'+a).addClass('active');
jQuery(".full-content-landing .header-landing-page .menu ul li a").not('.full-content-landing .header-landing-page .menu ul li a.'+a).removeClass("active");
}
})   	
});
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// hide #back-top first
// jQuery("#back-top").hide();
// fade in #back-top
// jQuery(function () {
//     jQuery(window).scroll(function () {
//         if (jQuery(this).scrollTop() > 100) {
//             jQuery('#back-top').fadeIn();
//         } else {
//             jQuery('#back-top').fadeOut();
//         }
//     });
//     // scroll body to 0px on click
//     jQuery('#back-top a').click(function () {
//         jQuery('body,html').animate({
//             scrollTop: 0
//         }, 800);
//         return false;
//     });
// });
jQuery('.full-content-landing .btn1-click-input').click(function(){
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
jQuery('.full-content-landing .btn1-click-scroll,.full-content-landing #register.quick-action-item').click(function(){
jQuery('html,body').animate({
scrollTop: jQuery(".full-content-landing").offset().top},
'slow');    
jQuery('.full-content-landing .contact-landing-page-1 .meta-input span:nth-of-type(1) input').focus();
});
// landing-page
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("section1");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(2) a").addClass("section2");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(3) a").addClass("section3");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(4) a").addClass("section4");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(5) a").addClass("section5");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(6) a").addClass("section6");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(7) a").addClass("section7");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(8) a").addClass("section8");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(9) a").addClass("section9");
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(10) a").addClass("section10");
var i = 0;
jQuery(".full-content-landing .header-landing-page .menu ul li a").click(function(e){
// console.log(jQuery(this).attr('href'));
if(i != jQuery(this).attr('class')){
jQuery(".full-content-landing .header-landing-page .menu ul li a").not(this).removeClass("active");
jQuery(this).addClass("active");
// console.log(jQuery(this).attr('class'));
var id = jQuery(this).attr('class').replace(' active', '');
var position = jQuery('#'+id).offset().top;
if (jQuery(this).attr('class') == 'section1 active') {
position = 0;
}
jQuery("body, html").animate({
scrollTop: position + 1
} /* speed */ );
}
i = jQuery(this).attr('class');
});
if (jQuery(window).scrollTop() == 0) {
jQuery(".full-content-landing .header-landing-page .menu ul li:nth-of-type(1) a").addClass("active");
}
jQuery(window).scroll(function(){
var scroll = jQuery(window).scrollTop();
jQuery('.full-width').each(function(){
var topElement_2 = jQuery(this).offset().top;
// console.log(top, scroll, 'dsadfshaj');
if((scroll) > topElement_2){
// console.log(scroll);
var a = jQuery(this).attr('id');
// console.log(a);
jQuery('.full-content-landing .header-landing-page .menu ul li a.'+a).addClass('active');
jQuery(".full-content-landing .header-landing-page .menu ul li a").not('.full-content-landing .header-landing-page .menu ul li a.'+a).removeClass("active");
}
})   	
});
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
</body>
</html><!-- WP Fastest Cache file was created in 8.0654361248016 seconds, on 29-07-20 18:58:51 -->