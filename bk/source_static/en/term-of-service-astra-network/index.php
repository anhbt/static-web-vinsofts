<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="/xmlrpc.php">
<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W8XSN4W');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125473191-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-125473191-1');
</script>
<title>Terms of service of Astra Network - Vinsofts JSC - Vietnam IT Outsourcing Company</title>
<!-- This site is optimized with the Yoast SEO Premium plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
<link rel="canonical" href="/en/term-of-service-astra-network/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Terms of service of Astra Network - Vinsofts JSC - Vietnam IT Outsourcing Company" />
<meta property="og:description" content="Terms of service of Astra Network Welcome to Astra Network! Astra is the world’s leading social networking platform based on blockchain for tourism. Astra aspires to build a community of dynamic, creative, travel-loving people who keep up with innovations; connect individuals together and with service providers; as well as support the development of businesses in &hellip;" />
<meta property="og:url" content="/en/term-of-service-astra-network/" />
<meta property="og:site_name" content="Vinsofts JSC - Vietnam IT Outsourcing Company" />
<meta property="article:publisher" content="https://www.facebook.com/vinsoftsjsc" />
<meta property="fb:app_id" content="1096476827181655" />
<meta property="og:image" content="/wp-content/uploads/2018/08/all-vi.jpg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Terms of service of Astra Network Welcome to Astra Network! Astra is the world’s leading social networking platform based on blockchain for tourism. Astra aspires to build a community of dynamic, creative, travel-loving people who keep up with innovations; connect individuals together and with service providers; as well as support the development of businesses in [&hellip;]" />
<meta name="twitter:title" content="Terms of service of Astra Network - Vinsofts JSC - Vietnam IT Outsourcing Company" />
<meta name="twitter:image" content="/wp-content/uploads/2018/08/all-vi.jpg" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"\/en\/","name":"Vinsofts JSC - Vietnam IT Outsourcing Company","potentialAction":{"@type":"SearchAction","target":"\/en\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO Premium plugin. -->
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel="alternate" type="application/rss+xml" title="Vinsofts JSC - Vietnam IT Outsourcing Company &raquo; Feed" href="/en/feed/" />
<link rel="alternate" type="application/rss+xml" title="Vinsofts JSC - Vietnam IT Outsourcing Company &raquo; Comments Feed" href="/en/comments/feed/" />
<!-- <link rel='stylesheet' id='vc_extend_shortcode-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes.css?version=9890' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_extend_style-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes-3rd.css?version=9723' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='bootstrap-css'  href='/wp-content/themes/fo/css/bootstrap.css?version=3381' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?version=8075' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qjpyrk45/4b9o.css" media="all"/>
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<!-- <link rel='stylesheet' id='qts_front_styles-css'  href='/wp-content/plugins/qtranslate-slug/assets/css/qts-default.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_main-css'  href='/wp-content/themes/fo/css/main.css?version=2298' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-style-css'  href='/wp-content/themes/fo-child/style.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='font-fontawesome-css'  href='/wp-content/themes/fo/css/font-awesome.css?version=4992' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-ionicons-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=2100' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_simpleline-css-css'  href='/wp-content/themes/fo/css/simple-line-icons.css?version=4149' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_ion_icon-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=3840' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/9jminkn5/4b9o.css" media="all"/>
<!--[if lte IE 9]><link rel='stylesheet' id='ie9-css'  href='/wp-content/themes/fo/css/ie.css?version=437' type='text/css' media='all' /><![endif]-->
<!-- <link rel='stylesheet' id='themesflat_logo-css'  href='/wp-content/themes/fo/css/logo.css?version=3756' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_animate-css'  href='/wp-content/themes/fo/css/animate.css?version=2462' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_responsive-css'  href='/wp-content/themes/fo/css/responsive.css?version=4644' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-inline-css-css'  href='/wp-content/themes/fo/css/inline-css.css?version=9525' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/mmoo7qcx/4b9o.css" media="all"/>
<style id='themesflat-inline-css-inline-css' type='text/css'>
.logo{padding-top:24px; padding-left:15px; }
.footer{padding-top:64px; padding-bottom:74px; }
.page-title{padding-top:21px; }
.logo img, .logo svg { height:70px; }
.page-title .overlay{ background: #ffffff}.page-title {background: url() center /cover no-repeat;}.page-title h1 {color:#21536c!important;
}
.breadcrumbs span,.breadcrumbs span a, .breadcrumbs a {color:#595959!important;
}
body,button,input,select,textarea { font-family:Poppins ; }
body,button,input,select,textarea { font-weight:400;}
body,button,input,select,textarea { font-style:normal; }
body,button,input,select,textarea { font-size:14px; }
body,button,input,select,textarea { line-height:25px ; }
h1,h2,h3,h5,h6 { font-family:Poppins;}
h1,h2,h3,h4,h5,h6 { font-weight:600;}
h1,h2,h3,h4,h5,h6  { font-style:normal; }
#mainnav > ul > li > a { font-family:Poppins;}
#mainnav > ul > li > a { font-weight:600;}
#mainnav > ul > li > a  { font-style:normal; }
#mainnav ul li a { font-size:14px;}
#mainnav > ul > li > a { line_height100px;}
h1 { font-size:32px; }
h2 { font-size:25px; }
h3 { font-size:22px; }
h4 { font-size:18px; }
h5 { font-size:15px; }
h6 { font-size:14px; }
.iconbox .box-header .box-icon span,a:hover, a:focus,.portfolio-filter li a:hover, .portfolio-filter li.active a,.themesflat-portfolio .item .category-post a,.color_theme,.widget ul li a:hover,.footer-widgets ul li a:hover,.footer a:hover,.themesflat-top a:hover,.themesflat-portfolio .portfolio-container.grid2 .title-post a:hover,.themesflat-button.no-background, .themesflat-button.blog-list-small,.show-search a i:hover,.widget.widget_categories ul li a:hover,.breadcrumbs span a:hover, .breadcrumbs a:hover,.comment-list-wrap .comment-reply-link,.portfolio-single .content-portfolio-detail h3,.portfolio-single .content-portfolio-detail ul li:before, .themesflat-list-star li:before, .themesflat-list li:before,.navigation.posts-navigation .nav-links li a .meta-nav,.testimonial-sliders.style3 .author-name a,ul.iconlist .list-title a:hover,.themesflat_iconbox .iconbox-icon .icon span,.bottom .copyright a,.top_bar2 .wrap-header-content ul li i { color:#337493;}
#Ellipse_7 circle,.testimonial-sliders .logo_svg path { fill:#337493;}
.info-top-right a.appoinment, .wrap-header-content a.appoinment,button, input[type=button], input[type=reset], input[type=submit],.go-top:hover,.portfolio-filter.filter-2 li a:hover, .portfolio-filter.filter-2 li.active a,.themesflat-socials li a:hover, .entry-footer .social-share-article ul li a:hover,.themesflat-button,.featured-post.blog-slider .flex-prev, .featured-post.blog-slider .flex-next,mark, ins,#themesflat-portfolio-carousel ul.flex-direction-nav li a, .flex-direction-nav li a,.navigation.posts-navigation .nav-links li a:after,.title_related_portfolio:after,.navigation.loadmore a:hover,.owl-theme .owl-controls .owl-nav [class*=owl-],.widget.widget_tag_cloud .tagcloud a,.btn-menu:before, .btn-menu:after, .btn-menu span,.themesflat_counter.style2 .themesflat_counter-icon .icon,widget a.appoinment,.themesflat_imagebox .imagebox-image:after,.nav-widget a.appoinment { background-color:#337493; }
.themesflat_btnslider:not(:hover) {
background-color:#337493!important;
}
.loading-effect-2 > span, .loading-effect-2 > span:before, .loading-effect-2 > span:after,textarea:focus, input[type=text]:focus, input[type=password]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=date]:focus, input[type=month]:focus, input[type=time]:focus, input[type=week]:focus, input[type=number]:focus, input[type=email]:focus, input[type=url]:focus, input[type=search]:focus, input[type=tel]:focus, input[type=color]:focus,select:focus,.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span,.navigation.loadmore a:hover { border-color:#337493}
{
border-color:#337493!important;
}
{
color: #fff !important;
}
{
background-color: #2e363a !important;
}
#Financial_Occult text,#F__x26__O tspan { fill:#595959;}
body { color:#595959}
a,.portfolio-filter li a,.themesflat-portfolio .item .category-post a:hover,.title-section .title,ul.iconlist .list-title a,.breadcrumbs span a, .breadcrumbs a,.breadcrumbs span,h1, h2, h3, h4, h5, h6,strong,.testimonial-content blockquote,.testimonial-content .author-info,.sidebar .widget ul li a,.themesflat_counter.style2 .themesflat_counter-content-right,.themesflat_counter.style2 .themesflat_counter-content-left,.title_related_portfolio,.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation .current, .page-links a:hover, .page-links a:focus,.widget_search .search-form input[type=search],.entry-meta ul,.entry-meta ul.meta-right,.entry-footer strong,.widget.widget_archive ul li:before, .widget.widget_categories ul li:before, .widget.widget_recent_entries ul li:before { color:#595959}
.owl-theme .owl-dots .owl-dot span,.widget .widget-title:after, .widget .widget-title:before,ul.iconlist li.circle:before { background-color:#595959}
.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation:not(.loadmore) .current, .page-links a:hover, .page-links a:focus, .page-links > span { border-color:#595959}
.themesflat-top { background-color:#21536c ; } 
.themesflat-top .border-left:before, .themesflat-widget-languages:before, .themesflat-top .border-right:after { background-color: rgba(255,255,255,0.2);}.themesflat-top,.info-top-right,.themesflat-top a, .themesflat-top .themesflat-socials li a { color:#ffffff ;} 
.themesflat_header_wrap.header-style1,.nav.header-style2,.themesflat_header_wrap.header-style3,.nav.header-style4,.header.widget-header .nav { background-color:#fff;}
#mainnav > ul > li > a { color:#595959;}
#mainnav > ul > li > a:hover,#mainnav > ul > li.current-menu-item > a { color:#377493 !important;}
#mainnav ul.sub-menu > li > a { color:#ffffff!important;}
#mainnav ul.sub-menu { background-color:#377493;}
#mainnav ul.sub-menu > li > a:hover { background-color:#aac9ce!important;}
#mainnav ul.sub-menu > li { border-color:#dde9eb!important;}
.footer_background:before { background-color:#21536c;}
.footer a, .footer, .themesflat-before-footer .custom-info > div,.footer-widgets ul li a,.footer-widgets .company-description p { color:#f9f9f9;}
.bottom { background-color:#337493;}
.bottom .copyright p,.bottom #menu-bottom li a { color:#e5e5e5;}
.white #Financial_Occult text,.white #F__x26__O tspan {
fill: #fff; }test_filter_render
</style>
<link rel='https://api.w.org/' href='/en/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.8" />
<link rel='shortlink' href='/en/?p=8679' />
<link rel="alternate" type="application/json+oembed" href="/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fen%2Fterm-of-service-astra-network%2F" />
<link rel="alternate" type="text/xml+oembed" href="/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fen%2Fterm-of-service-astra-network%2F&#038;format=xml" />
<meta name="generator" content="qTranslate-X 3.4.6.8" />
<link hreflang="x-default" href="/dieu-khoan-mxh-astra/" rel="alternate" />
<link hreflang="vi" href="/dieu-khoan-mxh-astra/" rel="alternate" />
<link hreflang="en" href="/en/term-of-service-astra-network/" rel="alternate" />
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?version=9578" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.6.3.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/10/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="/wp-content/uploads/2018/10/cropped-favicon-270x270.png" />
<style type="text/css" id="wp-custom-css">
.page-wrap.sidebar-left #primary.content-area {
/*     margin-top: -40px; */
}
.single-post .page-wrap.sidebar-left #primary.content-area {
margin-top: 0;
}
.themesflat-portfolio .item .featured-post {
text-align: center;
}
.themesflat-portfolio .item .featured-post img, .content-area.fullwidth .themesflat_iconbox.inline-left img {
height: 220px;
width: 100%;
object-fit: cover;
}
/* .content-area.fullwidth .themesflat_iconbox.transparent .title {
height: 55px;
display: flex;
align-items: center;
margin-top: 0;
}
.content-area.fullwidth .themesflat_iconbox.transparent .iconbox-content p:nth-child(2) {
height: 125px;
overflow: hidden;
}
*/
.header.header-sticky .wpmenucartli {
display:none !important;
}
.header.header-sticky #menu-main li:first-child {
display: inline-block !important;
}
#mainnav>ul>li {
margin-left: 25px !important;
}		</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<script data-wpfc-render="false">var Wpfcll={s:[],osl:0,scroll:false,i:function(){Wpfcll.ss();window.addEventListener('load',function(){window.addEventListener("DOMSubtreeModified",function(e){Wpfcll.osl=Wpfcll.s.length;Wpfcll.ss();if(Wpfcll.s.length > Wpfcll.osl){Wpfcll.ls(false);}},false);Wpfcll.ls(true);});window.addEventListener('scroll',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('resize',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('click',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});},c:function(e,pageload){var w=document.documentElement.clientHeight || body.clientHeight;var n=0;if(pageload){n=0;}else{n=(w > 800) ? 800:200;n=Wpfcll.scroll ? 800:n;}var er=e.getBoundingClientRect();var t=0;var p=e.parentNode;if(typeof p.getBoundingClientRect=="undefined"){var pr=false;}else{var pr=p.getBoundingClientRect();}if(er.x==0 && er.y==0){for(var i=0;i < 10;i++){if(p){if(pr.x==0 && pr.y==0){p=p.parentNode;if(typeof p.getBoundingClientRect=="undefined"){pr=false;}else{pr=p.getBoundingClientRect();}}else{t=pr.top;break;}}};}else{t=er.top;}if(w - t+n > 0){return true;}return false;},r:function(e,pageload){var s=this;var oc,ot;try{oc=e.getAttribute("data-wpfc-original-src");ot=e.getAttribute("data-wpfc-original-srcset");if(s.c(e,pageload)){if(oc || ot){if(e.tagName=="DIV" || e.tagName=="A"){e.style.backgroundImage="url("+oc+")";e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");}else{if(oc){e.setAttribute('src',oc);}if(ot){e.setAttribute('srcset',ot);}e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");if(e.tagName=="IFRAME"){e.onload=function(){if(typeof window.jQuery !="undefined"){if(jQuery.fn.fitVids){jQuery(e).parent().fitVids({customSelector:"iframe[src]"});}}var s=e.getAttribute("src").match(/templates\/youtube\.html\#(.+)/);var y="https://www.youtube.com/embed/";if(s){try{var i=e.contentDocument || e.contentWindow;if(i.location.href=="about:blank"){e.setAttribute('src',y+s[1]);}}catch(err){e.setAttribute('src',y+s[1]);}}}}}}else{if(e.tagName=="NOSCRIPT"){if(jQuery(e).attr("data-type")=="wpfc"){e.removeAttribute("data-type");jQuery(e).after(jQuery(e).text());}}}}}catch(error){console.log(error);console.log("==>",e);}},ss:function(){var i=Array.prototype.slice.call(document.getElementsByTagName("img"));var f=Array.prototype.slice.call(document.getElementsByTagName("iframe"));var d=Array.prototype.slice.call(document.getElementsByTagName("div"));var a=Array.prototype.slice.call(document.getElementsByTagName("a"));var n=Array.prototype.slice.call(document.getElementsByTagName("noscript"));this.s=i.concat(f).concat(d).concat(a).concat(n);},ls:function(pageload){var s=this;[].forEach.call(s.s,function(e,index){s.r(e,pageload);});}};document.addEventListener('DOMContentLoaded',function(){wpfci();});function wpfci(){Wpfcll.i();}</script>
</head>
<body class="page-template page-template-tpl page-template-page_fullwidth page-template-tplpage_fullwidth-php page page-id-8679  has-topbar header_sticky page-fullwidth wide sidebar-left bottom-center wpb-js-composer js-comp-ver-5.4.7 vc_responsive en">
<div class="themesflat-boxed">
<!-- Preloader -->
<div class="preloader">
<div class="clear-loading loading-effect-2">
<span></span>
</div>
</div>
<!-- Top -->
<div class="themesflat-top header-style1">    
<div class="container">
<div class="container-inside">
<div class="content-left">
<ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-chooser">
<li class="lang-vi"><a href="/vi/dieu-khoan-mxh-astra/" hreflang="vi" title="VI (vi)" class="qtranxs_image qtranxs_image_vi"><img src="/wp-content/plugins/qtranslate-x/flags/vn.png" alt="VI (vi)" /><span style="display:none">VI</span></a></li>
<li class="lang-en active"><a href="/en/term-of-service-astra-network/" hreflang="en" title="EN (en)" class="qtranxs_image qtranxs_image_en"><img src="/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN (en)" /><span style="display:none">EN</span></a></li>
</ul><div class="qtranxs_widget_end"></div>
<ul>
<li class="border-right">
<i class="fa fa-phone"></i><a href="tel:0247 1080 285" target="_top"> (+84) 0247 1080 285</a>     
</li>
<li>
<i class="fa fa-envelope"></i> <a href="mailto:info@vinsofts.com" target="_top">  info@vinsofts.com</a> 
</li>
</ul>	
</div><!-- /.col-md-7 -->
<div class="content-right">
<ul class="themesflat-socials">
<li class="facebook">
<a class="title" href="">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="/en/term-of-service-astra-network/linkedin.com/company/vinsofts" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
<div class="info-top-right border-left">
<span><i class="fa fa-question-circle"></i>Got a project idea?</span>
<a class="appoinment" href="#">GET QUOTE</a>
</div>            </div><!-- /.col-md-5 -->
</div><!-- /.container -->
</div><!-- /.container -->        
</div><!-- /.top -->
<div class="popup">
<!--Form contact-->
<div id="myModal2" class="modal fade in" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content form contact-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<div role="form" class="wpcf7" id="wpcf7-f3697-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/en/term-of-service-astra-network/#wpcf7-f3697-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3697" />
<input type="hidden" name="_wpcf7_version" value="5.0.5" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3697-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<div class="row gutter-10 contactform4">
<h4 class="title-form" style="display:none">Please fill in the form below and send us, we will contact you soon.</h4>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name *" /></span></div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Phone Number *" /></span> </div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Your Question" /></span> </div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can"><select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false"><option value="Please select your interest">Please select your interest</option><option value="Blockchain Application Development">Blockchain Application Development</option><option value="Web App and Website Development">Web App and Website Development</option><option value="Mobile Application Development">Mobile Application Development</option><option value="Custom Software Development">Custom Software Development</option><option value="Software Testing and QA Services">Software Testing and QA Services</option><option value="Hire developers from us">Hire developers from us</option></select></span></div>
<div class="col-sm-6">
<span>Attache file</span><br />
<span class="wpcf7-form-control-wrap file-978"><input type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" /></span>
</div>
<div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="More details"></textarea></span> </div>
<div class="col-sm-6 captcha-form-vins">
<div class="wpcf7-form-control-wrap"><div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
<noscript>
<div style="width: 302px; height: 422px;">
<div style="width: 302px; height: 422px; position: relative;">
<div style="width: 302px; height: 422px; position: absolute;">
<iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
</iframe>
</div>
<div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
</textarea>
</div>
</div>
</div>
</noscript>
</div>
</div>
<div class="col-sm-6"><input type="submit" value="SEND" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>          </div>
</div>
</div>
</div>
</div>
<div id="popup-team-member" class="popup-2">
<div class="modal fade" id="myModa1" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="row">
<div class="image-members-full col-md-4 col-sm-6 col-xs-12">
<div class="image">
</div>
</div>
<div class="title-modal col-md-8 col-sm-6 col-xs-12">
<div class="title">
<h3 class="modal-title"></h3>
<span class="modal-title-mini"></span>
<div class="social-content">
</div>
</div>
</div>
</div>
</div>
<div class="line-color"></div>
<div class="modal-body">
<div class="content-member">
</div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
</div>
<style type="text/css">
.themesflat-top .content-left {
float: none;
}
.themesflat-top .content-left ul{
float: left;
}
.themesflat-top .content-left ul.language-chooser{
/*margin-right: 15px;*/
}
.themesflat-top .content-left ul.language-chooser li{
padding-left: 0px;
padding-right: 10px;
}
.themesflat-top .content-left ul.language-chooser li a::after {
content: "";
border-right: 1px solid #d8d8d8;
margin-left: 6px;
}
.themesflat-top .content-left ul.language-chooser li:last-child a::after {
border: 0px;
content: "";
}
.themesflat-top .content-left ul.language-chooser li.active a,
.themesflat-top .content-left ul.language-chooser li a:hover{
color: #3d9be9;
opacity: 1;
}
.themesflat-top .content-left ul.language-chooser li a{
padding: 0px;
font-size: 13px;
color: #ffffff94;
opacity: 0.4;
}
.themesflat-top .content-left ul.language-chooser li a img{
width: 22px;
height: 14px;
}
/*.themesflat-top .content-left ul.language-chooser li:nth-child(2) a img{
height: 16px;
}*/
</style>
<style type="text/css">
.popup #myModal2{
z-index: 99999;
}
</style><div class="themesflat_header_wrap header-style1" data-header_style="header-style1">
<!-- Header -->
<header id="header" class="header header-style1" >
<div class="container nav">
<div class="row">
<div class="col-md-12">
<div class="header-wrap clearfix">
<div id="logo" class="logo" >                  
<a href="/en/"  title="Vinsofts JSC &#8211; Vietnam IT Outsourcing Company">
<img class="site-logo"  src="/wp-content/uploads/2019/06/Logo-Vinsofts.png" alt="Vinsofts JSC &#8211; Vietnam IT Outsourcing Company"  data-retina="/wp-content/uploads/2019/06/Logo-Vinsofts.png" />
</a>
</div>
<div class="show-search">
<a href="#"><i class="fa fa-search"></i></a>         
</div> 
<div class="nav-wrap">
<div class="btn-menu">
<span></span>
</div><!-- //mobile menu button -->
<nav id="mainnav" class="mainnav" role="navigation">
<ul id="menu-main" class="menu"><li id="menu-item-2167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2167"><a href="/en/about-us/">About Us</a>
<ul class="sub-menu">
<li id="menu-item-3792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3792"><a href="/en/about-us/">Company Overview</a></li>
<li id="menu-item-3789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3789"><a href="/en/our-history/">Our History</a></li>
<li id="menu-item-3790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3790"><a href="/en/our-partner/">Our Partners</a></li>
<li id="menu-item-3791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3791"><a href="/en/our-team/">Our Team</a></li>
<li id="menu-item-6972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6972"><a href="/en/profile-vinsofts/">Company Profile (pdf)</a></li>
<li id="menu-item-6108" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6108"><a href="/en/cat/company-activities/">Company Activities</a></li>
</ul>
</li>
<li id="menu-item-2994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2994"><a href="/en/services/">Services</a>
<ul class="sub-menu">
<li id="menu-item-2933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2933"><a href="/en/mobile-application-development/">Mobile App Development</a></li>
<li id="menu-item-2934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2934"><a href="/en/web-application-development/">Website Development</a></li>
<li id="menu-item-2932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2932"><a href="/en/custom-software-development/">Custom Software Development</a></li>
<li id="menu-item-2935" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2935"><a href="/en/blockchain-application-development/">Blockchain Development</a></li>
<li id="menu-item-2943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2943"><a href="/en/software-testing-and-qa-services/">Software Testing and QA Services</a></li>
<li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="/en/automated-testing/">Automated Testing</a></li>
<li id="menu-item-2942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2942"><a href="/en/offshore-development-center/">Offshore Development Center</a></li>
</ul>
</li>
<li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3202"><a href="/en/case-study/">Case Studies</a></li>
<li id="menu-item-4112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4112"><a href="/en/jobs">Careers</a>
<ul class="sub-menu">
<li id="menu-item-4912" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4912"><a href="/en/job_cats/mobile-development-2/">Mobile Development</a></li>
<li id="menu-item-4911" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4911"><a href="/en/job_cats/web-development-2/">Web Development</a></li>
<li id="menu-item-4913" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4913"><a href="/en/job_cats/blockchain-development-2/">Blockchain Development</a></li>
<li id="menu-item-4810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a href="/en/apply-for-job/">Apply for a job</a></li>
</ul>
</li>
<li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="/en/news/">News</a></li>
<li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="/en/contact-us/">Contact Us</a></li>
</ul>        <div class="info-top-right border-left" style="display: none">
<a class="appoinment" href="#" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Get Quote</a>
<a class="hotline-header" href="tel:(+84) 0247 1080 285">
<i class="fa fa-phone" aria-hidden="true"></i> <span>(+84) 0247 1080 285</span>
</a>
<i></i>
</div>
</nav><!-- #site-navigation -->  
</div><!-- /.nav-wrap -->                                
</div><!-- /.header-wrap -->
<div class="submenu top-search widget_search">
<form role="search" method="get" class="search-form" action="/en/">
<label>
<span class="screen-reader-text">Search for:</span>
<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
</label>
<input type="submit" class="search-submit" value="Search" />
</form>                </div> 
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->    
</header><!-- /.header --></div> 	<!-- Page Title -->
<!-- Page title -->
<div class="page-title">
<div class="overlay"></div>   
<div class="container"> 
<div class="row">
<div class="col-md-12 page-title-container">
<div class="breadcrumb-trail breadcrumbs">
<span class="trail-browse"></span> <span class="trail-begin"><a href="/en" title="Vinsofts JSC - Vietnam IT Outsourcing Company">Home</a></span>
<span class="sep">&gt;</span> <span class="trail-end">Terms of service of Astra Network</span>
</div> 
<h1>Terms of service of Astra Network</h1>            </div><!-- /.col-md-12 -->  
</div><!-- /.row -->  
</div><!-- /.container -->                      
</div><!-- /.page-title --> 	
<div id="content" class="page-wrap sidebar-left">
<div class="container content-wrapper">
<div class="row">
<div class="col-md-12">
<div id="primary" class="content-area-full-width">
<main id="main" class="site-main" role="main">
<p><strong>Terms of service of Astra Network</strong></p>
<p>Welcome to Astra Network!</p>
<p>Astra is the world’s leading social networking platform based on blockchain for tourism. Astra aspires to build a community of dynamic, creative, travel-loving people who keep up with innovations; connect individuals together and with service providers; as well as support the development of businesses in the Astra ecosystem. To use Astra&#8217;s services and become a member of the global travel-loving community, please take a look at Astra’s Terms of service. These terms will govern all your activities on Astra and how you use all features, services and technologies we provide.</p>
<p>Astra offers you complimentary travel experience on an online platform, where each touch of your hand can open a new world with different destinations, cuisine, and cultures. Astra believes user contributions makes the social network so attractive. As a result, users not only don&#8217;t have to pay to use Astra&#8217;s services but can also receive rewards equivalent to the value of the content they build and share on our Social networking platform.</p>
<p>In return, to maintain the community and the platform, we use resources that are the costs businesses and organizations pay to advertise their products and services on Astra. Those businesses and organizations mentioned above are all partners in Astra’s ecosystem, which are travel service providers. By joining Astra, you allow us to display these advertisements on your New Feeling board. We commit ourselves to giving quality, well-censored, and verified advertisements to provide you with exciting choices for your future travel needs. We use the data collected about your behavior and interactions on Astra to decide what advertising content is allowed to appear on your New Feeling board.</p>
<p>We do not sell your data to third parties (businesses, organizations, agencies that use advertising services) nor disclose your personally identifiable information (name, email address, phone numbers, and other contact information) with the above units unless you permit. Instead, when buying advertisements, third parties are responsible for providing us with information about the target groups, occupations, ages, interests they want to reach. We will use our algorithms to show their advertisements to users who are currently interested in and in need of the type of products and services they offer. We will evaluate the effectiveness of advertisements and inform businesses and organizations to let them know how Astra users respond to their content.</p>
<ol>
<li><strong>The services we provide and our features</strong></li>
</ol>
<p>Astra builds a community for people who love and have the desire to learn about travel, where the information is verified, rich in reliability and experience is exchanged, thereby inspiring movements. To accomplish these goals, we offer our users the following products and services:</p>
<ul>
<li>Connectivity: Astra connects you with the individuals and organizations you interested in and with the community of travel lovers around the world. Astra tries to bridge the gap between users and travel service providers, create favorable conditions for service providers to listen to customer feedback, understand their needs and endeavor to bring them the best experience. With that circle of connections, we believe that Astra is a model of social networking platform that will bring a lot of value to the community &#8211; “Tourism for Connection”. For travel service providers, this is a platform for you to connect with clients through personalized and easy-to-interact information channels. For users, travel is no longer a lonely journey; it is a journey of connection and extension. At Astra, we have an idea that an exciting journey must be an inspiring journey for many others.</li>
<li>Share: Astra builds a platform for users to share travel experiences freely and easily. You just visited a famous destination last week, marveled at the cuisine, amused by the interesting experience yet still not satisfied with the service’s quality or the cost? Do not hesitate to share your story with Astra’s  travel community to receive high-value rewards and credibility. At Astra, we promote equal rights among all users by awarding them all based on the value that users contribute through their personal experience.</li>
<li>Reward: Engaging in Astra, you will receive rewards equivalent to the value you contribute when building and posting authentic, interesting content about tourism. Astra’s content measurement tool is Proof of Rating. The calculation formula includes parameters on the number of types of interaction, the reputation index of authors and interactors, and parameters on content type. In Astra ecosystem, the reward unit is Astra Point.The sooner users engage in content creation, the higher the rank and reputation and the greater the value they will receive. The rewards that users receive will be redeemable for products and services depending on users’ needs.</li>
<li>Travel knowledge: Astra builds a centralized and unlimited travel experience store to help users accumulate knowledge for future travel purposes. By joining Astra, you will have the opportunity to reach out to a large tourism community and enrich your knowledge with the experiences that others share. Astra team has also developed a set of information filtering tools to ensure users provide authentic and useful travel experiences, minimizing false information.</li>
<li>Buy/Services: Users have the opportunity to receive great deals from travel service providers such as vouchers at hotels or resorts, discounts when dining at high-class restaurants. Besides, users can also purchase these offers by using the points that have been accumulated in personal accounts at Astra when participating in content creation.</li>
<li>Marketing: Astra builds business accounts with effective marketing and support tools for organizations to reach and promote their image to the public, attracting potential customers. In an well-oriented market for tourism as Astra, travel service providers can reduce the time and cost of spreading their image to customers and partners. Service providers can also listen to customer feedback, improve quality promptly and impress potential customers with your professionality..</li>
<li>Advanced technologies: We use and develop advanced technologies such as blockchain, artificial intelligence (AI), and machine learning systems to ensure Astra users can experience the most intelligent and convenient travel services. We have developed a Proof of Rating tool to measure the value of content posted by users, showing respect for users and transparency through an attractive reward payment mechanism. Another set of tools we use is Proof of Trust that works as an information filtering layer, helps minimize false and fraudulent reviews, protects the community from harmful factors, misleading and divisive ideas.</li>
<li>Services improvement: Through users’ behavior and interaction data, we analyze and make new improvements to help users have a better experience. Simultaneously, we also conduct surveys and tests to find out the needs and concerns of users about new features.</li>
</ul>
<ol start="2">
<li><strong>How we maintain the service </strong></li>
</ol>
<p>Your experience on Astra is completely free of charge. You do not have to pay any fee to use Astra’s services and features. Furthermore, you have the opportunity to receive rewards with a high redemption value. In return, we operate and maintain our services by charging fees to agencies wanting to place their advertisements on Astra. Travel providers and businesses can collaborate with Astra in three methods: reaching ads (ads with high coverage and don’t distinguish users based on demographic characteristics), display ads (ads that target specific groups with high level of accuracy) and recommendation ads (ads that recommend good and excellent businesses &amp; organizations to users).</p>
<p>When purchasing our advertising packages, businesses &amp; organizations are responsible for providing information about the target audience they want to reach. We use the data collected about your behavior and interactions on Astra to decide what advertising content is allowed to appear on your New Feeling board. We do not sell your data nor disclose the contact information you provide us to third parties. We respect users&#8217; privacy and commit to providing enjoyable experiences.</p>
<ol start="3">
<li><strong>Your commitment to Astra and our community</strong></li>
</ol>
<p>We provide these services to you and others to advance our mission. In return, we need you to perform the following commitments:</p>
<p><strong>Who can use Astra</strong></p>
<p>We want our services and products to be as comprehensive as possible but still ensure safety, security, and compliance with laws. Therefore, Astra needs your commitment to obey with some of the following restrictions to become a member of Astra community.</p>
<ul>
<li>You must use your real name, providing accurate information about yourself.</li>
<li>Create one account only (your own) and use the timeline for personal purposes.</li>
<li>Do not share your password, give others access to your Astra account, or transfer your account to anyone else without our permission.</li>
</ul>
<p>We always try to make Astra Network widely available to everyone. You will be able to use Astra if:</p>
<ul>
<li>You are at least 13 years old (or the minimum legal age in your country to use Astra).</li>
<li>You are not a convicted sex offender</li>
<li>You have not been previously disabled your account for violating the law or for violating any of our policies.</li>
<li>You are not prohibited from receiving our services, products according to applicable laws or not prohibited from making payments related to the service if you are on the list of the denied party being applied.</li>
</ul>
<p><strong>Things you can share and do on Astra </strong></p>
<p>We want people to use Astra to express themselves and honestly evaluate travel destinations openly and safely, and we respect freedom of expression and respect for users. We hope and continuously strive to make Astra become an interesting community space and inspire everyone. Therefore, you agree not to engage in the behaviors described below (or to facilitate or assist others in the conduct of those acts):</p>
<ul>
<li>You must not create, post or share any content which against the Socialist Republic of Vietnam; cause harm to national security, social order, and safety; threat the great unity bloc of the entire people; propaganda of war and conflict; cause hatred and conflict among ethnic groups, and religions; propagate and promote violence, lust, depravity, crime, social evils, superstition; undermine the nation&#8217;s fine customs and traditions; threat erosion of national cultural identity; disclose state secrets, military secrets, security, economy, foreign affairs and important confidential information of the State of the Socialist Republic of Vietnam.</li>
<li>You must not perform acts that are contrary to the laws of the Socialist Republic of Vietnam, for example: posting information on buying and selling banned goods under the laws of the Socialist Republic of Vietnam on Astra, spreading information about anti-state sabotage, smearing ethnicity, spreading all forms of prohibited information by the laws and regulations of the State.</li>
<li>You must not spread on the forum the content, links, posts, libelous, defame others, offend and infringe on the rights, prestige, and privacy of others. You must not create and post content that misrepresents, slander, or offends the reputation of the organization; honor and dignity of citizens; misleading, discriminatory, or deceptive.</li>
<li>You must not impersonate another person or provide inaccurate information. You do not have to disclose your identity on Astra, but you must provide us with accurate and up-to-date information (including registration information). Besides, you cannot impersonate another person and create accounts for others unless they give permission.</li>
<li>You must not disrupt, sabotage equipment systems and obstruct illegal management, provision, and use of Internet services and information on the Internet. You must not steal and use passwords illegally, cryptographic keys and private information of organizations and individuals on the Internet. It is strictly forbidden to send or upload information, software that attaches viruses or other harmful components or use any tools that interfere and harm Astra&#8217;s system.</li>
</ul>
<p>We may remove or restrict access to content that violates these terms.</p>
<p><strong>The right you give us</strong></p>
<p>We need you to permit some rights to provide the service:</p>
<ol>
<li>The right to use your content:</li>
</ol>
<ul>
<li>Some content that you share or upload, such as photos or videos, may be protected by intellectual property rìghts.</li>
<li>You have intellectual property rights (such as copyright or trademark) in any content that you create and share on the Astra. Nothing in these Terms takes away the rights you have to your content.</li>
<li>However, to provide the service, you need to give us some legal rights (called a &#8220;license&#8221;) to use that content. This is only to provide and improve our products and services described in Part 1 above. Specifically, when you share, post or upload content that is protected by intellectual property rights on or in connection with our products, you grant us a non-exclusive, transferable, sub-licensable, royalty-free and worldwide license to store, use, distribute, modify, run, copy, perform or publicly display, translate and create derivative works from your content. You can delete each or all content by deleting your account. This license will expire when your content is removed from our system.</li>
</ul>
<ol>
<li>Right to use your name, avatar, and information about your actions with sponsored ads and content: You authorize us to use your name, avatar, and information about your actions on Astra next to or relating to advertisements, offers and other sponsored content displayed by us on our products, without compensation to you. For example, we can let your friends know that you are interested in an advertisement event or have liked a Page due to a brand (paid us to display their ad on Astra) create. Such ads will only be shown to people who have your permission to see your actions on Astra. You can learn more about advertising settings and options.</li>
</ol>
<ol>
<li>Right to update software: Your download or use our software means that you give us permission to download and install software updates.</li>
<li>Right to block access: We reserve the right to prohibit or deny users&#8217; access to Astra or any part of the website immediately and without prior announcement if we believe the user has violated any terms, or the prohibition of access comes from our identification when we believe that such refusal is appropriate and necessary in our jurisdiction.</li>
<li><strong> Additional provisions </strong></li>
</ol>
<p>We are constantly working to improve our services and develop new features to create better products for you and our community. As a result, we need to update these Terms sometimes to accurately reflect our services and practices.We will only make changes if these terms are no longer relevant or incomplete and only if the changes are reasonable as well as appropriate consideration of your interests.</p>
<p>We will notify you (by email or through our service, products) at least 30 days before making changes to these Terms, and allow you to review before they go into effect unless we have to make changes as required by law. After any updated Terms become effective, you will be bound by them if you continue to use our service, products.</p>
<p>We hope you will continue using our Products. However, if you do not agree with the updated Terms and no longer want to join the Astra community, you can delete your account at any time.</p>
<p><strong>Account suspension or termination</strong></p>
<p>We want Astra to become a place where people feel safe and welcome to express themselves as well as share their thoughts and ideas, travel experiences and capture emotions, hit the most honest assessment of travel destinations.</p>
<p>If it is determined that you clearly, severely or repeatedly violate our Terms or Policies, we may suspend or permanently disable access to your account. We may also suspend or disable an account if you repeatedly infringe on the intellectual property rights of others or if we have to do so for legal reasons.</p>
<p>When taking the above action, we will let you know and explain any options you have to make a reconsideration request, unless this may put us or others liable; harm Astra community; influence / interfere with the integrity or operation of any of our services, systems or products; or when we are restricted due to technical limitations; or when we are prohibited from doing this for legal reasons.</p>
<p>You can learn more about what you can do if your account is disabled and how to contact us if you think we have disabled your account by mistake.</p>
<p><strong>Limitation of liability</strong></p>
<p>Astra Network always strives to provide users with the most convenient products and services, however, we cannot guarantee that our services and products are always safe, secure and error-free, or they will operate without interruption, delay or lack of omission. At the same time, we do not control or direct the actions or statements of people and others. We are also not responsible for their actions or conduct (whether online or offline) or any content they share (including offensive, inappropriate, obscene, unlawful content and other objectionable content). Our liability shall be limited to the fullest extent permitted by applicable law, and under no circumstance will we be liable to you for any lost profits, revenues, information, or data</p>
</main><!-- #main -->
</div><!-- #primary -->
</div>

</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- #content -->
<!-- Footer -->
<div class="footer_background">
<footer class="footer">      
<div class="container">
<div class="row"> 
<div class="footer-widgets">
<div class="col-md-4 col-sm-6">
<div id="text-2" class="widget widget_text">			<div class="textwidget"><p><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-full wp-image-8330" data-wpfc-original-src="/wp-content/uploads/2019/06/LOGO-FOOTER-min.png" alt="blank" width="169" height="91" /></p>
</div>
</div><div id="text-5" class="widget widget_text">			<div class="textwidget"><p>With over 70 employees and 10+ years experience in software development for clients from all over the world, we are proud of being a top software outsourcing company in Vietnam. We always commit to deliver highest quality of services, to be a trustful partner of any company.</p>
</div>
</div>                        </div>
<div class="col-md-2 col-sm-6">
<div id="text-10" class="widget widget_text"><h4 class="widget-title">FIND US</h4>			<div class="textwidget"></div>
</div><div id="widget_themesflat_socials-5" class="widget widget_themesflat_socials">
<ul class="themesflat-shortcode-socials">
<li class="facebook">
<a class="title" href="https://www.facebook.com/vinsoftsjsc" target="_blank" rel="alternate" title="Facebook">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="https://www.youtube.com/channel/UC33N-fg42Gkp1Fcin5Zp5EA/" target="_blank" rel="alternate" title="Youtube">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="https://www.linkedin.com/company/vinsofts/" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
</div><div id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><ul class="themesflat-shortcode-socials">
<li style="width: 115%;">
<a href="https://goo.gl/maps/UksWXm7gqr62" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>Google map</a>
</li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title">Our Services</h4><div class="menu-footer_service-container"><ul id="menu-footer_service" class="menu"><li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2902"><a href="/en/mobile-application-development/">Mobile App Development</a></li>
<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903"><a href="/en/web-application-development/">Website Development</a></li>
<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2901"><a href="/en/custom-software-development/">Custom Software Development</a></li>
<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2904"><a href="/en/blockchain-application-development/">Blockchain Development</a></li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-14" class="widget widget_nav_menu"><h4 class="widget-title">Information</h4><div class="menu-menu-footer-right-container"><ul id="menu-menu-footer-right" class="menu"><li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3559"><a href="/en/contact-us/">Contact Us</a></li>
<li id="menu-item-4013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4013"><a href="/en/jobs/">Recruitment</a></li>
</ul></div></div><div id="custom_html-6" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="cus_dmca">
<a href="//www.dmca.com/Protection/Status.aspx?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/dmca_protected_16_120.png?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559"  alt="DMCA.com Protection Status" /></a>
</div>
<style type="text/css">
.cus_dmca a img {
padding: 10px 0px 0px 48px;
}
</style></div></div>                        </div>
</div><!-- /.footer-widgets -->           
</div><!-- /.row -->    
</div><!-- /.container -->   
</footer>
<div class="content-register-footer">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="left">  
<!-- English content here -->
<div class="textwidget">
<p>Company: Vinsofts Joint Stock Company</p>
<!-- <p>Head Office: 8th floor, Sannam building, Duy Tan street, Dich Vong Hau ward, Cau Giay district, Hanoi, Vietnam</p> -->
<p>Office in Hanoi: 5th floor, No. 8 Phan Van Truong street, Dich Vong Hau ward, Cau Giay district, Hanoi, Vietnam</p>
<!-- <p>Office in Ho Chi Minh City: Unit P5-16.B Charmington La Pointe, No. 181 Cao Thang street, Ward 12, District 10, Ho Chi Minh City, Vietnam</p> -->
<p>Office in Ho Chi Minh City: P516 Block C Charmington La Pointe, No. 181 Cao Thang street, Ward 12, District 10, Ho Chi Minh City, Vietnam</p>
<p>Tel:&nbsp;<a href="tel:0462593148">04.6259.3148</a>&nbsp;– Hotlline:&nbsp;<a href="tel:0961678247">0961.678.247</a>&nbsp;– Email:&nbsp;<a href="mailto:info@vinsofts.com">info@vinsofts.com</a></p>
<p>Business License No. 0107354530 issued by Department of Planning and Investment on 14/03/2016</p>
</div>
</div>
</div>
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="right">
<!-- English content here -->
<div class="textwidget">
<p>Please read the <a href="/en/privacy-policy">Privacy Policy</a> and <a href="/en/terms-of-use">Terms of Use</a> carefully!</p>
<p>Website has been notified and accepted by the Department of E-commerce and Information Technology, Ministry of Industry and Trade.</p>
<p><a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43043"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-medium wp-image-5318" data-wpfc-original-src="/wp-content/uploads/2018/10/vinsofts-dathongbao-300x114.png" alt="blank" width="300" height="114" /></a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Bottom -->
<div class="bottom">
<div class="container">           
<div class="row">
<div class="col-md-12">
<div class="copyright">                        
<p>Copyright <i class="fa fa-copyright"></i> 2012 - 2019
<a href="#">Vinsofts JSC</a>. All rights reserved.</p>                        </div>
<!-- Go Top -->
<a class="go-top show">
<i class="fa fa-chevron-up"></i>
</a>
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->
</div> 
<div id="tawk-to-vinsofts">
<!--Start of Tawk.to Script-->
<!--End of Tawk.to Script-->
</div>  
</div> <!-- Footer Background Image -->
</div><!-- /#boxed -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.googletagmanager.com/ns.html?id=GTM-W8XSN4W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <link rel='stylesheet' id='fo-child-css'  href='/wp-content/themes/fo-child/common/css/custom-css.css?version=126' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/kl040ku2/4b9n.css" media="all"/>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&#038;render=explicit'></script>
<noscript id="wpfc-google-fonts"><link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
</noscript>
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/folii5hc/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7lob9sfs/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/d6ougkxu/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/eqjz612f/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lpm2zzii/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/fte2l3l2/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mkx6gd8y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mknr8988/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/zdl5aix/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7xsi43ue/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/976glhgy/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1eotoqvg/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/e726dwxx/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/llhj9sk8/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/erkz11cs/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1281u0q0/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lll4wyip/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/dq6j6xeh/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/869bu30y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/47mj14y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/qa893n0m/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/es8b11eb/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/knvqh2vr/4b9o.js'></script> -->
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"fadeb5bb67","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b7f8308f31d0f771d84184a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/en\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script>
setTimeout(function(){
// $('.popup #myModal2').slideDown();
jQuery('.popup #myModal2').show();
jQuery('#myModal2').modal({
backdrop: 'static',
keyboard: false
});
// $('body').addClass('modal-open');
// $('body').append('<div class="modal-backdrop fade in"></div>');
// $('button.close').click(function(){
//     $('#myModal2').slideUp();
//     $('body').removeClass('modal-open');
//     $('div.modal-backdrop').remove();
// })
}, 50000);
</script>
<script type='text/javascript'>
//<![CDATA[
// JavaScript Document
// var message="NoRightClicking";
// function defeatIE() {
//   if (document.all) {(message);return false;}
// }
// function defeatNS(e) {
//   if (document.layers||(document.getElementById&&!document.all))
//   { if (e.which==2||e.which==3) {(message);return false;}}
// }
// if (document.layers) {
//   document.captureEvents(Event.MOUSEDOWN);
//   document.onmousedown=defeatNS;
// } else{
//   document.onmouseup=defeatNS;document.oncontextmenu=defeatIE;
// }
// document.oncontextmenu=new Function("return false")
// //]]>
// // enable to override webpacks publicPath
// var webpackPublicPath = '/';
// jQuery(document).keydown(function(event) {
//     if (
//       event.keyCode === 123 ||
//       (event.ctrlKey && event.shiftKey && event.keyCode === 67) ||
//       (event.ctrlKey && event.keyCode === 85)
//     ) {
//       return false;
//     }
// });
// document.onselectstart = new Function('return false');
// if (window.sidebar) {
//     document.onmousedown = false;
//     document.onclick = true;
// }
</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script defer src='/wp-content/cache/wpfc-minified/qjscumg/4bl9.js' type="text/javascript"></script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
</body>
</html><!-- WP Fastest Cache file was created in 15.028029203415 seconds, on 29-07-20 21:13:50 -->