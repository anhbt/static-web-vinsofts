<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="/xmlrpc.php">
<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W8XSN4W');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125473191-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-125473191-1');
</script>
<title>Blockchain Development - Vinsofts JSC</title>
<!-- This site is optimized with the Yoast SEO Premium plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
<meta name="description" content="Due to their high security, Blockchain applications are rapidly getting adopted by the financial industry and many central banks."/>
<link rel="canonical" href="/en/blockchain-application-development/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Blockchain Development - Vinsofts JSC" />
<meta property="og:description" content="Due to their high security, Blockchain applications are rapidly getting adopted by the financial industry and many central banks." />
<meta property="og:url" content="/en/blockchain-application-development/" />
<meta property="og:site_name" content="Vinsofts JSC - Vietnam IT Outsourcing Company" />
<meta property="article:publisher" content="https://www.facebook.com/vinsoftsjsc" />
<meta property="fb:app_id" content="1096476827181655" />
<meta property="og:image" content="/wp-content/uploads/2018/08/all-vi.jpg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Due to their high security, Blockchain applications are rapidly getting adopted by the financial industry and many central banks." />
<meta name="twitter:title" content="Blockchain Development - Vinsofts JSC" />
<meta name="twitter:image" content="/wp-content/uploads/2018/08/all-vi.jpg" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"\/en\/","name":"Vinsofts JSC - Vietnam IT Outsourcing Company","potentialAction":{"@type":"SearchAction","target":"\/en\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO Premium plugin. -->
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel="alternate" type="application/rss+xml" title="Vinsofts JSC - Vietnam IT Outsourcing Company &raquo; Feed" href="/en/feed/" />
<link rel="alternate" type="application/rss+xml" title="Vinsofts JSC - Vietnam IT Outsourcing Company &raquo; Comments Feed" href="/en/comments/feed/" />
<!-- <link rel='stylesheet' id='vc_extend_shortcode-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes.css?version=6565' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_extend_style-css'  href='/wp-content/plugins/themesflat/assets/css/shortcodes-3rd.css?version=6889' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='bootstrap-css'  href='/wp-content/themes/fo/css/bootstrap.css?version=2729' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?version=6996' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qjpyrk45/4b9o.css" media="all"/>
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<!-- <link rel='stylesheet' id='qts_front_styles-css'  href='/wp-content/plugins/qtranslate-slug/assets/css/qts-default.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_main-css'  href='/wp-content/themes/fo/css/main.css?version=7517' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-style-css'  href='/wp-content/themes/fo-child/style.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='font-fontawesome-css'  href='/wp-content/themes/fo/css/font-awesome.css?version=519' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-ionicons-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=7910' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_simpleline-css-css'  href='/wp-content/themes/fo/css/simple-line-icons.css?version=3859' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_ion_icon-css'  href='/wp-content/themes/fo/css/ionicons.min.css?version=2037' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/9jminkn5/4b9o.css" media="all"/>
<!--[if lte IE 9]><link rel='stylesheet' id='ie9-css'  href='/wp-content/themes/fo/css/ie.css?version=4710' type='text/css' media='all' /><![endif]-->
<!-- <link rel='stylesheet' id='themesflat_logo-css'  href='/wp-content/themes/fo/css/logo.css?version=9737' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_animate-css'  href='/wp-content/themes/fo/css/animate.css?version=1401' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat_responsive-css'  href='/wp-content/themes/fo/css/responsive.css?version=6824' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='themesflat-inline-css-css'  href='/wp-content/themes/fo/css/inline-css.css?version=2434' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/mmoo7qcx/4b9o.css" media="all"/>
<style id='themesflat-inline-css-inline-css' type='text/css'>
.logo{padding-top:24px; padding-left:15px; }
.footer{padding-top:64px; padding-bottom:74px; }
.page-title{padding-top:21px; }
.logo img, .logo svg { height:70px; }
.page-title .overlay{ background: #ffffff}.page-title {background: url() center /cover no-repeat;}.page-title h1 {color:#21536c!important;
}
.breadcrumbs span,.breadcrumbs span a, .breadcrumbs a {color:#595959!important;
}
body,button,input,select,textarea { font-family:Poppins ; }
body,button,input,select,textarea { font-weight:400;}
body,button,input,select,textarea { font-style:normal; }
body,button,input,select,textarea { font-size:14px; }
body,button,input,select,textarea { line-height:25px ; }
h1,h2,h3,h5,h6 { font-family:Poppins;}
h1,h2,h3,h4,h5,h6 { font-weight:600;}
h1,h2,h3,h4,h5,h6  { font-style:normal; }
#mainnav > ul > li > a { font-family:Poppins;}
#mainnav > ul > li > a { font-weight:600;}
#mainnav > ul > li > a  { font-style:normal; }
#mainnav ul li a { font-size:14px;}
#mainnav > ul > li > a { line_height100px;}
h1 { font-size:32px; }
h2 { font-size:25px; }
h3 { font-size:22px; }
h4 { font-size:18px; }
h5 { font-size:15px; }
h6 { font-size:14px; }
.iconbox .box-header .box-icon span,a:hover, a:focus,.portfolio-filter li a:hover, .portfolio-filter li.active a,.themesflat-portfolio .item .category-post a,.color_theme,.widget ul li a:hover,.footer-widgets ul li a:hover,.footer a:hover,.themesflat-top a:hover,.themesflat-portfolio .portfolio-container.grid2 .title-post a:hover,.themesflat-button.no-background, .themesflat-button.blog-list-small,.show-search a i:hover,.widget.widget_categories ul li a:hover,.breadcrumbs span a:hover, .breadcrumbs a:hover,.comment-list-wrap .comment-reply-link,.portfolio-single .content-portfolio-detail h3,.portfolio-single .content-portfolio-detail ul li:before, .themesflat-list-star li:before, .themesflat-list li:before,.navigation.posts-navigation .nav-links li a .meta-nav,.testimonial-sliders.style3 .author-name a,ul.iconlist .list-title a:hover,.themesflat_iconbox .iconbox-icon .icon span,.bottom .copyright a,.top_bar2 .wrap-header-content ul li i { color:#337493;}
#Ellipse_7 circle,.testimonial-sliders .logo_svg path { fill:#337493;}
.info-top-right a.appoinment, .wrap-header-content a.appoinment,button, input[type=button], input[type=reset], input[type=submit],.go-top:hover,.portfolio-filter.filter-2 li a:hover, .portfolio-filter.filter-2 li.active a,.themesflat-socials li a:hover, .entry-footer .social-share-article ul li a:hover,.themesflat-button,.featured-post.blog-slider .flex-prev, .featured-post.blog-slider .flex-next,mark, ins,#themesflat-portfolio-carousel ul.flex-direction-nav li a, .flex-direction-nav li a,.navigation.posts-navigation .nav-links li a:after,.title_related_portfolio:after,.navigation.loadmore a:hover,.owl-theme .owl-controls .owl-nav [class*=owl-],.widget.widget_tag_cloud .tagcloud a,.btn-menu:before, .btn-menu:after, .btn-menu span,.themesflat_counter.style2 .themesflat_counter-icon .icon,widget a.appoinment,.themesflat_imagebox .imagebox-image:after,.nav-widget a.appoinment { background-color:#337493; }
.themesflat_btnslider:not(:hover) {
background-color:#337493!important;
}
.loading-effect-2 > span, .loading-effect-2 > span:before, .loading-effect-2 > span:after,textarea:focus, input[type=text]:focus, input[type=password]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=date]:focus, input[type=month]:focus, input[type=time]:focus, input[type=week]:focus, input[type=number]:focus, input[type=email]:focus, input[type=url]:focus, input[type=search]:focus, input[type=tel]:focus, input[type=color]:focus,select:focus,.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span,.navigation.loadmore a:hover { border-color:#337493}
{
border-color:#337493!important;
}
{
color: #fff !important;
}
{
background-color: #2e363a !important;
}
#Financial_Occult text,#F__x26__O tspan { fill:#595959;}
body { color:#595959}
a,.portfolio-filter li a,.themesflat-portfolio .item .category-post a:hover,.title-section .title,ul.iconlist .list-title a,.breadcrumbs span a, .breadcrumbs a,.breadcrumbs span,h1, h2, h3, h4, h5, h6,strong,.testimonial-content blockquote,.testimonial-content .author-info,.sidebar .widget ul li a,.themesflat_counter.style2 .themesflat_counter-content-right,.themesflat_counter.style2 .themesflat_counter-content-left,.title_related_portfolio,.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation .current, .page-links a:hover, .page-links a:focus,.widget_search .search-form input[type=search],.entry-meta ul,.entry-meta ul.meta-right,.entry-footer strong,.widget.widget_archive ul li:before, .widget.widget_categories ul li:before, .widget.widget_recent_entries ul li:before { color:#595959}
.owl-theme .owl-dots .owl-dot span,.widget .widget-title:after, .widget .widget-title:before,ul.iconlist li.circle:before { background-color:#595959}
.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation:not(.loadmore) .current, .page-links a:hover, .page-links a:focus, .page-links > span { border-color:#595959}
.themesflat-top { background-color:#21536c ; } 
.themesflat-top .border-left:before, .themesflat-widget-languages:before, .themesflat-top .border-right:after { background-color: rgba(255,255,255,0.2);}.themesflat-top,.info-top-right,.themesflat-top a, .themesflat-top .themesflat-socials li a { color:#ffffff ;} 
.themesflat_header_wrap.header-style1,.nav.header-style2,.themesflat_header_wrap.header-style3,.nav.header-style4,.header.widget-header .nav { background-color:#fff;}
#mainnav > ul > li > a { color:#595959;}
#mainnav > ul > li > a:hover,#mainnav > ul > li.current-menu-item > a { color:#377493 !important;}
#mainnav ul.sub-menu > li > a { color:#ffffff!important;}
#mainnav ul.sub-menu { background-color:#377493;}
#mainnav ul.sub-menu > li > a:hover { background-color:#aac9ce!important;}
#mainnav ul.sub-menu > li { border-color:#dde9eb!important;}
.footer_background:before { background-color:#21536c;}
.footer a, .footer, .themesflat-before-footer .custom-info > div,.footer-widgets ul li a,.footer-widgets .company-description p { color:#f9f9f9;}
.bottom { background-color:#337493;}
.bottom .copyright p,.bottom #menu-bottom li a { color:#e5e5e5;}
.white #Financial_Occult text,.white #F__x26__O tspan {
fill: #fff; }test_filter_render
</style>
<!-- <link rel='stylesheet' id='js_composer_front-css'  href='/wp-content/plugins/js_composer/assets/css/js_composer.min.css?version=6709' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/qhynyhyt/4b9n.css" media="all"/>
<link rel='https://api.w.org/' href='/en/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.8" />
<link rel='shortlink' href='/en/?p=2882' />
<link rel="alternate" type="application/json+oembed" href="/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fen%2Fblockchain-application-development%2F" />
<link rel="alternate" type="text/xml+oembed" href="/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2F%2Fen%2Fblockchain-application-development%2F&#038;format=xml" />
<meta name="generator" content="qTranslate-X 3.4.6.8" />
<link hreflang="x-default" href="/phat-trien-ung-dung-blockchain/" rel="alternate" />
<link hreflang="vi" href="/phat-trien-ung-dung-blockchain/" rel="alternate" />
<link hreflang="en" href="/en/blockchain-application-development/" rel="alternate" />
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?version=2246" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.6.3.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/10/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="/wp-content/uploads/2018/10/cropped-favicon-270x270.png" />
<style type="text/css" id="wp-custom-css">
.page-wrap.sidebar-left #primary.content-area {
/*     margin-top: -40px; */
}
.single-post .page-wrap.sidebar-left #primary.content-area {
margin-top: 0;
}
.themesflat-portfolio .item .featured-post {
text-align: center;
}
.themesflat-portfolio .item .featured-post img, .content-area.fullwidth .themesflat_iconbox.inline-left img {
height: 220px;
width: 100%;
object-fit: cover;
}
/* .content-area.fullwidth .themesflat_iconbox.transparent .title {
height: 55px;
display: flex;
align-items: center;
margin-top: 0;
}
.content-area.fullwidth .themesflat_iconbox.transparent .iconbox-content p:nth-child(2) {
height: 125px;
overflow: hidden;
}
*/
.header.header-sticky .wpmenucartli {
display:none !important;
}
.header.header-sticky #menu-main li:first-child {
display: inline-block !important;
}
#mainnav>ul>li {
margin-left: 25px !important;
}		</style>
<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1492683179903{padding-top: 14px !important;padding-bottom: 4px !important;}.vc_custom_1492683179903{padding-top: 14px !important;padding-bottom: 4px !important;}.vc_custom_1533714094222{margin-bottom: 48px !important;}.vc_custom_1533782130726{margin-bottom: 37px !important;}.vc_custom_1495612460535{padding-bottom: 62px !important;}.vc_custom_1533797734733{padding-bottom: 62px !important;}.vc_custom_1533782241723{background-color: #eeeeee !important;}.vc_custom_1533722665312{background-color: #eeeeee !important;}.vc_custom_1534410932454{padding-bottom: 37px !important;background-color: #eeeeee !important;}.vc_custom_1533722934965{background-color: #eeeeee !important;}.vc_custom_1533714064468{margin-bottom: 14px !important;padding-top: 17px !important;}.vc_custom_1492737448135{padding-top: 40px !important;}.vc_custom_1533714064468{margin-bottom: 14px !important;padding-top: 17px !important;}.vc_custom_1533714094222{margin-bottom: 48px !important;}.vc_custom_1536115941579{margin-bottom: 37px !important;}.vc_custom_1495612460535{padding-bottom: 62px !important;}.vc_custom_1533797734733{padding-bottom: 62px !important;}.vc_custom_1554178126148{background-color: #eeeeee !important;}.vc_custom_1554178136624{background-color: #eeeeee !important;}.vc_custom_1553850447802{padding-bottom: 37px !important;background-color: #eeeeee !important;}.vc_custom_1554178191857{background-color: #eeeeee !important;}.vc_custom_1537759372846{margin-bottom: 14px !important;padding-top: 17px !important;}.vc_custom_1492737448135{padding-top: 40px !important;}.vc_custom_1533714064468{margin-bottom: 14px !important;padding-top: 17px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
<script data-wpfc-render="false">var Wpfcll={s:[],osl:0,scroll:false,i:function(){Wpfcll.ss();window.addEventListener('load',function(){window.addEventListener("DOMSubtreeModified",function(e){Wpfcll.osl=Wpfcll.s.length;Wpfcll.ss();if(Wpfcll.s.length > Wpfcll.osl){Wpfcll.ls(false);}},false);Wpfcll.ls(true);});window.addEventListener('scroll',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('resize',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});window.addEventListener('click',function(){Wpfcll.scroll=true;Wpfcll.ls(false);});},c:function(e,pageload){var w=document.documentElement.clientHeight || body.clientHeight;var n=0;if(pageload){n=0;}else{n=(w > 800) ? 800:200;n=Wpfcll.scroll ? 800:n;}var er=e.getBoundingClientRect();var t=0;var p=e.parentNode;if(typeof p.getBoundingClientRect=="undefined"){var pr=false;}else{var pr=p.getBoundingClientRect();}if(er.x==0 && er.y==0){for(var i=0;i < 10;i++){if(p){if(pr.x==0 && pr.y==0){p=p.parentNode;if(typeof p.getBoundingClientRect=="undefined"){pr=false;}else{pr=p.getBoundingClientRect();}}else{t=pr.top;break;}}};}else{t=er.top;}if(w - t+n > 0){return true;}return false;},r:function(e,pageload){var s=this;var oc,ot;try{oc=e.getAttribute("data-wpfc-original-src");ot=e.getAttribute("data-wpfc-original-srcset");if(s.c(e,pageload)){if(oc || ot){if(e.tagName=="DIV" || e.tagName=="A"){e.style.backgroundImage="url("+oc+")";e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");}else{if(oc){e.setAttribute('src',oc);}if(ot){e.setAttribute('srcset',ot);}e.removeAttribute("data-wpfc-original-src");e.removeAttribute("data-wpfc-original-srcset");e.removeAttribute("onload");if(e.tagName=="IFRAME"){e.onload=function(){if(typeof window.jQuery !="undefined"){if(jQuery.fn.fitVids){jQuery(e).parent().fitVids({customSelector:"iframe[src]"});}}var s=e.getAttribute("src").match(/templates\/youtube\.html\#(.+)/);var y="https://www.youtube.com/embed/";if(s){try{var i=e.contentDocument || e.contentWindow;if(i.location.href=="about:blank"){e.setAttribute('src',y+s[1]);}}catch(err){e.setAttribute('src',y+s[1]);}}}}}}else{if(e.tagName=="NOSCRIPT"){if(jQuery(e).attr("data-type")=="wpfc"){e.removeAttribute("data-type");jQuery(e).after(jQuery(e).text());}}}}}catch(error){console.log(error);console.log("==>",e);}},ss:function(){var i=Array.prototype.slice.call(document.getElementsByTagName("img"));var f=Array.prototype.slice.call(document.getElementsByTagName("iframe"));var d=Array.prototype.slice.call(document.getElementsByTagName("div"));var a=Array.prototype.slice.call(document.getElementsByTagName("a"));var n=Array.prototype.slice.call(document.getElementsByTagName("noscript"));this.s=i.concat(f).concat(d).concat(a).concat(n);},ls:function(pageload){var s=this;[].forEach.call(s.s,function(e,index){s.r(e,pageload);});}};document.addEventListener('DOMContentLoaded',function(){wpfci();});function wpfci(){Wpfcll.i();}</script>
</head>
<body class="page-template-default page page-id-2882  has-topbar header_sticky wide sidebar-left bottom-center wpb-js-composer js-comp-ver-5.4.7 vc_responsive en">
<div class="themesflat-boxed">
<!-- Preloader -->
<div class="preloader">
<div class="clear-loading loading-effect-2">
<span></span>
</div>
</div>
<!-- Top -->
<div class="themesflat-top header-style1">    
<div class="container">
<div class="container-inside">
<div class="content-left">
<ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-chooser">
<li class="lang-vi"><a href="/vi/phat-trien-ung-dung-blockchain/" hreflang="vi" title="VI (vi)" class="qtranxs_image qtranxs_image_vi"><img src="/wp-content/plugins/qtranslate-x/flags/vn.png" alt="VI (vi)" /><span style="display:none">VI</span></a></li>
<li class="lang-en active"><a href="/en/blockchain-application-development/" hreflang="en" title="EN (en)" class="qtranxs_image qtranxs_image_en"><img src="/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN (en)" /><span style="display:none">EN</span></a></li>
</ul><div class="qtranxs_widget_end"></div>
<ul>
<li class="border-right">
<i class="fa fa-phone"></i><a href="tel:0247 1080 285" target="_top"> (+84) 0247 1080 285</a>     
</li>
<li>
<i class="fa fa-envelope"></i> <a href="mailto:info@vinsofts.com" target="_top">  info@vinsofts.com</a> 
</li>
</ul>	
</div><!-- /.col-md-7 -->
<div class="content-right">
<ul class="themesflat-socials">
<li class="facebook">
<a class="title" href="">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="/en/blockchain-application-development/linkedin.com/company/vinsofts" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
<div class="info-top-right border-left">
<span><i class="fa fa-question-circle"></i>Got a project idea?</span>
<a class="appoinment" href="#">GET QUOTE</a>
</div>            </div><!-- /.col-md-5 -->
</div><!-- /.container -->
</div><!-- /.container -->        
</div><!-- /.top -->
<div class="popup">
<!--Form contact-->
<div id="myModal2" class="modal fade in" role="dialog">
<div class="modal-dialog modal-lg">
<!-- Modal content form contact-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<div role="form" class="wpcf7" id="wpcf7-f3697-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/en/blockchain-application-development/#wpcf7-f3697-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3697" />
<input type="hidden" name="_wpcf7_version" value="5.0.5" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3697-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<div class="row gutter-10 contactform4">
<h4 class="title-form" style="display:none">Please fill in the form below and send us, we will contact you soon.</h4>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your Name *" /></span></div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" /></span></div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Phone Number *" /></span> </div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Your Question" /></span> </div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can"><select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false"><option value="Please select your interest">Please select your interest</option><option value="Blockchain Application Development">Blockchain Application Development</option><option value="Web App and Website Development">Web App and Website Development</option><option value="Mobile Application Development">Mobile Application Development</option><option value="Custom Software Development">Custom Software Development</option><option value="Software Testing and QA Services">Software Testing and QA Services</option><option value="Hire developers from us">Hire developers from us</option></select></span></div>
<div class="col-sm-6">
<span>Attache file</span><br />
<span class="wpcf7-form-control-wrap file-978"><input type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false" /></span>
</div>
<div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="More details"></textarea></span> </div>
<div class="col-sm-6 captcha-form-vins">
<div class="wpcf7-form-control-wrap"><div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
<noscript>
<div style="width: 302px; height: 422px;">
<div style="width: 302px; height: 422px; position: relative;">
<div style="width: 302px; height: 422px; position: absolute;">
<iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
</iframe>
</div>
<div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
</textarea>
</div>
</div>
</div>
</noscript>
</div>
</div>
<div class="col-sm-6"><input type="submit" value="SEND" class="wpcf7-form-control wpcf7-submit frm_ycbg" /></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>          </div>
</div>
</div>
</div>
</div>
<div id="popup-team-member" class="popup-2">
<div class="modal fade" id="myModa1" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="row">
<div class="image-members-full col-md-4 col-sm-6 col-xs-12">
<div class="image">
</div>
</div>
<div class="title-modal col-md-8 col-sm-6 col-xs-12">
<div class="title">
<h3 class="modal-title"></h3>
<span class="modal-title-mini"></span>
<div class="social-content">
</div>
</div>
</div>
</div>
</div>
<div class="line-color"></div>
<div class="modal-body">
<div class="content-member">
</div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
</div>
<style type="text/css">
.themesflat-top .content-left {
float: none;
}
.themesflat-top .content-left ul{
float: left;
}
.themesflat-top .content-left ul.language-chooser{
/*margin-right: 15px;*/
}
.themesflat-top .content-left ul.language-chooser li{
padding-left: 0px;
padding-right: 10px;
}
.themesflat-top .content-left ul.language-chooser li a::after {
content: "";
border-right: 1px solid #d8d8d8;
margin-left: 6px;
}
.themesflat-top .content-left ul.language-chooser li:last-child a::after {
border: 0px;
content: "";
}
.themesflat-top .content-left ul.language-chooser li.active a,
.themesflat-top .content-left ul.language-chooser li a:hover{
color: #3d9be9;
opacity: 1;
}
.themesflat-top .content-left ul.language-chooser li a{
padding: 0px;
font-size: 13px;
color: #ffffff94;
opacity: 0.4;
}
.themesflat-top .content-left ul.language-chooser li a img{
width: 22px;
height: 14px;
}
/*.themesflat-top .content-left ul.language-chooser li:nth-child(2) a img{
height: 16px;
}*/
</style>
<style type="text/css">
.popup #myModal2{
z-index: 99999;
}
</style><div class="themesflat_header_wrap header-style1" data-header_style="header-style1">
<!-- Header -->
<header id="header" class="header header-style1" >
<div class="container nav">
<div class="row">
<div class="col-md-12">
<div class="header-wrap clearfix">
<div id="logo" class="logo" >                  
<a href="/en/"  title="Vinsofts JSC &#8211; Vietnam IT Outsourcing Company">
<img class="site-logo"  src="/wp-content/uploads/2019/06/Logo-Vinsofts.png" alt="Vinsofts JSC &#8211; Vietnam IT Outsourcing Company"  data-retina="/wp-content/uploads/2019/06/Logo-Vinsofts.png" />
</a>
</div>
<div class="show-search">
<a href="#"><i class="fa fa-search"></i></a>         
</div> 
<div class="nav-wrap">
<div class="btn-menu">
<span></span>
</div><!-- //mobile menu button -->
<nav id="mainnav" class="mainnav" role="navigation">
<ul id="menu-main" class="menu"><li id="menu-item-2167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2167"><a href="/en/about-us/">About Us</a>
<ul class="sub-menu">
<li id="menu-item-3792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3792"><a href="/en/about-us/">Company Overview</a></li>
<li id="menu-item-3789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3789"><a href="/en/our-history/">Our History</a></li>
<li id="menu-item-3790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3790"><a href="/en/our-partner/">Our Partners</a></li>
<li id="menu-item-3791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3791"><a href="/en/our-team/">Our Team</a></li>
<li id="menu-item-6972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6972"><a href="/en/profile-vinsofts/">Company Profile (pdf)</a></li>
<li id="menu-item-6108" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6108"><a href="/en/cat/company-activities/">Company Activities</a></li>
</ul>
</li>
<li id="menu-item-2994" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-2994"><a href="/en/services/">Services</a>
<ul class="sub-menu">
<li id="menu-item-2933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2933"><a href="/en/mobile-application-development/">Mobile App Development</a></li>
<li id="menu-item-2934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2934"><a href="/en/web-application-development/">Website Development</a></li>
<li id="menu-item-2932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2932"><a href="/en/custom-software-development/">Custom Software Development</a></li>
<li id="menu-item-2935" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2882 current_page_item menu-item-2935"><a href="/en/blockchain-application-development/">Blockchain Development</a></li>
<li id="menu-item-2943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2943"><a href="/en/software-testing-and-qa-services/">Software Testing and QA Services</a></li>
<li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="/en/automated-testing/">Automated Testing</a></li>
<li id="menu-item-2942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2942"><a href="/en/offshore-development-center/">Offshore Development Center</a></li>
</ul>
</li>
<li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3202"><a href="/en/case-study/">Case Studies</a></li>
<li id="menu-item-4112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4112"><a href="/en/jobs">Careers</a>
<ul class="sub-menu">
<li id="menu-item-4912" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4912"><a href="/en/job_cats/mobile-development-2/">Mobile Development</a></li>
<li id="menu-item-4911" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4911"><a href="/en/job_cats/web-development-2/">Web Development</a></li>
<li id="menu-item-4913" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4913"><a href="/en/job_cats/blockchain-development-2/">Blockchain Development</a></li>
<li id="menu-item-4810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a href="/en/apply-for-job/">Apply for a job</a></li>
</ul>
</li>
<li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="/en/news/">News</a></li>
<li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="/en/contact-us/">Contact Us</a></li>
</ul>        <div class="info-top-right border-left" style="display: none">
<a class="appoinment" href="#" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Get Quote</a>
<a class="hotline-header" href="tel:(+84) 0247 1080 285">
<i class="fa fa-phone" aria-hidden="true"></i> <span>(+84) 0247 1080 285</span>
</a>
<i></i>
</div>
</nav><!-- #site-navigation -->  
</div><!-- /.nav-wrap -->                                
</div><!-- /.header-wrap -->
<div class="submenu top-search widget_search">
<form role="search" method="get" class="search-form" action="/en/">
<label>
<span class="screen-reader-text">Search for:</span>
<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
</label>
<input type="submit" class="search-submit" value="Search" />
</form>                </div> 
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->    
</header><!-- /.header --></div> 	<!-- Page Title -->
<!-- Page title -->
<div class="page-title">
<div class="overlay"></div>   
<div class="container"> 
<div class="row">
<div class="col-md-12 page-title-container">
<div class="breadcrumb-trail breadcrumbs">
<span class="trail-browse"></span> <span class="trail-begin"><a href="/en" title="Vinsofts JSC - Vietnam IT Outsourcing Company">Home</a></span>
<span class="sep">&gt;</span> <span class="trail-end">Blockchain Development</span>
</div> 
<h1>Blockchain Development</h1>            </div><!-- /.col-md-12 -->  
</div><!-- /.row -->  
</div><!-- /.container -->                      
</div><!-- /.page-title --> 	
<div id="content" class="page-wrap sidebar-left">
<div class="container content-wrapper">
<div class="row">
<div class="col-md-12">
<div id="primary" class="content-area sidebar-left">
<main id="main" class="post-wrap" role="main">
<article id="post-2882" class="post-2882 page type-page status-publish hentry">	
<div class="entry-content">
<div class="vc_row wpb_row vc_row-fluid vc_custom_1492683179903"><div class="row_overlay" style=""></div><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="title-section">
<h5 class="title">
</h5>		
<div class="title-content">
<p><span style="font-weight: 400;">Blockchain is the pinnacle in the era of 4.0 technology, which is considered as the key technology for digital transformation and building the information technology platform in the future. Blockchain allows all the </span> the data from transactions to be stored transparently and without the option to be modified, thus enhancing the accuracy in management.</p>
</div>
</div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid themesflat_1596047346"><div class="row_overlay" style=""></div><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
<div  class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1533714094222">
<figure class="wpb_wrapper vc_figure">
<div class="vc_single_image-wrapper   vc_box_border_grey"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" width="870" height="350" data-wpfc-original-src="/wp-content/uploads/2018/08/blockchain-2-870x350.jpg" class="vc_single_image-img attachment-themesflat-blog" alt="blank" /></div>
</figure>
</div>
<div class="wpb_text_column wpb_content_element" >
<div class="wpb_wrapper">
<h2 style="margin-bottom: 17px; font-size: 22px; font-weight: bold; letter-spacing: -0.2px; text-align: justify;"><strong>WHY DO ENTERPRISES NEED TO USE BLOCKCHAIN IN MANAGEMENT?</strong></h2>
<p style="text-align: justify;"><span style="font-weight: 400;">Blockchain technology will open up a potential application trend for many areas such as banking &amp; finance, retail, transportation, manufacturing and telecommunications. Applying Blockchain to our lives will benefit the community and the whole society.</span></p>
<ul>
<li style="font-weight: 400;"><strong>For Production</strong></li>
</ul>
<p style="text-align: justify;"><span style="font-weight: 400;">Applying Blockchain&#8217;s inexpensive and non-destructive characteristics to the manufacturing industry will help consumers trace the origin of products.</span></p>
<p><span style="font-weight: 400;">For the manufacturers, they can record and store the total quantity of products sold in the market and the amount of products left in stock.</span></p>
<ul>
<li style="font-weight: 400;"><strong>For the Medical field</strong></li>
</ul>
<p style="text-align: justify;"><span style="font-weight: 400;">At hospitals, all the results of patient&#8217;s health checks can be stored using Blockchain technology, thus keeping all information in one secure place. In addition, it helps people reduce the cost of testing again when visiting other hospitals as well as helping to trace the history of treatment process or the side effects of previous drugs. This will help to diagnose patients better.</span></p>
<ul>
<li style="font-weight: 400;"><strong>For Education</strong></li>
</ul>
<p style="text-align: justify;"><span style="font-weight: 400;">Applying Blockchain technology in education helps to manage the certificates and qualifications of universities in general or vocational training institutions in particular. It helps to verify student profiles as well as make it easy for employers to track down candidates&#8217; learning progress over time.</span></p>
<ul>
<li style="font-weight: 400;"><strong>For the Financial sector</strong></li>
</ul>
<p style="text-align: justify;"><span style="font-weight: 400;">The blockchain-based peer-to-peer money transfer program is part of the effort to provide cost-effective and high-security financial services. This is why many large banks are being pushed out by smaller rivals. Blockchain is being considered as a way to cut costs and time in interbank transactions.</span></p>
<ul>
<li style="font-weight: 400;"><strong>For E-commerce</strong></li>
</ul>
<p style="text-align: justify;"><span style="font-weight: 400;">With Blockchain technology, enterprises can take advantage of signing smart contracts and offering convenient payment solutions. Thanks to that, it is easy to do business and cooperate with many domestic and foreign companies with a time-saving and simpler procedure.</span></p>
</div>
</div>
<div class="wpb_text_column wpb_content_element  vc_custom_1536115941579" >
<div class="wpb_wrapper">
<h3 style="letter-spacing: 0.5px; margin-bottom: 15px;">WHY CHOOSE US?</h3>
<div style="letter-spacing: 0.13px; line-height: 28px; font-size: 15px;"></div>
</div>
</div>
<div id="custom_equal_height" class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="themesflat_counter style2 custom_1  vc_custom_1554178126148">
<div class="counter-icon"><div class="icon"><span class="vc_li vc_li-user"></span></div></div>
<div class="counter-content-left">
<span class="counter-prefix"></span>
<span class="numb-count" data-add_zero = "0" data-from="0" data-to="10" data-speed="1000" data-waypoint-active="yes">10</span>
<span class="counter-surfix">+</span>
</div>
<div class="counter-content-right">
<div class="counter-content">
<p style="text-align: center; font-size: 14px;">BLOCKCHAIN<br />
DEVELOPERS</p>
</div>
</div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="themesflat_counter style2   vc_custom_1554178136624">
<div class="counter-icon"><div class="icon"><span class="vc-material vc-material-check"></span></div></div>
<div class="counter-content-left">
<span class="counter-prefix"></span>
<span class="numb-count" data-add_zero = "0" data-from="0" data-to="10" data-speed="1000" data-waypoint-active="yes">10</span>
<span class="counter-surfix">+</span>
</div>
<div class="counter-content-right">
<div class="counter-content">
<p style="text-align: center; font-size: 14px;">SUCCESSFUL<br />
PROJECTS</p>
</div>
</div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="themesflat_counter style2   vc_custom_1553850447802">
<div class="counter-icon"><div class="icon"><span class="vc_li vc_li-world"></span></div></div>
<div class="counter-content-left">
<span class="counter-prefix"></span>
<span class="numb-count" data-add_zero = "0" data-from="0" data-to="10" data-speed="1000" data-waypoint-active="yes">10</span>
<span class="counter-surfix">+</span>
</div>
<div class="counter-content-right">
<div class="counter-content">
<p style="text-align: center; font-size: 14px;">COUNTRIES<br />
WORLDWIDE</p>
</div>
</div></div></div></div></div><div class="e-height wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="themesflat_counter style2   vc_custom_1554178191857">
<div class="counter-icon"><div class="icon"><span class="vc_li vc_li-like"></span></div></div>
<div class="counter-content-left">
<span class="counter-prefix"></span>
<span class="numb-count" data-add_zero = "0" data-from="0" data-to="3" data-speed="1000" data-waypoint-active="yes">3</span>
<span class="counter-surfix">+</span>
</div>
<div class="counter-content-right">
<div class="counter-content">
<p style="text-align: center; font-size: 14px;">YEARS OF<br />
EXPERIENCE</p>
</div>
</div></div></div></div></div></div><div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1495612460535"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element  vc_custom_1537759372846" >
<div class="wpb_wrapper">
<h3 style="letter-spacing: -0.01px; margin-bottom: 16px; text-transform: uppercase; font-weight: bold;"><strong>WORKING PROCEss IN VINSOFTS</strong></h3>
</div>
</div>
<div class="wpb_text_column wpb_content_element" >
<div class="wpb_wrapper">
<ul class="themesflat-list" style="font-size: 14px;">
<li><span style="font-weight: 400;">Communicate with customers to understand their needs</span></li>
<li><span style="font-weight: 400;">Give out solutions and quotation to customers</span></li>
<li><span style="font-weight: 400;">Carry out project implementation</span></li>
<li><span style="font-weight: 400;">Customer test output and give feedbacks</span></li>
<li><span style="font-weight: 400;">Vinsofts complete the product based on the suggestions of customers</span></li>
<li><span style="font-weight: 400;">Handing over products to customers</span></li>
<li><span style="font-weight: 400;">Vinsofts training and providing guiding documentation</span></li>
</ul>
<p>&nbsp;</p>
<p style="text-align: center;"><strong>Contact us now for advice and support as quickly as possible</strong></p>
<p style="text-align: center;"><div class="link-contact-us"><a href="" target="" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Contact Now</a></div>
</div>
</div>
</div></div></div></div></div></div></div></div>
</div><!-- .entry-content -->
<footer class="entry-footer">
</footer><!-- .entry-footer -->
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->
<div id="secondary" class="widget-area" role="complementary">
<div class="sidebar">
<div id="nav_menu-12" class="widget widget_nav_menu"><div class="menu-menu-service-container"><ul id="menu-menu-service" class="menu"><li id="menu-item-3209" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3209"><a href="/en/mobile-application-development/">Mobile App Development</a></li>
<li id="menu-item-3211" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3211"><a href="/en/web-application-development/">Website Development</a></li>
<li id="menu-item-3207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3207"><a href="/en/custom-software-development/">Custom Software Development</a></li>
<li id="menu-item-3206" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2882 current_page_item menu-item-3206"><a href="/en/blockchain-application-development/">Blockchain Development</a></li>
<li id="menu-item-3210" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3210"><a href="/en/software-testing-and-qa-services/">Software Testing and QA Services</a></li>
<li id="menu-item-3205" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3205"><a href="/en/automated-testing/">Automated Testing</a></li>
<li id="menu-item-3208" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3208"><a href="/en/offshore-development-center/">Offshore Development Center</a></li>
</ul></div></div>	</div>
</div><!-- #secondary -->
</div><!-- /.col-md-12 -->

</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- #content -->
<!-- Footer -->
<div class="footer_background">
<footer class="footer">      
<div class="container">
<div class="row"> 
<div class="footer-widgets">
<div class="col-md-4 col-sm-6">
<div id="text-2" class="widget widget_text">			<div class="textwidget"><p><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-full wp-image-8330" data-wpfc-original-src="/wp-content/uploads/2019/06/LOGO-FOOTER-min.png" alt="blank" width="169" height="91" /></p>
</div>
</div><div id="text-5" class="widget widget_text">			<div class="textwidget"><p>With over 70 employees and 10+ years experience in software development for clients from all over the world, we are proud of being a top software outsourcing company in Vietnam. We always commit to deliver highest quality of services, to be a trustful partner of any company.</p>
</div>
</div>                        </div>
<div class="col-md-2 col-sm-6">
<div id="text-10" class="widget widget_text"><h4 class="widget-title">FIND US</h4>			<div class="textwidget"></div>
</div><div id="widget_themesflat_socials-5" class="widget widget_themesflat_socials">
<ul class="themesflat-shortcode-socials">
<li class="facebook">
<a class="title" href="https://www.facebook.com/vinsoftsjsc" target="_blank" rel="alternate" title="Facebook">
<i class="fa fa-facebook"></i>
<span>Facebook</span>
</a>
</li><li class="youtube">
<a class="title" href="https://www.youtube.com/channel/UC33N-fg42Gkp1Fcin5Zp5EA/" target="_blank" rel="alternate" title="Youtube">
<i class="fa fa-youtube"></i>
<span>Youtube</span>
</a>
</li><li class="linkedin">
<a class="title" href="https://www.linkedin.com/company/vinsofts/" target="_blank" rel="alternate" title="LinkedIn">
<i class="fa fa-linkedin"></i>
<span>LinkedIn</span>
</a>
</li>        </ul><!-- /.social -->       
</div><div id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><ul class="themesflat-shortcode-socials">
<li style="width: 115%;">
<a href="https://goo.gl/maps/UksWXm7gqr62" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>Google map</a>
</li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title">Our Services</h4><div class="menu-footer_service-container"><ul id="menu-footer_service" class="menu"><li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2902"><a href="/en/mobile-application-development/">Mobile App Development</a></li>
<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903"><a href="/en/web-application-development/">Website Development</a></li>
<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2901"><a href="/en/custom-software-development/">Custom Software Development</a></li>
<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2882 current_page_item menu-item-2904"><a href="/en/blockchain-application-development/">Blockchain Development</a></li>
</ul></div></div>                        </div>
<div class="col-md-3 col-sm-6">
<div id="nav_menu-14" class="widget widget_nav_menu"><h4 class="widget-title">Information</h4><div class="menu-menu-footer-right-container"><ul id="menu-menu-footer-right" class="menu"><li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3559"><a href="/en/contact-us/">Contact Us</a></li>
<li id="menu-item-4013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4013"><a href="/en/jobs/">Recruitment</a></li>
</ul></div></div><div id="custom_html-6" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="cus_dmca">
<a href="//www.dmca.com/Protection/Status.aspx?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/dmca_protected_16_120.png?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559"  alt="DMCA.com Protection Status" /></a>
</div>
<style type="text/css">
.cus_dmca a img {
padding: 10px 0px 0px 48px;
}
</style></div></div>                        </div>
</div><!-- /.footer-widgets -->           
</div><!-- /.row -->    
</div><!-- /.container -->   
</footer>
<div class="content-register-footer">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="left">  
<!-- English content here -->
<div class="textwidget">
<p>Company: Vinsofts Joint Stock Company</p>
<!-- <p>Head Office: 8th floor, Sannam building, Duy Tan street, Dich Vong Hau ward, Cau Giay district, Hanoi, Vietnam</p> -->
<p>Office in Hanoi: 5th floor, No. 8 Phan Van Truong street, Dich Vong Hau ward, Cau Giay district, Hanoi, Vietnam</p>
<!-- <p>Office in Ho Chi Minh City: Unit P5-16.B Charmington La Pointe, No. 181 Cao Thang street, Ward 12, District 10, Ho Chi Minh City, Vietnam</p> -->
<p>Office in Ho Chi Minh City: P516 Block C Charmington La Pointe, No. 181 Cao Thang street, Ward 12, District 10, Ho Chi Minh City, Vietnam</p>
<p>Tel:&nbsp;<a href="tel:0462593148">04.6259.3148</a>&nbsp;– Hotlline:&nbsp;<a href="tel:0961678247">0961.678.247</a>&nbsp;– Email:&nbsp;<a href="mailto:info@vinsofts.com">info@vinsofts.com</a></p>
<p>Business License No. 0107354530 issued by Department of Planning and Investment on 14/03/2016</p>
</div>
</div>
</div>
<div class="col-xs-12 col-md-6 col-sm-6">
<div class="right">
<!-- English content here -->
<div class="textwidget">
<p>Please read the <a href="/en/privacy-policy">Privacy Policy</a> and <a href="/en/terms-of-use">Terms of Use</a> carefully!</p>
<p>Website has been notified and accepted by the Department of E-commerce and Information Technology, Ministry of Industry and Trade.</p>
<p><a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43043"><img onload="Wpfcll.r(this,true);" src="/wp-content/plugins/wp-fastest-cache-premium/pro/images/blank.gif" class="alignnone size-medium wp-image-5318" data-wpfc-original-src="/wp-content/uploads/2018/10/vinsofts-dathongbao-300x114.png" alt="blank" width="300" height="114" /></a></p>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Bottom -->
<div class="bottom">
<div class="container">           
<div class="row">
<div class="col-md-12">
<div class="copyright">                        
<p>Copyright <i class="fa fa-copyright"></i> 2012 - 2019
<a href="#">Vinsofts JSC</a>. All rights reserved.</p>                        </div>
<!-- Go Top -->
<a class="go-top show">
<i class="fa fa-chevron-up"></i>
</a>
</div><!-- /.col-md-12 -->
</div><!-- /.row -->
</div><!-- /.container -->
</div> 
<div id="tawk-to-vinsofts">
<!--Start of Tawk.to Script-->
<!--End of Tawk.to Script-->
</div>  
</div> <!-- Footer Background Image -->
</div><!-- /#boxed -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe onload="Wpfcll.r(this,true);" data-wpfc-original-src="https://www.googletagmanager.com/ns.html?id=GTM-W8XSN4W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <link rel='stylesheet' id='fo-child-css'  href='/wp-content/themes/fo-child/common/css/custom-css.css?version=68' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_linecons-css'  href='/wp-content/plugins/js_composer/assets/css/lib/vc-linecons/vc_linecons_icons.min.css' type='text/css' media='all' /> -->
<!-- <link rel='stylesheet' id='vc_material-css'  href='/wp-content/plugins/js_composer/assets/css/lib/vc-material/vc_material.min.css' type='text/css' media='all' /> -->
<link rel="stylesheet" type="text/css" href="/wp-content/cache/wpfc-minified/f13l60j9/4ckr.css" media="all"/>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&#038;render=explicit'></script>
<noscript id="wpfc-google-fonts"><link rel='stylesheet' id='themesflat-theme-slug-fonts-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600' type='text/css' media='all' />
</noscript>
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/folii5hc/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7lob9sfs/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/d6ougkxu/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/eqjz612f/4b9p.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lpm2zzii/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/fte2l3l2/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mkx6gd8y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/mknr8988/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/zdl5aix/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/7xsi43ue/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/976glhgy/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1eotoqvg/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/e726dwxx/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/llhj9sk8/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/erkz11cs/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/1281u0q0/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/lll4wyip/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/dq6j6xeh/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/869bu30y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/47mj14y/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/qa893n0m/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/es8b11eb/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/knvqh2vr/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/49n6o1s/4b9o.js'></script> -->
<!-- <script type='text/javascript' src='/wp-content/cache/wpfc-minified/f42lk0az/4cks.js'></script> -->
<script type='text/javascript'>
/* <![CDATA[ */
var aamLocal = {"nonce":"6207c2d185","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b7f8308f31d0f771d84184a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/en\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/en\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>
<script src='/wp-content/cache/wpfc-minified/2e3jmnrl/4b9p.js' type="text/javascript"></script>
<script type="text/javascript">function setREVStartSize(e){
try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
}catch(d){console.log("Failure at Presize of Slider:"+d)}
};</script>
<script>
setTimeout(function(){
// $('.popup #myModal2').slideDown();
jQuery('.popup #myModal2').show();
jQuery('#myModal2').modal({
backdrop: 'static',
keyboard: false
});
// $('body').addClass('modal-open');
// $('body').append('<div class="modal-backdrop fade in"></div>');
// $('button.close').click(function(){
//     $('#myModal2').slideUp();
//     $('body').removeClass('modal-open');
//     $('div.modal-backdrop').remove();
// })
}, 50000);
</script>
<script type='text/javascript'>
//<![CDATA[
// JavaScript Document
// var message="NoRightClicking";
// function defeatIE() {
//   if (document.all) {(message);return false;}
// }
// function defeatNS(e) {
//   if (document.layers||(document.getElementById&&!document.all))
//   { if (e.which==2||e.which==3) {(message);return false;}}
// }
// if (document.layers) {
//   document.captureEvents(Event.MOUSEDOWN);
//   document.onmousedown=defeatNS;
// } else{
//   document.onmouseup=defeatNS;document.oncontextmenu=defeatIE;
// }
// document.oncontextmenu=new Function("return false")
// //]]>
// // enable to override webpacks publicPath
// var webpackPublicPath = '/';
// jQuery(document).keydown(function(event) {
//     if (
//       event.keyCode === 123 ||
//       (event.ctrlKey && event.shiftKey && event.keyCode === 67) ||
//       (event.ctrlKey && event.keyCode === 85)
//     ) {
//       return false;
//     }
// });
// document.onselectstart = new Function('return false');
// if (window.sidebar) {
//     document.onmousedown = false;
//     document.onclick = true;
// }
</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
var forms = document.getElementsByTagName( 'form' );
var pattern = /(^|\s)g-recaptcha(\s|$)/;
for ( var i = 0; i < forms.length; i++ ) {
var divs = forms[ i ].getElementsByTagName( 'div' );
for ( var j = 0; j < divs.length; j++ ) {
var sitekey = divs[ j ].getAttribute( 'data-sitekey' );
if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
var params = {
'sitekey': sitekey,
'type': divs[ j ].getAttribute( 'data-type' ),
'size': divs[ j ].getAttribute( 'data-size' ),
'theme': divs[ j ].getAttribute( 'data-theme' ),
'badge': divs[ j ].getAttribute( 'data-badge' ),
'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
};
var callback = divs[ j ].getAttribute( 'data-callback' );
if ( callback && 'function' == typeof window[ callback ] ) {
params[ 'callback' ] = window[ callback ];
}
var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );
if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
params[ 'expired-callback' ] = window[ expired_callback ];
}
var widget_id = grecaptcha.render( divs[ j ], params );
recaptchaWidgets.push( widget_id );
break;
}
}
}
};
document.addEventListener( 'wpcf7submit', function( event ) {
switch ( event.detail.status ) {
case 'spam':
case 'mail_sent':
case 'mail_failed':
for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
grecaptcha.reset( recaptchaWidgets[ i ] );
}
}
}, false );
</script>
<script defer src='/wp-content/cache/wpfc-minified/f3jtvtsg/4cks.js' type="text/javascript"></script>
<script>document.addEventListener('DOMContentLoaded',function(){function wpfcgl(){var wgh=document.querySelector('noscript#wpfc-google-fonts').innerText, wgha=wgh.match(/<link[^\>]+>/gi);for(i=0;i<wgha.length;i++){var wrpr=document.createElement('div');wrpr.innerHTML=wgha[i];document.body.appendChild(wrpr.firstChild);}}wpfcgl();});</script>
<script type="text/javascript">document.addEventListener('DOMContentLoaded',function(){
jQuery(document).ready(function(){
// jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
//     jQuery(".home-page-form-contact").hide('fade');
// });
// var date = new Date();
// var minutes = 1;
// date.setTime(date.getTime() + (minutes * 60 * 1000));
// if (jQuery.cookie("popup_1_2") == null) {
//     setTimeout(function(){
//         jQuery(".popup #myModal2").show();
//     }, 30000);
//     jQuery.cookie("popup_1_2", "foo", { expires: date });
// }
// jQuery('#myModal2').modal({
//     backdrop: 'static',
//     keyboard: false
// });
jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());
jQuery(".btn-our-members").attr('data-toggle', 'modal');
jQuery(".btn-our-members").attr('data-target', '#myModa1');
jQuery(".btn-our-members").attr('href','#');
// jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());
jQuery(".btn-our-members").click(function(){
var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
var content   = parent.find('.team-desc').html();
var title   = parent.find('.team-info .team-name').html();
var title_mini   = parent.find('.team-info .team-subtitle').html();
var image_member =  parent.find('.team-image').html();
var social =  parent.find('.box-social-links .social-links').html();
// jQuery(".popup-2 .modal-body").html(content);
jQuery(".popup-2 .modal-content .modal-title").html(title);
jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
jQuery(".popup-2 .modal-body .content-member").html(content);
jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
})
});});</script>
<script>
(function () {
document.addEventListener("DOMContentLoaded", function () {
var e = "dmca-badge";
var t = "refurl";
if (!document.getElementsByClassName) {
document.getElementsByClassName = function (e) {
var t = document.getElementsByTagName("a"), n = [], r = 0, i;
while (i = t[r++]) {
i.className == e ? n[n.length] = i : null
}
return n
}
}
var n = document.getElementsByClassName(e);
if (n[0].getAttribute("href").indexOf("refurl") < 0) {
for (var r = 0; r < n.length; r++) {
var i = n[r];
i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
}
}
}, false)
}
)()
</script>
</body>
</html><!-- WP Fastest Cache file was created in 5.3125109672546 seconds, on 29-07-20 18:29:10 -->