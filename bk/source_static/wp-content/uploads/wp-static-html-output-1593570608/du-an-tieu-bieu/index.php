<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">



<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W8XSN4W');</script>
    
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125473191-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-125473191-1');
</script>
<title>Dự án tiêu biểu</title>


<meta name="description" content="Vinsofts với 10 năm kinh nghiệm đã được nhiều khách hàng trong nước và nước ngoài sử dụng dịch vụ. Dự án tiêu biểu như: JWC Blockchain Ventures, App RBX active">

<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="article">
<meta property="og:title" content="Dự án tiêu biểu">
<meta property="og:description" content="Vinsofts với 10 năm kinh nghiệm đã được nhiều khách hàng trong nước và nước ngoài sử dụng dịch vụ. Dự án tiêu biểu như: JWC Blockchain Ventures, App RBX active">
<meta property="og:url" content="/du-an-tieu-bieu/">
<meta property="og:site_name" content="Công ty phần mềm Vinsofts">
<meta property="article:publisher" content="https://www.facebook.com/vinsoftsjsc">
<meta property="fb:app_id" content="1096476827181655">
<meta property="og:image" content="http://PLACEHOLDER.wpsho/wp-content/uploads/2018/08/all-vi.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:description" content="Vinsofts với 10 năm kinh nghiệm đã được nhiều khách hàng trong nước và nước ngoài sử dụng dịch vụ. Dự án tiêu biểu như: JWC Blockchain Ventures, App RBX active">
<meta name="twitter:title" content="Dự án tiêu biểu">
<meta name="twitter:image" content="http://PLACEHOLDER.wpsho/wp-content/uploads/2018/08/all-vi.jpg">
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"\/","name":"C\u00f4ng ty ph\u1ea7n m\u1ec1m Vinsofts","potentialAction":{"@type":"SearchAction","target":"\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>


<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//s.w.org">


<link rel="stylesheet" href="/wp-content/cache/minify/78e0d.css" media="all">





<style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style>
<link rel="stylesheet" href="/wp-content/cache/minify/2e5fd.css" media="all">

<link rel="stylesheet" id="themesflat-theme-slug-fonts-css" href="https://fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900%2Cregular%7CPoppins%3A300%2C400%2C500%2C600%2C700%2C900%2C600%7CPoppins%3A600" type="text/css" media="all">
<link rel="stylesheet" href="/wp-content/cache/minify/28e3c.css" media="all">











<style id="themesflat-inline-css-inline-css" type="text/css">
.logo{padding-top:24px; padding-left:15px; }
.footer{padding-top:64px; padding-bottom:74px; }
.page-title{padding-top:21px; }
.logo img, .logo svg { height:70px; }
.page-title .overlay{ background: #ffffff}.page-title {background: url() center /cover no-repeat;}.page-title h1 {color:#21536c!important;
		}
.breadcrumbs span,.breadcrumbs span a, .breadcrumbs a {color:#595959!important;
		}
body,button,input,select,textarea { font-family:Poppins ; }
body,button,input,select,textarea { font-weight:400;}
body,button,input,select,textarea { font-style:normal; }
body,button,input,select,textarea { font-size:14px; }
body,button,input,select,textarea { line-height:25px ; }
h1,h2,h3,h5,h6 { font-family:Poppins;}
h1,h2,h3,h4,h5,h6 { font-weight:600;}
h1,h2,h3,h4,h5,h6  { font-style:normal; }
#mainnav > ul > li > a { font-family:Poppins;}
#mainnav > ul > li > a { font-weight:600;}
#mainnav > ul > li > a  { font-style:normal; }
#mainnav ul li a { font-size:14px;}
#mainnav > ul > li > a { line_height100px;}
h1 { font-size:32px; }
h2 { font-size:25px; }
h3 { font-size:22px; }
h4 { font-size:18px; }
h5 { font-size:15px; }
h6 { font-size:14px; }
.iconbox .box-header .box-icon span,a:hover, a:focus,.portfolio-filter li a:hover, .portfolio-filter li.active a,.themesflat-portfolio .item .category-post a,.color_theme,.widget ul li a:hover,.footer-widgets ul li a:hover,.footer a:hover,.themesflat-top a:hover,.themesflat-portfolio .portfolio-container.grid2 .title-post a:hover,.themesflat-button.no-background, .themesflat-button.blog-list-small,.show-search a i:hover,.widget.widget_categories ul li a:hover,.breadcrumbs span a:hover, .breadcrumbs a:hover,.comment-list-wrap .comment-reply-link,.portfolio-single .content-portfolio-detail h3,.portfolio-single .content-portfolio-detail ul li:before, .themesflat-list-star li:before, .themesflat-list li:before,.navigation.posts-navigation .nav-links li a .meta-nav,.testimonial-sliders.style3 .author-name a,ul.iconlist .list-title a:hover,.themesflat_iconbox .iconbox-icon .icon span,.bottom .copyright a,.top_bar2 .wrap-header-content ul li i { color:#337493;}
 #Ellipse_7 circle,.testimonial-sliders .logo_svg path { fill:#337493;}
.info-top-right a.appoinment, .wrap-header-content a.appoinment,button, input[type=button], input[type=reset], input[type=submit],.go-top:hover,.portfolio-filter.filter-2 li a:hover, .portfolio-filter.filter-2 li.active a,.themesflat-socials li a:hover, .entry-footer .social-share-article ul li a:hover,.themesflat-button,.featured-post.blog-slider .flex-prev, .featured-post.blog-slider .flex-next,mark, ins,#themesflat-portfolio-carousel ul.flex-direction-nav li a, .flex-direction-nav li a,.navigation.posts-navigation .nav-links li a:after,.title_related_portfolio:after,.navigation.loadmore a:hover,.owl-theme .owl-controls .owl-nav [class*=owl-],.widget.widget_tag_cloud .tagcloud a,.btn-menu:before, .btn-menu:after, .btn-menu span,.themesflat_counter.style2 .themesflat_counter-icon .icon,widget a.appoinment,.themesflat_imagebox .imagebox-image:after,.nav-widget a.appoinment { background-color:#337493; }
.themesflat_btnslider:not(:hover) {
			background-color:#337493!important;
		}
.loading-effect-2 > span, .loading-effect-2 > span:before, .loading-effect-2 > span:after,textarea:focus, input[type=text]:focus, input[type=password]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=date]:focus, input[type=month]:focus, input[type=time]:focus, input[type=week]:focus, input[type=number]:focus, input[type=email]:focus, input[type=url]:focus, input[type=search]:focus, input[type=tel]:focus, input[type=color]:focus,select:focus,.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span,.navigation.loadmore a:hover { border-color:#337493}
 {
			border-color:#337493!important;

		}
 {
			color: #fff !important;

		}
 {
		background-color: #2e363a !important;
	}
#Financial_Occult text,#F__x26__O tspan { fill:#595959;}
body { color:#595959}
a,.portfolio-filter li a,.themesflat-portfolio .item .category-post a:hover,.title-section .title,ul.iconlist .list-title a,.breadcrumbs span a, .breadcrumbs a,.breadcrumbs span,h1, h2, h3, h4, h5, h6,strong,.testimonial-content blockquote,.testimonial-content .author-info,.sidebar .widget ul li a,.themesflat_counter.style2 .themesflat_counter-content-right,.themesflat_counter.style2 .themesflat_counter-content-left,.title_related_portfolio,.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation .current, .page-links a:hover, .page-links a:focus,.widget_search .search-form input[type=search],.entry-meta ul,.entry-meta ul.meta-right,.entry-footer strong,.widget.widget_archive ul li:before, .widget.widget_categories ul li:before, .widget.widget_recent_entries ul li:before { color:#595959}
.owl-theme .owl-dots .owl-dot span,.widget .widget-title:after, .widget .widget-title:before,ul.iconlist li.circle:before { background-color:#595959}
.navigation.paging-navigation:not(.loadmore) a:hover, .navigation.paging-navigation:not(.loadmore) .current, .page-links a:hover, .page-links a:focus, .page-links > span { border-color:#595959}
.themesflat-top { background-color:#21536c ; } 
.themesflat-top .border-left:before, .themesflat-widget-languages:before, .themesflat-top .border-right:after { background-color: rgba(255,255,255,0.2);}.themesflat-top,.info-top-right,.themesflat-top a, .themesflat-top .themesflat-socials li a { color:#ffffff ;} 
.themesflat_header_wrap.header-style1,.nav.header-style2,.themesflat_header_wrap.header-style3,.nav.header-style4,.header.widget-header .nav { background-color:#fff;}
#mainnav > ul > li > a { color:#595959;}
#mainnav > ul > li > a:hover,#mainnav > ul > li.current-menu-item > a { color:#377493 !important;}
#mainnav ul.sub-menu > li > a { color:#ffffff!important;}
#mainnav ul.sub-menu { background-color:#377493;}
#mainnav ul.sub-menu > li > a:hover { background-color:#aac9ce!important;}
#mainnav ul.sub-menu > li { border-color:#dde9eb!important;}
.footer_background:before { background-color:#21536c;}
.footer a, .footer, .themesflat-before-footer .custom-info > div,.footer-widgets ul li a,.footer-widgets .company-description p { color:#f9f9f9;}
.bottom { background-color:#337493;}
.bottom .copyright p,.bottom #menu-bottom li a { color:#e5e5e5;}
.white #Financial_Occult text,.white #F__x26__O tspan {
			fill: #fff; }test_filter_render
</style>
<script src="/wp-content/cache/minify/2a1cf.js"></script>





<script type="text/javascript">
/* <![CDATA[ */
var aamLocal = {"nonce":"5701e91cb0","ajaxurl":"\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="/wp-content/cache/minify/016b6.js"></script>













<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-32x32.png" sizes="32x32">
<link rel="icon" href="/wp-content/uploads/2018/10/cropped-favicon-192x192.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/10/cropped-favicon-180x180.png">
<meta name="msapplication-TileImage" content="/wp-content/uploads/2018/10/cropped-favicon-270x270.png">
<script type="text/javascript">function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;					
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};</script>
		<style type="text/css" id="wp-custom-css">
			.page-wrap.sidebar-left #primary.content-area {
/*     margin-top: -40px; */
}
.single-post .page-wrap.sidebar-left #primary.content-area {
    margin-top: 0;
}
.themesflat-portfolio .item .featured-post {
    text-align: center;
}
.themesflat-portfolio .item .featured-post img, .content-area.fullwidth .themesflat_iconbox.inline-left img {
    height: 220px;
		width: 100%;
		object-fit: cover;
}

/* .content-area.fullwidth .themesflat_iconbox.transparent .title {
    height: 55px;
    display: flex;
    align-items: center;
    margin-top: 0;
}
.content-area.fullwidth .themesflat_iconbox.transparent .iconbox-content p:nth-child(2) {
		height: 125px;
    overflow: hidden;
}
     */
.header.header-sticky .wpmenucartli {
	display:none !important;
}
.header.header-sticky #menu-main li:first-child {
	display: inline-block !important;
}
#mainnav>ul>li {
    margin-left: 25px !important;
}		</style>
	<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>

<body class="page-template page-template-archive-portfolios page-template-archive-portfolios-php page page-id-118  has-topbar header_sticky wide fullwidth bottom-center wpb-js-composer js-comp-ver-5.4.7 vc_responsive vi">
<div class="themesflat-boxed">
	
	<div class="preloader">
		<div class="clear-loading loading-effect-2">
			<span></span>
		</div>
	</div>
	
	

            <script>
                setTimeout(function(){
                    // $('.popup #myModal2').slideDown();
                    jQuery('.popup #myModal2').show();
                    jQuery('#myModal2').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    // $('body').addClass('modal-open');
                    // $('body').append('<div class="modal-backdrop fade in"></script></div>');
                    // $('button.close').click(function(){
                    //     $('#myModal2').slideUp();
                    //     $('body').removeClass('modal-open');
                    //     $('div.modal-backdrop').remove();
                    // })
                  }, 50000);
            
            
<div class="themesflat-top header-style1">    
    <div class="container">
        <div class="container-inside">

        	<div class="content-left">
                
<ul class="language-chooser language-chooser-image qtranxs_language_chooser" id="qtranslate-chooser">
<li class="lang-vi active"><a href="/vi/du-an-tieu-bieu/" hreflang="vi" title="VI (vi)" class="qtranxs_image qtranxs_image_vi"><img src="/wp-content/plugins/qtranslate-x/flags/vn.png" alt="VI (vi)"><span style="display:none">VI</span></a></li>
<li class="lang-en"><a href="/en/case-study/" hreflang="en" title="EN (en)" class="qtranxs_image qtranxs_image_en"><img src="/wp-content/plugins/qtranslate-x/flags/gb.png" alt="EN (en)"><span style="display:none">EN</span></a></li>
</ul><div class="qtranxs_widget_end"></div>
            
		<ul>
			<li class="border-right">
				<i class="fa fa-phone"></i><a href="tel:0247%201080%20285" target="_top"> (+84) 0247 1080 285</a>     
			</li>
			<li>
				<i class="fa fa-envelope"></i> <a href="mailto:info@vinsofts.com" target="_top">  info@vinsofts.com</a> 
			</li>

		</ul>	
		            </div>

            <div class="content-right">
                    <ul class="themesflat-socials">
            <li class="facebook">
                            <a class="title" href="" https: target="_blank" rel="alternate" title="Facebook">
                                <i class="fa fa-facebook"></i>
                                <span>Facebook</span>
                            </a>
                        </li><li class="youtube">
                            <a class="title" href="" https: target="_blank" rel="alternate" title="Youtube">
                                <i class="fa fa-youtube"></i>
                                <span>Youtube</span>
                            </a>
                        </li><li class="linkedin">
                            <a class="title" href="/wp-content/uploads/wp-static-html-output-1593570608/du-an-tieu-bieu/linkedin.com/company/vinsofts" target="_blank" rel="alternate" title="LinkedIn">
                                <i class="fa fa-linkedin"></i>
                                <span>LinkedIn</span>
                            </a>
                        </li>        </ul>       
    <div class="info-top-right border-left">
		<span><i class="fa fa-question-circle"></i>Bạn cần tư vấn?</span>
		<a class="appoinment" href="#">Yêu cầu báo giá</a>

	</div>            </div>

        </div>
    </div>        
</div>

<div class="popup">
    
    <div id="myModal2" class="modal fade in" role="dialog">
      <div class="modal-dialog modal-lg">
        
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <div role="form" class="wpcf7" id="wpcf7-f3777-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/du-an-tieu-bieu/#wpcf7-f3777-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3777">
<input type="hidden" name="_wpcf7_version" value="5.0.5">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3777-o1">
<input type="hidden" name="_wpcf7_container_post" value="0">
</div>
<div class="row gutter-10 contactform4">
<h4 class="title-form" style="display:none">Quý khách vui lòng điền form bên dưới và gửi cho chúng tôi. Chúng tôi sẽ liên hệ lại ngay.</h4>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ tên *"></span></div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *"></span></div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-phone"><input type="text" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Số điện thoại *"></span> </div>
<div class="col-sm-6"> <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Câu hỏi"></span> </div>
<div class="col-sm-6"><span class="wpcf7-form-control-wrap how_can"><select name="how_can" class="wpcf7-form-control wpcf7-select" id="how_can" aria-invalid="false"><option value="Bạn quan tâm đến dịch vụ nào?">Bạn quan tâm đến dịch vụ nào?</option><option value="Phát triển ứng dụng Blockchain">Phát triển ứng dụng Blockchain</option><option value="Phát triển ứng dụng web, thiết kế website">Phát triển ứng dụng web, thiết kế website</option><option value="Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin">Phát triển ứng dụng Mobile (iOS, Android, React Native, Xamarin</option><option value="Phát triển phần mềm quản lý">Phát triển phần mềm quản lý</option><option value="Mua Phần mềm quản trị doanh nghiệp (ERP)">Mua Phần mềm quản trị doanh nghiệp (ERP)</option><option value="Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</option><option value="Thuê nhân sự cho dự án">Thuê nhân sự cho dự án</option></select></span></div>
<div class="col-sm-6">
  <span>Đính kèm file:</span><br>
  <span class="wpcf7-form-control-wrap file-978"><input type="file" name="file-978" size="40" class="wpcf7-form-control wpcf7-file" id="attached-file" accept=".pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.gif,.zip,.rar,.gz" aria-invalid="false"></span>
  </div>
<div class="col-sm-12"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Nội dung chi tiết"></textarea></span> </div>
<div class="col-sm-10 col-xs-12 captcha-form-vins">
<div class="wpcf7-form-control-wrap"><div data-sitekey="6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" class="wpcf7-form-control g-recaptcha wpcf7-recaptcha recaptcha-all"></div>
<noscript>
	<div style="width: 302px; height: 422px;">
		<div style="width: 302px; height: 422px; position: relative;">
			<div style="width: 302px; height: 422px; position: absolute;">
				<iframe src="https://www.google.com/recaptcha/api/fallback?k=6LcAH7EUAAAAAKJpul3gjkOnwe5lgkZ8zfYZ0xBS" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
				</iframe>
			</div>
			<div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
				<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;">
				</textarea>
			</div>
		</div>
	</div>
</noscript>
</div>
</div>
<div class="col-sm-2 col-xs-12"><input type="submit" value="GỬI" class="wpcf7-form-control wpcf7-submit frm_ycbg"></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>          </div>
        </div>
      </div>
    </div>
</div>
<div id="popup-team-member" class="popup-2">
    <div class="modal fade" id="myModa1" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="row">
                        <div class="image-members-full col-md-4 col-sm-6 col-xs-12">
                            <div class="image">
                                
                            </div>
                        </div>
                        <div class="title-modal col-md-8 col-sm-6 col-xs-12">
                            <div class="title">
                                <h3 class="modal-title"></h3>
                                <span class="modal-title-mini"></span>
                                <div class="social-content">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="line-color"></div>
                <div class="modal-body">
                   
                    <div class="content-member">
                        
                    </div>
                </div>
            </div>
            
        </div>
        
      </div>
</div>
<style type="text/css">
    .themesflat-top .content-left {
        float: none;
    }
    .themesflat-top .content-left ul{
        float: left;
    }
    .themesflat-top .content-left ul.language-chooser{
        /*margin-right: 15px;*/
    }
    .themesflat-top .content-left ul.language-chooser li{
        padding-left: 0px;
        padding-right: 10px;
    }
    .themesflat-top .content-left ul.language-chooser li a::after {
        content: "";
        border-right: 1px solid #d8d8d8;
        margin-left: 6px;
    }
    .themesflat-top .content-left ul.language-chooser li:last-child a::after {
        border: 0px;
        content: "";
    }
    .themesflat-top .content-left ul.language-chooser li.active a,
    .themesflat-top .content-left ul.language-chooser li a:hover{
        color: #3d9be9;
        opacity: 1;
    }
    .themesflat-top .content-left ul.language-chooser li a{
        padding: 0px;
        font-size: 13px;
        color: #ffffff94;
        opacity: 0.4;
    }
    .themesflat-top .content-left ul.language-chooser li a img{
        width: 22px;
        height: 14px;
    }
    /*.themesflat-top .content-left ul.language-chooser li:nth-child(2) a img{
        height: 16px;
    }*/
</style>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // jQuery(".home-page-form-contact #calltrap-btn").click(function(e){
        //     jQuery(".home-page-form-contact").hide('fade');
        // });
        // var date = new Date();
        // var minutes = 1;
        // date.setTime(date.getTime() + (minutes * 60 * 1000));
        // if (jQuery.cookie("popup_1_2") == null) {
        //     setTimeout(function(){
        //         jQuery(".popup #myModal2").show();
        //     }, 30000);
        //     jQuery.cookie("popup_1_2", "foo", { expires: date });
        // }
        // jQuery('#myModal2').modal({
        //     backdrop: 'static',
        //     keyboard: false
        // });
        jQuery(".info-top-right.border-left .appoinment").attr('data-toggle', 'modal');
        jQuery(".info-top-right.border-left .appoinment").attr('data-target','#myModal2');
        jQuery(".info-top-right.border-left .appoinment").attr('data-backdrop','static');
        jQuery(".info-top-right.border-left .appoinment").attr('data-keyboard','false');
        jQuery(".themesflat-boxed .popup #myModal2 h4.modal-title").html(jQuery('.themesflat-boxed .popup #myModal2 h4.title-form').text());

        jQuery(".btn-our-members").attr('data-toggle', 'modal');
        jQuery(".btn-our-members").attr('data-target', '#myModa1');
        jQuery(".btn-our-members").attr('href','#');
        // jQuery(".popup-2 .modal-body").html(jQuery('.team-info').text());

        jQuery(".btn-our-members").click(function(){
            var parent = jQuery(this).closest('.themesflat-team.themesflat-hover');
            var content   = parent.find('.team-desc').html();
            var title   = parent.find('.team-info .team-name').html();
            var title_mini   = parent.find('.team-info .team-subtitle').html();
            var image_member =  parent.find('.team-image').html();
            var social =  parent.find('.box-social-links .social-links').html();
            // jQuery(".popup-2 .modal-body").html(content);
            jQuery(".popup-2 .modal-content .modal-title").html(title);
            jQuery(".popup-2 .modal-content .modal-title-mini").html(title_mini);
            jQuery(".popup-2 .modal-body .content-member").html(content);
            jQuery(".popup-2 .modal-content .image-members-full .image").html(image_member);
            jQuery(".popup-2 .modal-content .modal-header .social-content").html(social);
        })
    });
</script>
<style type="text/css">
    .popup #myModal2{
        z-index: 99999;
    }
</style><div class="themesflat_header_wrap header-style1" data-header_style="header-style1">

<header id="header" class="header header-style1">
    <div class="container nav">
        <div class="row">
            <div class="col-md-12">
                <div class="header-wrap clearfix">
                        <div id="logo" class="logo">                  
        <a href="/" title="Công ty phần mềm Vinsofts">
                            <img class="site-logo" src="/wp-content/uploads/2019/06/Logo-Vinsofts.png" alt="Công ty phần mềm Vinsofts" data-retina="/wp-content/uploads/2019/06/Logo-Vinsofts.png">
                    </a>
    </div>

                                        <div class="show-search">
                        <a href="#"><i class="fa fa-search"></i></a>         
                    </div> 
                                        <div class="nav-wrap">
    <div class="btn-menu">
        <span></span>
    </div>
               
    <nav id="mainnav" class="mainnav" role="navigation">
        <ul id="menu-main" class="menu"><li id="menu-item-2167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2167"><a href="/ve-chung-toi/">Về chúng tôi</a>
<ul class="sub-menu">
	<li id="menu-item-3792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3792"><a href="/ve-chung-toi/">Giới thiệu chung</a></li>
	<li id="menu-item-3789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3789"><a href="/lich-su-cong-ty/">Lịch sử công ty</a></li>
	<li id="menu-item-3790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3790"><a href="/doi-tac/">Đối tác</a></li>
	<li id="menu-item-3791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3791"><a href="/nhan-su/">Nhân sự</a></li>
	<li id="menu-item-6972" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6972"><a href="/ho-so-nang-luc/">Hồ sơ năng lực</a></li>
	<li id="menu-item-6108" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6108"><a href="/danh-muc/hoat-dong-noi-bat/">Hoạt động công ty</a></li>
</ul>
</li>
<li id="menu-item-2994" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2994"><a href="/dich-vu/">Dịch vụ</a>
<ul class="sub-menu">
	<li id="menu-item-2933" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2933"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
	<li id="menu-item-2934" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2934"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
	<li id="menu-item-2932" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2932"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
	<li id="menu-item-2935" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2935"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
	<li id="menu-item-2943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2943"><a href="/kiem-thu-phan-mem-va-dich-vu-dam-bao-chat-luong/">Kiểm thử phần mềm và dịch vụ đảm bảo chất lượng</a></li>
	<li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="/kiem-tra-tu-dong/">Kiểm tra tự động</a></li>
	<li id="menu-item-2942" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2942"><a href="/trung-tam-phat-trien-phan-mem-offshore/">Trung tâm phát triển phần mềm offshore</a></li>
</ul>
</li>
<li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-118 current_page_item menu-item-3202"><a href="/du-an-tieu-bieu/">Dự án tiêu biểu</a></li>
<li id="menu-item-4112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4112"><a href="/jobs">Tuyển dụng</a>
<ul class="sub-menu">
	<li id="menu-item-4912" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4912"><a href="/job_cats/lap-trinh-mobile/">Lập trình Mobile</a></li>
	<li id="menu-item-4911" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4911"><a href="/job_cats/lap-trinh-web/">Lập trình Web</a></li>
	<li id="menu-item-4913" class="menu-item menu-item-type-taxonomy menu-item-object-job_cats menu-item-4913"><a href="/job_cats/lap-trinh-blockchain/">Lập trình Blockchain</a></li>
	<li id="menu-item-4810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4810"><a href="/ung-tuyen/">Ứng tuyển ngay</a></li>
</ul>
</li>
<li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="/tin-tuc/">Tin tức</a></li>
<li id="menu-item-206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-206"><a href="/lien-he/">Liên hệ</a></li>
</ul>        <div class="info-top-right border-left" style="display: none">
			<a class="appoinment" href="#" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">Yêu cầu Báo giá</a>
			<a class="hotline-header" href="tel:(+84)%200247%201080%20285">
				<i class="fa fa-phone" aria-hidden="true"></i> <span>(+84) 0247 1080 285</span>
			</a>
			<i></i>
		</div>
    </nav>  
</div>                                
                </div>
                <div class="submenu top-search widget_search">
                    <form role="search" method="get" class="search-form" action="/">
				<label>
					<span class="screen-reader-text">Tìm kiếm cho:</span>
					<input type="search" class="search-field" placeholder="Tìm kiếm …" value="" name="s">
				</label>
				<input type="submit" class="search-submit" value="Tìm kiếm">
			</form>                </div> 
            </div>
        </div>
    </div>    
</header></div> 	
	
   
<div class="page-title">
    <div class="overlay"></div>   
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 page-title-container">
            
		<div class="breadcrumb-trail breadcrumbs">
			<span class="trail-browse"></span> <span class="trail-begin"><a href="/" title="Công ty phần mềm Vinsofts">Home</a></span>
			 <span class="sep">></span> <span class="trail-end">Dự án tiêu biểu</span>
		</div> 
            <h1>Dự án tiêu biểu</h1>            </div>  
        </div>  
    </div>                      
</div> 	
	<div id="content" class="page-wrap fullwidth">
		<div class="container content-wrapper">
			<div class="row">
										<script type="text/javascript">
							jQuery(document).ready(function() {
								jQuery(".themesflat-boxed .themesflat-button.themesflat-archive").addClass('btn-readmore');
							    jQuery(".themesflat-boxed .themesflat-button.themesflat-archive").text(function(i, oldText) {
							        return oldText === 'Read More ' ? 'Xem Chi Tiết ' : oldText;
							    });
							});
						</script>
					<script type="text/javascript">
    //<![CDATA[
    // JavaScript Document
    // var message="NoRightClicking";
    // function defeatIE() {
    //   if (document.all) {(message);return false;}
    // }
    // function defeatNS(e) {
    //   if (document.layers||(document.getElementById&&!document.all))
    //   { if (e.which==2||e.which==3) {(message);return false;}}
    // }
    // if (document.layers) {
    //   document.captureEvents(Event.MOUSEDOWN);
    //   document.onmousedown=defeatNS;
    // } else{
    //   document.onmouseup=defeatNS;document.oncontextmenu=defeatIE;
    // }
    // document.oncontextmenu=new Function("return false")
    // //]]>
    // // enable to override webpacks publicPath
    // var webpackPublicPath = '/';
    // jQuery(document).keydown(function(event) {
    //     if (
    //       event.keyCode === 123 ||
    //       (event.ctrlKey && event.shiftKey && event.keyCode === 67) ||
    //       (event.ctrlKey && event.keyCode === 85)
    //     ) {
    //       return false;
    //     }
    // });
    // document.onselectstart = new Function('return false');
    // if (window.sidebar) {
    //     document.onmousedown = false;
    //     document.onclick = true;
    // }
</script>

<div class="col-md-12">    
    <div class="themesflat-portfolio content-area">
        

        <div class="themesflat-portfolio clearfix"><ul class="portfolio-filter"><li class="active"><a data-filter="*" href="#">All</a></li><li><a data-filter=".blockchain-development" href="#" title="Lập trình Blockchain">Lập trình Blockchain</a></li><li><a data-filter=".mobile-development" href="#" title="Thiết kế ứng dụng Mobile App">Thiết kế ứng dụng Mobile App</a></li><li><a data-filter=".web-developement" href="#" title="Thiết kế ứng dụng Website">Thiết kế ứng dụng Website</a></li></ul><div class="portfolio-container  grid one-four show_filter_portfolio"><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/04/0.salon-%C4%91o%CC%82%CC%80ng0-300x261.png" alt="Thiết kế mobile app ngành Salon tóc nữ – Salon Đồng"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app ngành Salon tóc nữ – Salon Đồng" href="/portfolios/thiet-ke-mobile-app-nganh-salon-toc-nu-salon-dong/">Thiết kế mobile app ngành Salon tóc nữ – Salon Đồng</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/04/0.dating1-300x274.png" alt="Thiết kế mobile app Kết bạn trực tuyến AROUNYA"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app Kết bạn trực tuyến AROUNYA" href="/portfolios/thiet-ke-mobile-app-ket-ban-truc-tuyen-arounya/">Thiết kế mobile app Kết bạn trực tuyến AROUNYA</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/04/0.shalala1-240x300.png" alt="Thiết kế mobile app sàn thương mại điện tử"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app sàn thương mại điện tử" href="/portfolios/thiet-ke-mobile-app-san-thuong-mai-dien-tu/">Thiết kế mobile app sàn thương mại điện tử</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/03/0run-team1-300x265.png" alt="Thiết kế mobile app Team Building – Run Team"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app Team Building – Run Team" href="/portfolios/thiet-ke-mobile-app-team-building-run-team/">Thiết kế mobile app Team Building – Run Team</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/03/0.por_.1-300x284.png" alt="Thiết kế mobile app IoT – điều khiển thiết bị trong nhà bằng Smartphone"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app IoT – điều khiển thiết bị trong nhà bằng Smartphone" href="/portfolios/thiet-ke-mobile-app-iot-dieu-khien-thiet-bi-trong-nha-bang-smartphone/">Thiết kế mobile app IoT – điều khiển thiết bị trong nhà bằng Smartphone</a></div></div></div></div><div class="item blockchain-development mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/03/0.astra1_-300x234.png" alt="Thiết kế mobile app Mạng xã hội du lịch Astra"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/ung-dung-blockchain/" rel="tag">Lập trình Blockchain</a> / <a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app Mạng xã hội du lịch Astra" href="/portfolios/thiet-ke-mobile-app-mang-xa-hoi-du-lich-astra/">Thiết kế mobile app Mạng xã hội du lịch Astra</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2020/02/0.MXH0_-196x300.png" alt="Thiết kế Mobile app Mạng xã hội thời trang"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế Mobile app Mạng xã hội thời trang" href="/portfolios/thiet-ke-mobile-app-mang-xa-hoi-thoi-trang/">Thiết kế Mobile app Mạng xã hội thời trang</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/10/1.-Ba%CC%80i-vie%CC%82%CC%81t-sie%CC%82u-thi%CC%A3-vlxd00-side-300x271.png" alt="Thiết kế mobile app siêu thị vật liệu xây dựng"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app siêu thị vật liệu xây dựng" href="/portfolios/thiet-ke-mobile-app-sieu-thi-vat-lieu-xay-dung/">Thiết kế mobile app siêu thị vật liệu xây dựng</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/10/1.Home-%E2%80%93-2-side-300x272.png" alt="Thiết kế mobile app học Tiếng Anh trên màn hình khoá"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app học Tiếng Anh trên màn hình khoá" href="/portfolios/thiet-ke-mobile-app-hoc-tieng-anh-tren-man-hinh-khoa/">Thiết kế mobile app học Tiếng Anh trên màn hình khoá</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/06/1.photo_2019-06-26_09-27-47-side-300x272.jpg" alt="Thiết kế mobile app ngành Giúp việc"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app ngành Giúp việc" href="/portfolios/thiet-ke-mobile-app-nganh-giup-viec/">Thiết kế mobile app ngành Giúp việc</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/05/viewretreats2-300x300.png" alt="Thiết kế website ngành du lịch"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Thiết kế website ngành du lịch" href="/portfolios/thiet-ke-website-nganh-du-lich/">Thiết kế website ngành du lịch</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/04/hp102-4.jpg-300x300.png" alt="Ứng dụng di động quản lý hiệu quả quảng cáo"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Ứng dụng di động quản lý hiệu quả quảng cáo" href="/portfolios/ung-dung-di-dong-do-luong-hieu-qua-quang-cao/">Ứng dụng di động quản lý hiệu quả quảng cáo</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/04/IP.jpg-300x300.png" alt="Thiết kế mobile app “Uber trong ngành xây dựng”"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app “Uber trong ngành xây dựng”" href="/portfolios/thiet-ke-mobile-app-uber-trong-nganh-noi-that/">Thiết kế mobile app “Uber trong ngành xây dựng”</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/04/cover.jpg-300x300.png" alt="Thiết kế ứng dụng di động kết bạn"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế ứng dụng di động kết bạn" href="/portfolios/thiet-ke-ung-dung-di-dong-ket-ban/">Thiết kế ứng dụng di động kết bạn</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/04/Artboard-%E2%80%93-2-2-300x300.png" alt="Thiết kế mobile app đặt lịch dịch vụ Spa"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế mobile app đặt lịch dịch vụ Spa" href="/portfolios/thiet-ke-mobile-app-dat-lich-dich-vu-spa/">Thiết kế mobile app đặt lịch dịch vụ Spa</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/01/Du-an-thiet-ke-web-du-lich-300x273.png" alt="Dự án thiết kế website du lịch"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Dự án thiết kế website du lịch" href="/portfolios/du-an-thiet-ke-website-du-lich/">Dự án thiết kế website du lịch</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/01/Du-an-thiet-ke-web-to-chuc-su-kien-300x215.png" alt="Dự án thiết kế website chuyên sự kiện"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Dự án thiết kế website chuyên sự kiện" href="/portfolios/du-an-thiet-ke-website-chuyen-su-kien/">Dự án thiết kế website chuyên sự kiện</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2019/01/D%E1%BB%B1-%C3%A1n-thi%E1%BA%BFt-k%E1%BA%BF-website-gi%E1%BB%9Bi-thi%E1%BB%87u-Hyhy-300x215.png" alt="Dự án thiết kế website giới thiệu Hy.Ly"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Dự án thiết kế website giới thiệu Hy.Ly" href="/portfolios/du-an-thiet-ke-website-gioi-thieu-hyly/">Dự án thiết kế website giới thiệu Hy.Ly</a></div></div></div></div><div class="item blockchain-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/10/JWC-300x300.png" alt="Dự án JWC Blockchain Ventures"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/ung-dung-blockchain/" rel="tag">Lập trình Blockchain</a></div><div class="title-post"><a title="Dự án JWC Blockchain Ventures" href="/portfolios/du-an-jwc-blockchain-ventures/">Dự án JWC Blockchain Ventures</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/RBX-active.jpg-300x300.png" alt="Thiết kế app RBX active"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế app RBX active" href="/portfolios/thiet-ke-app-rbx-active/">Thiết kế app RBX active</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/E-commerce-website-for-real-estate-300x300.png" alt="Thiết kế website thương mại điện tử cho bất động sản"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Thiết kế website thương mại điện tử cho bất động sản" href="/portfolios/thiet-ke-website-thuong-mai-dien-tu-cho-bat-dong-san/">Thiết kế website thương mại điện tử cho bất động sản</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/aspire-project-300x300.png" alt="Thiết kế app rèn luyện sức khỏe Aspire"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế app rèn luyện sức khỏe Aspire" href="/portfolios/thiet-ke-app-ren-luyen-suc-khoe-aspire/">Thiết kế app rèn luyện sức khỏe Aspire</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/tourism-website-300x300.png" alt="Hệ thống xây dựng website du lịch"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Hệ thống xây dựng website du lịch" href="/portfolios/he-thong-xay-dung-website-du-lich/">Hệ thống xây dựng website du lịch</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/shippy-300x300.png" alt="Thiết kế app Shippy"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế app Shippy" href="/portfolios/thiet-ke-app-shippy/">Thiết kế app Shippy</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/sustainabody-2-300x300.png" alt="Thiết kế app Sustainabody về chuyên gia dinh dưỡng"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Thiết kế app Sustainabody về chuyên gia dinh dưỡng" href="/portfolios/thiet-ke-app-sustainabody-ve-chuyen-gia-dinh-duong/">Thiết kế app Sustainabody về chuyên gia dinh dưỡng</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/mamamia-web-300x300.png" alt="Thiết kế website tin tức về phụ nữ"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Thiết kế website tin tức về phụ nữ" href="/portfolios/thiet-ke-website-tin-tuc-ve-phu-nu/">Thiết kế website tin tức về phụ nữ</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/workour-process-app-300x300.png" alt="Ứng dụng theo dõi luyện tập thể thao"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Ứng dụng theo dõi luyện tập thể thao" href="/portfolios/ung-dung-theo-doi-luyen-tap-the-thao/">Ứng dụng theo dõi luyện tập thể thao</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/elderly-app-300x300.png" alt="Ứng dụng dành cho người cao tuổi"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Ứng dụng dành cho người cao tuổi" href="/portfolios/ung-dung-danh-cho-nguoi-cao-tuoi/">Ứng dụng dành cho người cao tuổi</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/ksa-motor-300x300.png" alt="Sàn giao dịch xe ôtô uy tín và đầy đủ tính năng"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Sàn giao dịch xe ôtô uy tín và đầy đủ tính năng" href="/portfolios/san-giao-dich-xe-oto-top-3/">Sàn giao dịch xe ôtô uy tín và đầy đủ tính năng</a></div></div></div></div><div class="item blockchain-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/du-an-blockchain-1-300x300.png" alt="Dự án blockchain về nền tảng âm nhạc"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/ung-dung-blockchain/" rel="tag">Lập trình Blockchain</a></div><div class="title-post"><a title="Dự án blockchain về nền tảng âm nhạc" href="/portfolios/du-an-blockchain-ve-nen-tang-am-nhac/">Dự án blockchain về nền tảng âm nhạc</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/lifebuddi-1-300x300.png" alt="Ứng dụng rèn luyện sức khỏe LifeBuddi"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Ứng dụng rèn luyện sức khỏe LifeBuddi" href="/portfolios/ung-dung-luyen-tap-suc-khoe-lifebuddi/">Ứng dụng rèn luyện sức khỏe LifeBuddi</a></div></div></div></div><div class="item mobile-development"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/mOfice-1-300x300.png" alt="Ứng dụng văn phòng điện tử mOffice"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-mobile-app-2/" rel="tag">Thiết kế ứng dụng Mobile App</a></div><div class="title-post"><a title="Ứng dụng văn phòng điện tử mOffice" href="/portfolios/ung-dung-van-phong-dien-tu-moffice/">Ứng dụng văn phòng điện tử mOffice</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/baitap123-1-300x300.png" alt="Website giáo dục Baitap123.com"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Website giáo dục Baitap123.com" href="/portfolios/website-bai-tap-trac-nghiem-truc-tuyen-baitap123-com/">Website giáo dục Baitap123.com</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2018/08/web-hero-300x300.png" alt="WebHero – Đơn vị thiết kế Website"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="WebHero – Đơn vị thiết kế Website" href="/portfolios/webhero-don-vi-thiet-ke-website/">WebHero – Đơn vị thiết kế Website</a></div></div></div></div><div class="item web-developement"><div class="wrap-border"><div class="featured-post"><img src="/wp-content/uploads/2017/04/caza1-300x280.png" alt="Website nội thất Caza.vn"></div><div class="portfolio-details"><div class="category-post"><a href="/portfolios_category/thiet-ke-ung-dung-website-2/" rel="tag">Thiết kế ứng dụng Website</a></div><div class="title-post"><a title="Website nội thất Caza.vn" href="/portfolios/website-noi-that-caza-vn/">Website nội thất Caza.vn</a></div></div></div></div></div></div>
    </div>   
        
<div id="secondary" class="widget-area" role="complementary">
	<div class="sidebar">
	<div id="nav_menu-4" class="widget widget_nav_menu"><div class="menu-vertical-menu-container"><ul id="menu-vertical-menu" class="menu"><li id="menu-item-1062" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1062"><a href="/ve-chung-toi/">Giới thiệu chung</a></li>
<li id="menu-item-156" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-156"><a href="/lich-su-cong-ty/">Lịch sử công ty</a></li>
<li id="menu-item-152" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-152"><a href="/doi-tac/">Đối tác</a></li>
<li id="menu-item-154" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-154"><a href="/nhan-su/">Nhân sự</a></li>
</ul></div></div><div id="text-7" class="widget widget_text"><h4 class="widget-title">BẠN CẦN TƯ VẤN?</h4>			<div class="textwidget"><div class="widget widget-need" style="margin-top: 7px;    margin-bottom: 2px;">
           <p style="letter-spacing:-0.5px;        margin-bottom: 23px;"> Chúng tôi trực tuyến 15 giờ mỗi ngày từ 8:30 sáng đến 11:30 tối. </p>
 <a href="/vi/lien-he/" class="themesflat-button" style="width: 130px; text-align: center;  letter-spacing: 0.4px;  border-radius: 3px; padding-top: 8px;">Liên hệ ngay</a>
 </div></div>
		</div><div id="text-6" class="widget widget_text"><h4 class="widget-title">HỒ SƠ NĂNG LỰC</h4>			<div class="textwidget"><div class="widget widget-brochures" style="    padding-top: 5px; letter-spacing: -0.4px;margin-bottom:0">
                                                                             <p style="margin-bottom:30px;">Bạn vui lòng tải hồ sơ năng lực tại đây</p>
                                        <ul class="dowload">
                                            <li class="dl-pdf"><a target="_blank" href="https://drive.google.com/file/d/19cJ2sOH-iN5F0Yr1QF2-L3Mw7Hm811hL/view">Vinsofts_Profile_VI.pdf</a></li>
                                        </ul>
                                    </div></div>
		</div><div id="widget_themesflat_testimonial-4" class="widget widget-themesflat-testimonial">
		<div class="testimonial-sliders   themesflat_1539250461 style3" data-autoplay="0" data-show_control="0" data-show_direction="0" data-margin="30" data-items="1">
			<img class="logo_svg testimonial_logo" data-src="/wp-content/themes/fo/images/testimonial.svg" src="/wp-content/themes/fo/images/testimonial.svg" alt="thumb">
			<div class="slide_nav"><ul class="slides"><li><img width="120" height="120" src="/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-120x120.jpg" class="attachment-themesflat-testimonial size-themesflat-testimonial wp-post-image" alt="" srcset="/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-120x120.jpg 120w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-300x300.jpg 300w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-100x100.jpg 100w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-600x600.jpg 600w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-150x150.jpg 150w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-570x570.jpg 570w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1.jpg 640w" sizes="(max-width: 120px) 100vw, 120px"></li></ul></div>
			<div class="testimonial-slider">		
				<div class="item">
			<div class="testimonial-content">
				<blockquote class="themesflat_quote1">
					<p style="font-family: FontVinsofts; text-align: justify;">Để đi đến Thành công là cả một chặng đường dài, chắc chắn sẽ gặp nhiều chông gai nhưng tập thể Vinsofts luôn xác định mỗi một dự án thành công, mỗi một khách hàng hài lòng, mỗi một mục tiêu hoàn thành chính là những Cột mốc trên Con đường đi đến Thành công đó.</p>
				</blockquote>
				<div class="testimonial-image">
					<img width="120" height="120" src="/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-120x120.jpg" class="attachment-themesflat-testimonial size-themesflat-testimonial wp-post-image" alt="" srcset="/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-120x120.jpg 120w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-300x300.jpg 300w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-100x100.jpg 100w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-600x600.jpg 600w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-150x150.jpg 150w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1-570x570.jpg 570w,/wp-content/uploads/2018/08/photo_2017-09-01_12-01-22-1.jpg 640w" sizes="(max-width: 120px) 100vw, 120px">
				</div>
				<div class="testimonial-author">
					<h6 class="author-name"><a href="">Phùng Văn Huân</a></h6>
					<div class="author-info">Ceo & Founder <p></p></div>
					
				</div>
				<blockquote class="themesflat_quote2">
					<p style="font-family: FontVinsofts; text-align: justify;">Để đi đến Thành công là cả một chặng đường dài, chắc chắn sẽ gặp nhiều chông gai nhưng tập thể Vinsofts luôn xác định mỗi một dự án thành công, mỗi một khách hàng hài lòng, mỗi một mục tiêu hoàn thành chính là những Cột mốc trên Con đường đi đến Thành công đó.</p>
				</blockquote>
			</div>	
		</div>			
			</div>
		</div>
	</div>	</div>
</div>
    
</div>


<div class="col-md-12">
       
</div>

            </div>
        </div>
    </div>

    
    <div class="footer_background">
        <footer class="footer">      
            <div class="container">
                <div class="row"> 
                 <div class="footer-widgets">
                                            <div class="col-md-4 col-sm-6">
                            <div id="text-2" class="widget widget_text">			<div class="textwidget"><p><a title="Financial Occult" href="/"><img style="height: 70px;" src="/wp-content/uploads/2019/06/LOGO-FOOTER-min.png" alt="thumb"><br>
</a><a title="Financial Occult" href="#"><br>
</a></p>
</div>
		</div><div id="text-5" class="widget widget_text">			<div class="textwidget"><p>Với đội ngũ 70+ nhân viên và 10+ năm kinh nghiệm trong việc hợp tác và phát triển phần mềm cho các khách hàng từ khắp nơi trên thế giới, chúng tôi tự hào là một công ty gia công phần mềm hàng đầu tại Việt Nam. Chúng tôi luôn cam kết đem đến chất lượng dịch vụ cao nhất, trở thành đối tác tin cậy với bất kỳ đơn vị doanh nghiệp nào</p>
</div>
		</div>                        </div>
                                            <div class="col-md-2 col-sm-6">
                            <div id="text-10" class="widget widget_text"><h4 class="widget-title">KẾT NỐI NHANH</h4>			<div class="textwidget"></div>
		</div><div id="widget_themesflat_socials-5" class="widget widget_themesflat_socials">

                <ul class="themesflat-shortcode-socials">
            <li class="facebook">
                            <a class="title" href="https://www.facebook.com/vinsoftsjsc" target="_blank" rel="alternate" title="Facebook">
                                <i class="fa fa-facebook"></i>
                                <span>Facebook</span>
                            </a>
                        </li><li class="youtube">
                            <a class="title" href="https://www.youtube.com/channel/UC33N-fg42Gkp1Fcin5Zp5EA/" target="_blank" rel="alternate" title="Youtube">
                                <i class="fa fa-youtube"></i>
                                <span>Youtube</span>
                            </a>
                        </li><li class="linkedin">
                            <a class="title" href="https://www.linkedin.com/company/vinsofts/" target="_blank" rel="alternate" title="LinkedIn">
                                <i class="fa fa-linkedin"></i>
                                <span>LinkedIn</span>
                            </a>
                        </li>        </ul>       
    

        </div><div id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><ul class="themesflat-shortcode-socials">
<li style="width: 115%;">
<a href="https://goo.gl/maps/UksWXm7gqr62" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i>Google map</a>
</li>
</ul></div></div>                        </div>
                                            <div class="col-md-3 col-sm-6">
                            <div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widget-title">DỊCH VỤ NỔI BẬT</h4><div class="menu-footer_service-container"><ul id="menu-footer_service" class="menu"><li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2902"><a href="/phat-trien-ung-dung-mobile/">Phát triển ứng dụng Mobile</a></li>
<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2903"><a href="/phat-trien-ung-dung-web/">Phát triển website doanh nghiệp</a></li>
<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2901"><a href="/phat-trien-phan-mem-quan-ly/">Phát triển phần mềm quản lý</a></li>
<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2904"><a href="/phat-trien-ung-dung-blockchain/">Phát triển ứng dụng Blockchain</a></li>
</ul></div></div>                        </div>
                                            <div class="col-md-3 col-sm-6">
                            <div id="nav_menu-14" class="widget widget_nav_menu"><h4 class="widget-title">Thông Tin</h4><div class="menu-menu-footer-right-container"><ul id="menu-menu-footer-right" class="menu"><li id="menu-item-3559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3559"><a href="/lien-he/">Liên hệ</a></li>
<li id="menu-item-4013" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4013"><a href="http://PLACEHOLDER.wpsho/jobs/">Tuyển dụng</a></li>
</ul></div></div><div id="custom_html-6" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="cus_dmca">
	<a href="//www.dmca.com/Protection/Status.aspx?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" title="DMCA.com Protection Status" class="dmca-badge"> <img src="https://images.dmca.com/Badges/dmca_protected_16_120.png?ID=206b4f12-b46d-46f4-b58f-8056bfdb1559" alt="DMCA.com Protection Status"></a>
</div>
<style type="text/css">
	.cus_dmca a img {
    padding: 10px 0px 0px 48px;
}
</style></div></div>                        </div>
                                       
                    </div>           
                </div>    
            </div>   
        </footer>
        <div class="content-register-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="left">  
                                                                                                                
                                <div class="textwidget">
                                    <p>Cơ quan chủ quản: Công ty Cổ phần Vinsofts</p>
                                    
                                    <p>Văn phòng tại Hà Nội: Tầng 5, số 8 Phan Văn Trường, phường Dịch Vọng Hậu, Cầu Giấy, Hà Nội</p>
                                    
                                    <p>Văn phòng tại Tp.HCM: P516 Block C Charmington La Pointe, 181 Cao Thắng, P.12, Q.10, TP.HCM</p>
                                    <p>Tel: <a href="tel:0462593148">04.6259.3148</a> – Hotlline: <a href="tel:0961678247">0961.678.247</a> – Email: <a href="mailto:info@vinsofts.com">info@vinsofts.com</a></p>
                                    <p>Giấy phép kinh doanh số 0107354530 do Sở Kế hoạch và đầu tư cấp ngày 14/03/2016</p>
                                </div>
                                                                                </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="right">
                                                                                    
                                <div class="textwidget">
                                     <p>Bạn vui lòng đọc kỹ <a href="/vi/chinh-sach-bao-mat">Chính sách bảo mật thông tin</a> và <a href="/vi/dieu-khoan-su-dung">Điều khoản sử dụng</a>!</p>
                                     <p>Website đã được thông báo và được chấp nhận bởi Cục TMĐT và CNTT, Bộ Công Thương.</p>
                                     <p><a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43043"><img class="alignnone size-medium wp-image-5318" src="/wp-content/uploads/2018/10/vinsofts-dathongbao-300x114.png" alt="" width="300" height="114"></a></p>
                                </div>
                                                                                 </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bottom">
            <div class="container">           
                <div class="row">
                    <div class="col-md-12">
                            
                        <div class="copyright">                        
                            <p>Copyright <i class="fa fa-copyright"></i> 2012 - 2019
<a href="#">Vinsofts JSC</a>. All rights reserved.</p>                        </div>

                                                    
                            <a class="go-top show">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                           

                            
                    </div>
                </div>
            </div>
        </div> 
                            <div id="tawk-to-vinsofts">
                
                <script type="text/javascript">
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5b7f8308f31d0f771d84184a/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();
                </script>
                
            </div>  
            </div> 


<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8XSN4W" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>
(function () {
    document.addEventListener("DOMContentLoaded", function () {
        var e = "dmca-badge";
        var t = "refurl";
        if (!document.getElementsByClassName) {
            document.getElementsByClassName = function (e) {
                var t = document.getElementsByTagName("a"), n = [], r = 0, i;
                while (i = t[r++]) {
                    i.className == e ? n[n.length] = i : null
                }
                return n
            }
        }
        var n = document.getElementsByClassName(e);
        if (n[0].getAttribute("href").indexOf("refurl") < 0) {
            for (var r = 0; r < n.length; r++) {
                var i = n[r];
                i.href = i.href + (i.href.indexOf("?") === -1 ? "?" : "&") + t + "=" + document.location
            }
        }
    }, false)
}
)()
</script>
<script type="text/javascript">
var recaptchaWidgets = [];
var recaptchaCallback = function() {
	var forms = document.getElementsByTagName( 'form' );
	var pattern = /(^|\s)g-recaptcha(\s|$)/;

	for ( var i = 0; i < forms.length; i++ ) {
		var divs = forms[ i ].getElementsByTagName( 'div' );

		for ( var j = 0; j < divs.length; j++ ) {
			var sitekey = divs[ j ].getAttribute( 'data-sitekey' );

			if ( divs[ j ].className && divs[ j ].className.match( pattern ) && sitekey ) {
				var params = {
					'sitekey': sitekey,
					'type': divs[ j ].getAttribute( 'data-type' ),
					'size': divs[ j ].getAttribute( 'data-size' ),
					'theme': divs[ j ].getAttribute( 'data-theme' ),
					'badge': divs[ j ].getAttribute( 'data-badge' ),
					'tabindex': divs[ j ].getAttribute( 'data-tabindex' )
				};

				var callback = divs[ j ].getAttribute( 'data-callback' );

				if ( callback && 'function' == typeof window[ callback ] ) {
					params[ 'callback' ] = window[ callback ];
				}

				var expired_callback = divs[ j ].getAttribute( 'data-expired-callback' );

				if ( expired_callback && 'function' == typeof window[ expired_callback ] ) {
					params[ 'expired-callback' ] = window[ expired_callback ];
				}

				var widget_id = grecaptcha.render( divs[ j ], params );
				recaptchaWidgets.push( widget_id );
				break;
			}
		}
	}
};

document.addEventListener( 'wpcf7submit', function( event ) {
	switch ( event.detail.status ) {
		case 'spam':
		case 'mail_sent':
		case 'mail_failed':
			for ( var i = 0; i < recaptchaWidgets.length; i++ ) {
				grecaptcha.reset( recaptchaWidgets[ i ] );
			}
	}
}, false );
</script>
<link rel="stylesheet" href="/wp-content/cache/minify/a01ef.css" media="all">

<script src="/wp-content/cache/minify/89975.js"></script>

<script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>

<script src="/wp-content/cache/minify/dd928.js"></script>

<script type="text/javascript">
/* <![CDATA[ */
var wpcf7_redirect_forms = {"5612":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"5444":{"page_id":"5739","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/dang-ky-thanh-cong\/"},"4387":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"4383":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3863":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3777":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"3769":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"3697":{"page_id":"3780","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":"\/thank-you\/"},"1989":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""},"644":{"page_id":"0","external_url":"","use_external_url":"","open_in_new_tab":"","http_build_query":"","http_build_query_selectively":"","http_build_query_selectively_fields":"","after_sent_script":"","thankyou_page_url":""}};
/* ]]> */
</script>












<script src="/wp-content/cache/minify/3d91a.js"></script>

<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit"></script>
<script src="/wp-content/cache/minify/fc1cb.js"></script>

</body>
</html>
